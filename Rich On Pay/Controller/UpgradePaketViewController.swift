//
//  UpgradePaketViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/22/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class UpgradePaketViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var mainView: UIView!
    var packages = ["silver", "gold", "platinum"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Upgrade Paket"
        if let user = Defaults[.user] {
            if user.packages == 2 {
                packages = ["gold"]
            }
            else if user.packages == 1 {
                packages = ["gold", "platinum"]
            }
        }
        pageControl.numberOfPages = self.packages.count
        switch self.packages[0] {
        case "silver":
            self.mainView.backgroundColor = UIColor.backgroundSilver
            break
        case "gold":
            self.mainView.backgroundColor = UIColor.backgroundGold
            break
        default:
            self.mainView.backgroundColor = UIColor.backgroundPlatinum
            break
        }
    }
    @IBAction func upgradeClicked(_ sender: Any) {
        let selectedPackages = self.packages[pageControl.currentPage]
        API.request(API.Service.upgrade(packages: selectedPackages == "platinum" ? 3 : (selectedPackages == "gold" ? 2 : 1)), showLoading: true, success: { (response: APIResponse) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopUpSelectBankViewController") as! TopUpSelectBankViewController
            vc.upgradeRequest = response.data?.upgradeRequest
            self.show(vc, sender: nil)
            
        }) { (error) in
            if let message = error.message {
                AlertView.showAlert(view: self, message: message, title: "Maaf")
            }
        }
        
        
    }
    
}
extension UpgradePaketViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.packages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PaketCollectionViewCell
        cell.setupView(packages: self.packages[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 370)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        self.pageControl.currentPage = page
        UIView.animate(withDuration: 0.7, delay: 0.0, options:[], animations: {
            switch self.packages[page] {
            case "silver":
                self.mainView.backgroundColor = UIColor.backgroundSilver
                break
            case "gold":
                self.mainView.backgroundColor = UIColor.backgroundGold
                break
            default:
                self.mainView.backgroundColor = UIColor.backgroundPlatinum
                break
            }
        }, completion:nil)
      
    }
//    func scroll
}

class PaketCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var packageIV: UIImageView!
    @IBOutlet weak var potensiLbl: UILabel!
    @IBOutlet weak var cashbackLbl: UILabel!
    @IBOutlet weak var paketTypeWrapperView: UIView!
//    @IBOutlet weak var paketTypeLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupView(packages: String) {
        switch packages {
        case "silver":
            packageIV.image = UIImage(named: "ic_silver")
            feeLbl.text = "RP 1.000.000"
            potensiLbl.text = "RP 300.000"
            cashbackLbl.text = "2%"
            break
        case "gold":
            packageIV.image = UIImage(named: "ic_gold")
            feeLbl.text = "RP 3.000.000"
            potensiLbl.text = "RP 1.200.000"
            cashbackLbl.text = "3%"
            break
        default:
            packageIV.image = UIImage(named: "ic_platinum")
            feeLbl.text = "RP 5.000.000"
            potensiLbl.text = "RP 3.600.000"
            cashbackLbl.text = "5%"
            break
        }
    }
}



