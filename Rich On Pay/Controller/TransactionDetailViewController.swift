//
//  TransactionDetailViewController.swift
//  Rich On Pay
//
//  Created by Edison on 1/17/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class TransactionDetailViewController: UIViewController {
    
    @IBOutlet weak var bankLbl: UILabel!
    @IBOutlet weak var contentViewHeightCT: NSLayoutConstraint!
    @IBOutlet weak var customerTitleLbl: UILabel!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var bankTitleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var transactionCodelbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    var order: Order!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detail Transaksi"
        self.setup(order: order)
        if order.type == 4 {
            customerNameLbl.isHidden = true
            customerTitleLbl.isHidden = true
            self.contentViewHeightCT.constant = 220
            bankTitleLbl.text = "Nomor Handphone"
            bankLbl.text = order.option.phoneNumber
        }
        else if order.type == 0 {
            customerNameLbl.isHidden = true
            customerTitleLbl.isHidden = true
            self.contentViewHeightCT.constant = 220
            bankTitleLbl.text = "Bank Pembayaran"
            bankLbl.text = "\(order.virtual.destBankAccount.bank.abbr!) / \(order.virtual.destBankAccount.accountNo!)"
        }
        else {
            bankTitleLbl.text = "Nomor Meter"
            self.contentViewHeightCT.constant = 280
            if let option = order.option {
                bankLbl.text = option.meterNo
                customerNameLbl.text = option.customerName
            }
        }
    }
    
    func setup(order: Order) {
        let package = getTypeDescription(type: order.type)
        priceLbl.text = "\(package) Rp \(order.total.formattedWithSeparator)"
        transactionCodelbl.text = "\(order.referenceNo!)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        dateLbl.text = "\(dateFormatter.string(from: order.createdAt))"
        switch order.status {
        case 5,4:
            self.statusLbl.text = "BATAL"
            self.statusLbl.backgroundColor = UIColor.primary
            break
        case 1:
            self.statusLbl.text = "MENUNGGU"
            self.statusLbl.backgroundColor = UIColor.yellowR
            break
        case 6:
            self.statusLbl.text = "REFUND"
            self.statusLbl.backgroundColor = UIColor.purpleR
            break
        case 3:
            self.statusLbl.text = "BERHASIL"
            self.statusLbl.backgroundColor = UIColor.greenR
            break
        case 2:
            self.statusLbl.text = "DIPROSES"
            self.statusLbl.backgroundColor = UIColor.blueR
            break
        case 0:
            self.statusLbl.text = "PENDING"
            self.statusLbl.backgroundColor = UIColor.yellowR
            break
        default:
            break
        }
    }
    
    func getTypeDescription(type: Int) -> String {
        switch type {
        case 1:
            return  "Withdraw"
        case 2:
            return  "PLN"
        case 3:
            return  "PLN"
        case 4:
            return  "Pulsa"
        case 5:
            return  "Pulsa"
        case 6:
            return  "Isi Saldo"
        case 7:
            return  "Pay"
        case 8:
            return  "Transfer"
        case 9:
            return  "Split"
        case 10:
            return  "Cashback"
        case 11:
            return  "Game"
        case 12:
            return  "Freeze"
        case 13:
            return  "Unfreeze"
        case 14:
            return  "Withdraw"
        case 15:
            return  "Bonus Sponsor"
        case 16:
            return  "BPJS"
        case 17:
            return  "Reversal"
        case 18:
            return  "Upgrade Paket"
        case 19:
            return  "Bonus Pairing"
        default:
            return ""
        }
    }
    
}
