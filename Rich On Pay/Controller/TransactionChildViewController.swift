//
//  TransactionChildViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/25/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class TransactionChildViewController: UIViewController {
    
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var tableView: UITableView!
    fileprivate var refreshControl: UIRefreshControl!
    var id: Int!
    var orders = [Order]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pesanan"
        self.addRefreshControl()
        self.getTransaction()
        NotificationCenter.default.addObserver(self, selector: #selector(getTransaction), name: NSNotification.Name(rawValue: "RefreshTransaction"), object: nil)
    }
    
    @objc func getTransaction() {
        API.request(API.Service.getTransaction(status: "0,1,2,3", type: "0,02,3,4,5,11,18"), showLoading: false, success: { (response: APIResponse) in
            if let orders = response.data?.orders {
                self.orders = orders
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.placeholderView.isHidden = orders.count != 0
                    self.tableView.isHidden = orders.count == 0
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
        }
    }
    
    fileprivate func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.primary
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.primary]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh".localized, attributes: attributes)
        refreshControl.addTarget(self, action: #selector(getTransaction), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
}

extension TransactionChildViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orders.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TransactionTableCell
        cell.setup(order: self.orders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.orders[indexPath.row].type == 0 || self.orders[indexPath.row].type == 4 || self.orders[indexPath.row].type == 2 || self.orders[indexPath.row].type == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionDetailViewController") as! TransactionDetailViewController
            vc.order = self.orders[indexPath.row]
            self.show(vc, sender: nil)
        }
        
    }
}

class TransactionTableCell: UITableViewCell {
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var transactionCodelbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    func setup(order: Order) {
        let package = getTypeDescription(type: order.type)
        priceLbl.text = "\(package) Rp \(order.total.formattedWithSeparator)"
        transactionCodelbl.text = "Kode Transaksi: \(order.referenceNo!)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        dateLbl.text = "\(dateFormatter.string(from: order.createdAt))"
        switch order.status {
        case 5,4:
            self.statusLbl.text = "BATAL"
            self.statusLbl.backgroundColor = UIColor.primary
            break
        case 1:
            self.statusLbl.text = "MENUNGGU"
            self.statusLbl.backgroundColor = UIColor.yellowR
            break
        case 6:
            self.statusLbl.text = "REFUND"
            self.statusLbl.backgroundColor = UIColor.purpleR
            break
        case 3:
            self.statusLbl.text = "BERHASIL"
            self.statusLbl.backgroundColor = UIColor.greenR
            break
        case 2:
            self.statusLbl.text = "DIPROSES"
            self.statusLbl.backgroundColor = UIColor.blueR
            break
        case 0:
            self.statusLbl.text = "PENDING"
            self.statusLbl.backgroundColor = UIColor.yellowR
            break
        default:
            break
        }
    }
    
    func getTypeDescription(type: Int) -> String {
        switch type {
        case 1:
            return  "Withdraw"
        case 2:
            return  "PLN"
        case 3:
            return  "PLN"
        case 4:
            return  "Pulsa"
        case 5:
            return  "Pulsa"
        case 6:
            return  "Isi Saldo"
        case 7:
            return  "Pay"
        case 8:
            return  "Transfer"
        case 9:
            return  "Split"
        case 10:
            return  "Cashback"
        case 11:
            return  "Game"
        case 12:
            return  "Freeze"
        case 13:
            return  "Unfreeze"
        case 14:
            return  "Withdraw"
        case 15:
            return  "Bonus Sponsor"
        case 16:
            return  "BPJS"
        case 17:
            return  "Reversal"
        case 18:
            return  "Upgrade Paket"
        case 19:
            return  "Bonus Pairing"
        default:
            return ""
        }
    }
}


