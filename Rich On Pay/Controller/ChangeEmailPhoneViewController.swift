//
//  ChangeEmailPhoneViewController.swift
//  Rich On Pay
//
//  Created by Edison on 10/10/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ChangeEmailPhoneViewController: UIViewController {
    
    @IBOutlet weak var newNumberTF: UITextField!
    @IBOutlet weak var newNumberTitle: UILabel!
    @IBOutlet weak var oldNumberTF: UITextField!
    @IBOutlet weak var oldNumberTitle: UILabel!
    var isVerifyEmail = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = Defaults[.user] {
            if isVerifyEmail {
                self.title = "Email"
                self.newNumberTitle.text = "Email Baru"
                self.oldNumberTitle.text = "Email Lama"
                self.oldNumberTF.text = user.email
            }
            else {
                self.title = "Nomor Handphone"
                self.oldNumberTF.text = user.phoneNumber
                self.newNumberTF.keyboardType = .numberPad
                newNumberTF.delegate = self
                newNumberTF.addTarget(self, action: #selector(phoneNumberTextFieldDidChanged(_:)), for: .editingChanged)
            }
        }
    }
    
    @IBAction func phoneNumberTextFieldDidChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        if text == "0" {
            sender.attributedText = getAttributedText(text: "+62 ")
        }
        else if text.contains("+62 ") && text.count == 4 {
            sender.text = ""
        }
        else if text.count >= 1 && !text.contains("+62 ") {
            var newText = text.replacingOccurrences(of: "+62", with: "")
            newText = newText.replacingOccurrences(of: "+", with: "")
            sender.attributedText = getAttributedText(text: "+62 " + newText)
        }
    }
    
    func getAttributedText(text: String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: text)
        let range = NSRange(location: 0, length: 3)
        attributedText.addAttribute(.foregroundColor, value: UIColor.lightGray, range: range)
        return attributedText
    }
    
    @IBAction func saveButtonCLicked(_ sender: Any) {
        if isVerifyEmail {
            API.request(API.Service.changeEmail(email: newNumberTF.text!), showLoading: true, success: { (response: APIResponse) in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterVerificationCodeViewController") as! EnterVerificationCodeViewController
                vc.forVerifyEmail = true
                vc.credential = self.newNumberTF.text!
                self.show(vc, sender: nil)
            }, failure: { (error) in
                if let email = error.errors.newEmail {
                    if email.count > 0 {
                        AlertView.showAlert(view: self, message: email[0], title: "Maaf")
                    }
                }
            })
        }
        else {
            API.request(API.Service.changePhone(phoneNumber: newNumberTF.text!.replacingOccurrences(of: " ", with: "")), showLoading: true, success: { (response: APIResponse) in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterVerificationCodeViewController") as! EnterVerificationCodeViewController
                vc.forVerifyEmail = false
                vc.credential = self.newNumberTF.text!
                self.show(vc, sender: nil)
            }, failure: { (error) in
                if let errors = error.errors {
                    if let phone = errors.newPhone {
                        if phone.count > 0 {
                            AlertView.showAlert(view: self, message: phone[0], title: "Maaf")
                        }
                    }
                }
                else {
                    AlertView.showAlert(view: self, message: error.message, title: "Maaf")
                }
            })
        }
    }
    
}

extension ChangeEmailPhoneViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !isVerifyEmail {
            guard let text = textField.text else { return true }
            if text == "+62 " {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b") == -92
                if (isBackSpace) {
                    textField.text = ""
                    return true
                }
            }
            return string.isPhoneNumberFiltered
        }
       return true
    }
    
}
