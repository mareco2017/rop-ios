//
//  AccountViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/21/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class AccountViewController: UIViewController {
    
    @IBOutlet weak var badgeIV: UIImageView!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var userNumberLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var upgradeWrapperViewCT: NSLayoutConstraint!
    @IBOutlet weak var headerViewCT: NSLayoutConstraint!
    @IBOutlet weak var upgradeWrapperView: UIView!
    @IBOutlet weak var badge: UIButton!
     @IBOutlet weak var scrollView: UIScrollView!
    fileprivate var refreshControl: UIRefreshControl!
    var menu = [["Mutasi Bonus"], ["Verifikasi Data Diri", "Jaringan", "Pengaturan Akun", "Informasi"], [""]]
    var icons =  [["ic_gift"], ["ic_verifikasi", "ic_jaringan", "ic_pengaturan", "ic_info"]]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Akun"
//        self.tableView.fitContentHeight(footerHeight: 10)
        badge.layer.borderWidth = 1
        badge.layer.cornerRadius = 13
        badge.layer.borderColor = UIColor.white.cgColor
        NotificationCenter.default.addObserver(self, selector: #selector(setupProfile), name: NSNotification.Name(rawValue: "RefreshAccount"), object: nil)
        self.addRefreshControl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupProfile()
    }
    
    @IBAction func shareOnlyText(_ sender: UIButton) {
        let text = "Halo , ayo gunakan kode rujukan saya di http://richonpay.com untuk mendapatkan bonus saldo ! \(String(describing: Defaults[.user]!.referralCode))"
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare  , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    fileprivate func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.primary
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.primary]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh".localized, attributes: attributes)
        refreshControl.addTarget(self, action: #selector(self.getProfileData), for: UIControlEvents.valueChanged)
        self.scrollView.addSubview(refreshControl)
    }
    
    @objc func getProfileData() {
        self.getProfile()
    }
    
    @objc func setupProfile() {
        self.refreshControl.endRefreshing()
        if let user = Defaults[.user] {
            let packages = user.packages
            self.usernameLbl.text = user.fullName
            self.userNumberLbl.text = user.phoneNumber
            self.codeLbl.text = user.referralCode
            if packages == 3 {
                badgeIV.image = UIImage(named: "ic_platinum")
                badge.isHidden = true
                upgradeWrapperView.isHidden = true
                upgradeWrapperViewCT.constant = 0
                headerViewCT.constant = 135
            }
            else if packages == 2 {
                badgeIV.image = UIImage(named: "ic_gold")
                badge.isHidden = true
                upgradeWrapperView.isHidden = false
                upgradeWrapperViewCT.constant = 100
                headerViewCT.constant = 235
            }
            else if packages == 1 {
                badgeIV.image = UIImage(named: "ic_silver")
                badge.isHidden = true
                upgradeWrapperView.isHidden = false
                upgradeWrapperViewCT.constant = 100
                headerViewCT.constant = 235
            }
            else {
                badgeIV.isHidden = true
                badge.isHidden = false
                badge.setTitle("FREE MEMBER", for: .normal)
                badge.backgroundColor = UIColor.black
                upgradeWrapperView.isHidden = false
                upgradeWrapperViewCT.constant = 100
                headerViewCT.constant = 235
            }
            if user.verificationStatus.nric == 1 {
                var index = 0
                if user.packages != 0 {
                    index += 1
                }
                if self.menu[index].count > 3 {
                    self.icons[index].remove(at: 0)
                    self.menu[index].remove(at: 0)
                }
            }
        }
        self.tableView.reloadAndFitHeight()
    }
    
    func showLogoutAlert() {
        let alertController = UIAlertController(title: nil, message: "Keluar dari ROP?".localized, preferredStyle: .alert)
        
        let logoutAction = UIAlertAction(title: "Keluar".localized, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            API.request(API.Service.logout, showLoading: false, success: { (response: APIResponse) in
                
            }, failure: { (error) in
            })
  
            self.logout()
            
        })
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        logoutAction.setValue(UIColor.primary, forKey: "titleTextColor")
        alertController.setValue(NSAttributedString(string: alertController.message!, attributes: [NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: 15.0)!, NSAttributedStringKey.foregroundColor: UIColor.darkGray]), forKey: "attributedMessage")
        alertController.addAction(logoutAction)
        alertController.addAction(cancelAction)
//        if let popoverPresentationController = alertController.popoverPresentationController {
//            popoverPresentationController.sourceView = sender.contentView
//            popoverPresentationController.sourceRect = sender.bounds
//        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func logout() {
        API.token = nil
        Defaults[.user] = nil
        Defaults[.isLogging] = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.swapRootView(vc: vc)
    }
    
}

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if let user = Defaults[.user] {
            if user.packages == 0 {
                return self.menu[section + 1].count
            }
        }
        return self.menu[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let user = Defaults[.user] {
            if user.packages == 0 {
                return self.menu.count - 1
            }
        }
        return self.menu.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.lightGrayS
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var section = indexPath.section
        if let user = Defaults[.user] {
            if user.packages == 0 {
                section += 1
            }
        }
        if section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "logoutCell", for: indexPath)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AccountTableCell
        cell.menuNameLbl.text = self.menu[section][indexPath.row]
        cell.menuIV.image = UIImage(named: self.icons[section][indexPath.row])
        cell.speratorView.isHidden = section == 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var section = indexPath.section
        if let user = Defaults[.user] {
            if user.packages == 0 {
                section += 1
            }
        }
        if section == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "MutationListViewController") as! MutationListViewController
            vc.title = "Mutasi Bonus"
            self.show(vc, sender: nil)
        }
        else if section == 2 {
            self.showLogoutAlert()
        }
        else {
            switch self.menu[section][indexPath.row] {
            case "Pengaturan Akun":
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingViewController")
                self.show(vc!, sender: nil)
                break
            case "Verifikasi Data Diri":
                if let user = Defaults[.user] {
                    if user.verificationStatus.nric == 2 {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                        self.show(vc, sender: nil)
                    }
                    else {
                        AlertView.showAlert(view: self, message: "", title: "Data anda sedang dalam proses verifikasi")
                    }
                }
                break
            case "Jaringan":
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebDocumentViewController") as! WebDocumentViewController
                vc.url = "/binary?token=\(API.token!)"
                vc.title = "Jaringan"
                self.show(vc, sender: nil)
                break
            case "Informasi":
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebDocumentViewController") as! WebDocumentViewController
                vc.url = "/about-us"
                vc.title = "Tentang Kami"
                self.show(vc, sender: nil)
                break
            default:
                break
            }
        }
    }
}

class AccountTableCell: UITableViewCell {
    
    @IBOutlet weak var menuIV: UIImageView!
    @IBOutlet weak var menuNameLbl: UILabel!
    @IBOutlet weak var speratorView: UIView!
    
    
}
