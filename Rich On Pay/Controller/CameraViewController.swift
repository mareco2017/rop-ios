//
//  CameraViewController.swift
//  Mareco
//
//  Created by Edison on 4/20/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import AVFoundation

protocol CapturePhotoDelegate {
    func getCapturedPhoto(image: UIImage)
}

class CameraViewController: UIViewController  {
    
    @IBOutlet weak var usePhotoBtn: UIButton!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var flipBtn: UIButton!
    @IBOutlet weak var captureBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var ktpPhoto: UIImage!
    
    var captureSession: AVCaptureSession!
    
    private var stillImageOutput: AnyObject?
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    var delegate: CapturePhotoDelegate?
    var usingFrontCamera = true
    var frontCameraEnabled = false
    var typeTitle = "KTP"
    
    override func viewDidLoad() {
        var titleText = ""
        var typeText = ""
        
        super.viewDidLoad()
        bottomView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        retakeBtn.layer.borderColor = UIColor.white.cgColor
        retakeBtn.layer.borderWidth = 1
        
        switch typeTitle {
        case "KTP":
            titleText = frontCameraEnabled ? "Selfie with your ID Card".localized : "ID Card Photo".localized
            typeText = frontCameraEnabled ? "face and ID Card".localized : "ID Card"
            break
        case "SIM":
            titleText = frontCameraEnabled ? "Selfie with your Driver License".localized : "Driver License Photo".localized
            typeText = frontCameraEnabled ? "face and Driver License".localized : "Driver License"
            break
        default:
            titleText = frontCameraEnabled ? "Selfie with your Passport".localized : "Passport Photo".localized
            typeText = frontCameraEnabled ? "face and Passport".localized : "Passport"
            break
        }
        titleLabel.text = titleText
        descriptionLabel.text = "Ensure the whole \(typeText) fits within the screen.".localized
        flipBtn.isHidden = !frontCameraEnabled
        self.title = !frontCameraEnabled ? "Verifikasi KTP" : "Selfie dengan KTP"
        UIApplication.shared.statusBarStyle = .lightContent
        if frontCameraEnabled {
            usePhotoBtn.setTitle("KIRIM", for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = frontCameraEnabled ? AVCaptureSession.Preset.hd1280x720 : AVCaptureSession.Preset.hd1920x1080
        let device = getCameraCaptureDevice(isFrontType: frontCameraEnabled)
        guard let captureDevice = device else { return }
        initializeCaptureDevice(device: captureDevice)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createOverlay()
        previewLayer?.bounds = self.view.frame
        previewLayer?.position = CGPoint(x: self.cameraView.frame.width / 2, y: self.cameraView.frame.height / 2)
        if let previewLayer = previewLayer {
            cameraView.layer.addSublayer(previewLayer)
            captureSession!.startRunning()
        }
    }
    
    func initializeCaptureDevice(device: AVCaptureDevice) {
        do {
            try device.lockForConfiguration()
            
            if device.isFlashAvailable {
                device.flashMode = .off
            }
            device.unlockForConfiguration()
            let input = try AVCaptureDeviceInput(device: device)
            if captureSession!.canAddInput(input) {
                captureSession!.addInput(input)
                if #available(iOS 10.0, *) {
                    stillImageOutput = AVCapturePhotoOutput()
                    if (captureSession.canAddOutput(stillImageOutput! as! AVCapturePhotoOutput)) {
                        captureSession.addOutput(stillImageOutput! as! AVCapturePhotoOutput)
                    }
                }
                else {
                    if let imageOutput = stillImageOutput as? AVCaptureStillImageOutput {
                        imageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                        if captureSession!.canAddOutput(imageOutput) {
                            captureSession!.addOutput(imageOutput)
                        }
                    }
                }
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
                previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
            }
        }
        catch {
            print(error)
        }
    }
    
    func createOverlay() {
        let overlayPath = UIBezierPath(rect: view.bounds)
        let transparentPath = UIBezierPath(roundedRect: CGRect(x: 15, y: 140, width: self.view.frame.width - 30, height: self.view.frame.height / 2), cornerRadius: 5)
        overlayPath.append(transparentPath)
        overlayPath.usesEvenOddFillRule = true
        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = UIColor.black.withAlphaComponent(0.6).cgColor
        overlayView.layer.addSublayer(fillLayer)
    }
    
    @IBAction func takeIt(_ sender: AnyObject) {
        if #available(iOS 10.0, *) {
            let settingsForMonitoring = AVCapturePhotoSettings()
            settingsForMonitoring.flashMode = .off
            settingsForMonitoring.isAutoStillImageStabilizationEnabled = true
            settingsForMonitoring.isHighResolutionPhotoEnabled = false
            stillImageOutput?.capturePhoto(with: settingsForMonitoring, delegate: self)
        } else {
            if let imageOutput = self.stillImageOutput as? AVCaptureStillImageOutput {
                let videoConnection = imageOutput.connection(with: AVMediaType.video)
                if(videoConnection != nil) {
                    imageOutput.captureStillImageAsynchronously(from: videoConnection!, completionHandler: { (sampleBuffer, error) -> Void in
                        if sampleBuffer != nil {
                            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                            let dataProvider = CGDataProvider(data: imageData! as CFData)
                            let cgImageRef = CGImage.init(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.relativeColorimetric)
                            let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                            self.imagePreview.image = image
                            self.toggleFlash(on: false)
                            self.setupBttmView(isDisableCapture: true)
                        }
                    })
                }
            }
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func flipButtonPressed() {
        usingFrontCamera = !usingFrontCamera

        for i: AVCaptureDeviceInput in (self.captureSession?.inputs as! [AVCaptureDeviceInput]){
            self.captureSession?.removeInput(i)
        }
        do {
            captureSession.stopRunning()
            captureSession.sessionPreset = usingFrontCamera ? AVCaptureSession.Preset.hd1280x720 : AVCaptureSession.Preset.hd1920x1080
            
            let captureDevice = getCameraCaptureDevice(isFrontType: usingFrontCamera)
            previewLayer?.add(getFlipTransition(fromLeft: !usingFrontCamera) , forKey: nil)
            if !usingFrontCamera {
                try captureDevice?.lockForConfiguration()
                if (captureDevice?.isFlashModeSupported(.off))! {
                    captureDevice?.flashMode = .off
                }
                captureDevice?.unlockForConfiguration()
            }
            
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession.addInput(deviceInput)
            captureSession.startRunning()
        } catch let error as NSError {
            AlertView.showAlert(view: self, message: error.localizedDescription, title: "Sorry".localized)
        }
    }
    
    func getFlipTransition(fromLeft: Bool) -> CATransition {
        let animation = CATransition()
        animation.duration = 0.25
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.type = "oglFlip"
        animation.subtype = fromLeft ? kCATransitionFromLeft : kCATransitionFromRight
        return animation
    }
    
    func getCameraCaptureDevice(isFrontType: Bool) -> AVCaptureDevice? {
        if !isFrontType {
            return AVCaptureDevice.default(for: .video)
        }
        let videoDevices = AVCaptureDevice.devices(for: AVMediaType.video)
        
        for device in videoDevices {
            if device.position == AVCaptureDevice.Position.front {
                return device
            }
        }
        return nil
    }
    
    func toggleFlash(on: Bool? = nil) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video)
            else {return}
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                if on != nil {
                    
                    device.torchMode = on! ? .on : .off
                }
                else {
                    device.torchMode = device.isTorchActive ? .off : .on
                }
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    @IBAction func retakeBtnPressed(_ sender: Any) {
        self.imagePreview.image = nil
        self.setupBttmView(isDisableCapture: false)
    }
    
    func setupBttmView(isDisableCapture: Bool) {
        self.captureBtn.isHidden = isDisableCapture
//        self.cancelBtn.isHidden = isDisableCapture
        self.retakeBtn.isHidden = !isDisableCapture
        self.usePhotoBtn.isHidden = !isDisableCapture
        self.bottomView.backgroundColor = !isDisableCapture ? UIColor.black.withAlphaComponent(0.4) : UIColor.clear
    }
    
    @IBAction func usePhotoButtonTapped(_ sender: UIButton) {
        if !frontCameraEnabled {
            guard let capturedPhoto = self.imagePreview.image else { return }
            let vc = storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
            vc.ktpPhoto = capturedPhoto
            vc.frontCameraEnabled = true
            self.show(vc, sender: nil)
        }
        else {
            guard let capturedPhoto = self.imagePreview.image else { return }
            API.upload(API.Service.uploadIdentity(ktp: self.ktpPhoto, selfieKtp: capturedPhoto), showLoading: true, success: { (response: APIResponse) in
                AlertView.showAlert(view: self, message: "Mohon menunggu, verifikasi data diri Anda sedang kami proses", title: "Sukses", showCancelBtn: false, rightBtnTitle: "KEMBALI KE BERANDA", leftBtnTitle: "", rightBtnAction: { _ in
                    self.navigationController?.popToRootViewController(animated: true)
                }, leftBtnAction: { _ in
                    
                })
            }, failure: { (error) in
                guard let message = error.message else {
                    return
                }
                AlertView.showAlert(view: self, message: message, title: "Maaf")
            })
        }
//        dismiss(animated: true, completion: {
//            guard let capturedPhoto = self.imagePreview.image else { return }
//            self.delegate?.getCapturedPhoto(image: capturedPhoto)
//        })
    }
    
}

@available(iOS 10, *)
extension CameraViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let photoSampleBuffer = photoSampleBuffer {
            let photoData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
            let image = UIImage(data: photoData!)
            self.toggleFlash(on: false)
            self.imagePreview.image = image
            self.setupBttmView(isDisableCapture: true)
        }
    }
    
}
