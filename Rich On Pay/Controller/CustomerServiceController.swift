//
//  CustomerServiceController.swift
//  Rich On Pay
//
//  Created by Edison on 1/14/19.
//  Copyright © 2019 ROP. All rights reserved.
//

import UIKit

class CustomerServiceController: UIViewController {
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var waLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    var csNumber: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDetail()
        self.title = "Customer Service"
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(onTapPhone(_:)))
        self.phoneLbl.addGestureRecognizer(gesture)
        self.phoneLbl.isUserInteractionEnabled = true
    }
    
    @objc func onTapPhone(_ sender: UITapGestureRecognizer) {
        if csNumber == nil {
            return
        }
        if let url = URL(string: "tel://\(csNumber!)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    func getDetail() {
        API.request(API.Service.contactUs, showLoading: false, success: { (response: APIResponse) in
            DispatchQueue.main.async {
                guard let data = response.data else {
                    return
                }
                self.csNumber = data.phone
                self.noteLbl.text = data.cs
                self.emailLbl.text = data.email
                self.phoneLbl.text = data.phone
                self.waLbl.text = data.wa
            }
        }) { (error) in
            
        }
    }
    
}

