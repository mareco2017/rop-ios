//
//  SignUpViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/17/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var referralBtn: UIButton!
    @IBOutlet weak var errorNameLbl: UILabel!
    @IBOutlet weak var errorEmailLbl: UILabel!
    @IBOutlet weak var errorPhoneLbl: UILabel!
    @IBOutlet weak var errorPasswLbl: UILabel!
    @IBOutlet weak var errorCodeSponsorLbl: UILabel!
    @IBOutlet weak var errorCodeLeaderLbl: UILabel!
    @IBOutlet weak var termsLbl: UILabel!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var codeCTHeight: NSLayoutConstraint!
    @IBOutlet weak var sponsorCodeTF: UITextField!
    @IBOutlet weak var leaderCodeLbl: UITextField!
    var hasReferral = 0
    var acceptTerms = false
    let underlineText = "Syarat".localized
    let secondUnderlineText = "Kebijakan Privasi".localized
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTF.delegate = self
        phoneNumberTF.addTarget(self, action: #selector(phoneNumberTextFieldDidChanged(_:)), for: .editingChanged)
        emailTF.addTarget(self, action: #selector(emailTextFieldDidChanged(_:)), for: .editingChanged)
        passwordTF.addTarget(self, action: #selector(passwordTextFieldDidChanged(_:)), for: .editingChanged)
        nameTF.addTarget(self, action: #selector(nameTextFieldDidChanged(_:)), for: .editingChanged)
        sponsorCodeTF.addTarget(self, action: #selector(sponsorTextFieldDidChanged(_:)), for: .editingChanged)
        leaderCodeLbl.addTarget(self, action: #selector(leaderTextFieldDidChanged(_:)), for: .editingChanged)
        self.prepareTermsAttributedText()
    }
    
    private func prepareTermsAttributedText() {
        let normalText = "Dengan mendaftar Anda telah setuju dengan\n".localized
        let range = NSRange(location: normalText.count,length: underlineText.count)
        let secondRange = NSRange(location: normalText.count + underlineText.count + 3,length: secondUnderlineText.count)
        let attributedText = NSMutableAttributedString(string:  "\(normalText)\(underlineText) " + "&" + " \(secondUnderlineText)", attributes: [NSAttributedStringKey.font:UIFont(name: "Roboto-Regular", size: 12)!])
        attributedText.addAttribute(.foregroundColor, value: UIColor.primary, range: range)
        attributedText.addAttribute(.foregroundColor, value: UIColor.primary, range: secondRange)
        termsLbl.attributedText = attributedText
        let termsGesture = UITapGestureRecognizer()
        termsGesture.addTarget(self, action: #selector(termsLabelTapped(_:)))
        termsLbl.isUserInteractionEnabled = true
        termsLbl.addGestureRecognizer(termsGesture)
    }
    
    @objc func termsLabelTapped(_ sender: UITapGestureRecognizer) {
        let text = (termsLbl.text)!
        let tncText = (text as NSString).range(of: underlineText)
        let privacyPolicyText = (text as NSString).range(of: secondUnderlineText)
        
        if sender.didTapAttributedTextInLabel(label: termsLbl, inRange: tncText) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebDocumentViewController") as! WebDocumentViewController
            nextViewController.url = "/terms-conditions"
            nextViewController.title = "Syarat dan Ketentuan"
            nextViewController.showBackBtn = true
            let nav = UINavigationController(rootViewController: nextViewController)
            self.show(nav, sender: nil)
        }
        else if sender.didTapAttributedTextInLabel(label: termsLbl, inRange: privacyPolicyText) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebDocumentViewController") as! WebDocumentViewController
            nextViewController.url = "/privacy-policy"
            nextViewController.showBackBtn = true
            nextViewController.title = "Kebijakan Privasi"
            let nav = UINavigationController(rootViewController: nextViewController)
            self.show(nav, sender: nil)
        }
    }
    
    @IBAction func onClickClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func emailTextFieldDidChanged(_ sender: UITextField) {
        errorEmailLbl.text = ""
    }
    
    @IBAction func nameTextFieldDidChanged(_ sender: UITextField) {
        errorNameLbl.text = ""
    }
    
    @IBAction func passwordTextFieldDidChanged(_ sender: UITextField) {
        errorPasswLbl.text = ""
    }
    
    @IBAction func leaderTextFieldDidChanged(_ sender: UITextField) {
        errorCodeLeaderLbl.text = ""
    }
    
    @IBAction func sponsorTextFieldDidChanged(_ sender: UITextField) {
        errorCodeSponsorLbl.text = ""
    }
    
    @IBAction func phoneNumberTextFieldDidChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        if text == "0" {
            sender.attributedText = getAttributedText(text: "+62 ")
        }
        else if text.contains("+62 ") && text.count == 4 {
            sender.text = ""
        }
        else if text.count >= 1 && !text.contains("+62 ") {
            var newText = text.replacingOccurrences(of: "+62", with: "")
            newText = newText.replacingOccurrences(of: "+", with: "")
            sender.attributedText = getAttributedText(text: "+62 " + newText)
        }
        errorPhoneLbl.text = ""
    }
    
    func getAttributedText(text: String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: text)
        let range = NSRange(location: 0, length: 3)
        attributedText.addAttribute(.foregroundColor, value: UIColor.lightGray, range: range)
        return attributedText
    }
    
    @IBAction func onClickRegister(_ sender: Any) {
        if !acceptTerms {
            AlertView.showAlert(view: self, message: "Please accept our terms", title: "Sorry")
        }
        else if nameTF.text?.count == 0 {
            AlertView.showAlert(view: self, message: "Please enter your full name", title: "Sorry")
        }
        else if phoneNumberTF.text?.count == 0 {
            AlertView.showAlert(view: self, message: "Please enter your phone number", title: "Sorry")
        }
        else if emailTF.text?.count == 0 {
            AlertView.showAlert(view: self, message: "Please enter your email address", title: "Sorry")
        }
        else if phoneNumberTF.text!.contains("+62 0") {
            AlertView.showAlert(view: self, message: "Your phone number format is invalid", title: "Sorry")
        }
        else if !emailTF.text!.isValidEmail {
            AlertView.showAlert(view: self, message: "Please enter a valid email address".localized, title: "Sorry".localized)
        }
        else if passwordTF.text?.count == 0 {
            AlertView.showAlert(view: self, message: "Please enter your password", title: "Sorry")
        }
        else if passwordTF.text! != confirmPasswordTF.text! {
            AlertView.showAlert(view: self, message: "Password not match", title: "Sorry")
        }
        else {
            if hasReferral == 1 {
                if leaderCodeLbl.text?.count == 0 {
                    AlertView.showAlert(view: self, message: "Please input your leader code", title: "Sorry")
                }
                else if sponsorCodeTF.text?.count == 0 {
                    AlertView.showAlert(view: self, message: "Please input your sponsor code", title: "Sorry")
                }
                else {
                    register()
                }
            }
            else {
                AlertView.showAlert(view: self, message: "Kode referal hanya dapat diisi pada saat registrasi dan tidak dapat diubah lagi.", title: "Apakah anda yakin tidak memiliki kode referral?", showCancelBtn:true, rightBtnTitle:"Tidak ada", leftBtnTitle: "Ada", rightBtnAction: {_ in
                    self.register()
                }) { _ in
                    self.onClickReferral(self.referralBtn)
                }
                
            }
        }
    }
    
    func showErrorLbl(error: Errors) {
        if let name = error.name {
            if name.count > 0 {
                self.errorNameLbl.text = name[0]
            }
        }
        else {
            self.errorNameLbl.text = ""
        }
        if let email = error.email {
            if email.count > 0 {
                self.errorEmailLbl.text = email[0]
            }
        }
        else {
            self.errorEmailLbl.text = ""
        }
        if let password = error.password {
            if password.count > 0 {
                self.errorPasswLbl.text = password[0]
            }
        }
        else {
            self.errorPasswLbl.text = ""
        }
        if let phone = error.phone {
            if phone.count > 0 {
                self.errorPhoneLbl.text = phone[0]
            }
        }
        else {
            self.errorPhoneLbl.text = ""
        }
        if let leader = error.leaderCode {
            if leader.count > 0 {
                self.errorCodeLeaderLbl.text = leader[0]
            }
        }
        else {
            self.errorCodeLeaderLbl.text = ""
        }
        if let sponsor = error.sponsorCode {
            if sponsor.count > 0 {
                self.errorCodeSponsorLbl.text = sponsor[0]
            }
        }
        else {
            self.errorCodeSponsorLbl.text = ""
        }
    }
    
    func register() {
        API.request(API.Service.register(pN: self.phoneNumberTF.text!.replacingOccurrences(of: " ", with: ""), email: self.emailTF.text!, name: self.nameTF.text!
            , password: self.passwordTF.text!, paswConfirm: self.confirmPasswordTF.text!, hasRefferal: self.hasReferral, sponsorCode: self.sponsorCodeTF.text!, leaderCode: self.leaderCodeLbl.text!), showLoading: true, success: { (response: APIResponse) in
                guard let data = response.data else {
                    return
                }
                API.token = data.token
                Defaults[.user] = data.user
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterVerificationCodeViewController") as! EnterVerificationCodeViewController
                vc.isFromRegister = true
                vc.credential = data.user.phoneNumber
                self.show(vc, sender: nil)
        }) { (error) in
            DispatchQueue.main.async {
                if error.message == "Sponsor user not found!" {
                    AlertView.showAlert(view: self, message: "Kode Sponsor tidak ditemukan!", title: "Maaf")
                    return
                }
                else if error.message == "Leader user not found!" {
                    AlertView.showAlert(view: self, message: "Kode Leader tidak ditemukan!", title: "Maaf")
                    return
                }
                else {
                    
                }
                if let errors = error.errors {
                    self.showErrorLbl(error: errors)
                }
            }
        }
    }
    
    func createWallet() {
        
    }
    
    @IBAction func termsBtnClicked(_ sender: UIButton) {
        sender.tag = sender.tag == 0 ? 1 : 0
        acceptTerms = sender.tag == 1
        sender.setImage(UIImage(named: sender.tag == 0 ? "ic_uncheck" : "ic_check"), for: .normal)
    }
    
    
    @IBAction func onClickEyeButton(_ sender: UIButton) {
        sender.tag = sender.tag == 0 ? 1 : 0
        passwordTF.isSecureTextEntry = sender.tag != 1
        sender.setImage(UIImage(named: sender.tag == 0 ? "ic_eye" : "ic_eye_hide"), for: .normal)
    }
    
    
    @IBAction func onClickReferral(_ sender: UIButton) {
        sender.tag = sender.tag == 0 ? 1 : 0
        hasReferral = sender.tag
        self.codeView.isHidden = sender.tag == 0
        self.codeCTHeight.constant = sender.tag == 0 ? 0 : 136
        sender.setImage(UIImage(named: sender.tag == 0 ? "ic_uncheck" : "ic_check"), for: .normal)
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        if sender.tag == 0 {
            self.leaderCodeLbl.text = ""
            self.sponsorCodeTF.text = ""
        }
    }
    
    
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if text == "+62 " {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b") == -92
            if (isBackSpace) {
                textField.text = ""
                return true
            }
        }
        return string.isPhoneNumberFiltered
    }
    
}
