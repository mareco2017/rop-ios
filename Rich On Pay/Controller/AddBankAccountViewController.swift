//
//  AddBankViewController.swift
//  Mareco
//
//  Created by Edison on 12/8/17.
//  Copyright © 2017 Mareco. All rights reserved.
//

import UIKit
import PopupDialog

protocol AddBankAccountDelegate {
    func bankAccountAdded(bankAccount: BankAccount)
}

class AddBankAccountViewController: UIViewController {
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var selectBankLbl: UILabel!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var accountNumbTF: UITextField!
    @IBOutlet weak var bankNameTitleLbl: UILabel!
    @IBOutlet weak var accountNameTitleLbl: UILabel!
    @IBOutlet weak var accountNoTitleLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    
    fileprivate var banks = [Bank]()
    fileprivate var selectedBank: Bank?
    var bankPopup: PopupDialog?
    var delegate: AddBankAccountDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(selectBank))
        self.selectBankLbl.isUserInteractionEnabled = true
        self.selectBankLbl.addGestureRecognizer(gesture)
        self.title = "Add Bank Account".localized
        accountNumbTF.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        nameTF.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        setUpView()
        callGetBanksAPI()
    }
    
    func setUpView() {
        self.bankNameTitleLbl.text = "Bank Name".localized
        self.selectBankLbl.text = "Select Bank".localized
        self.accountNameTitleLbl.text = "Name".localized
        self.nameTF.placeholder = "Enter your name".localized
        self.accountNoTitleLbl.text = "Account Number".localized
        self.accountNumbTF.placeholder = "Insert Account Number".localized
        self.noteLbl.text = "Note".localized + ": " + "Name must be the same according to your Bank Account Details.".localized
        self.saveBtn.setTitle("Save".localized.uppercased(), for: .normal)
        
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        if let bank = self.selectedBank {
            if self.nameTF.text!.isEmptyField {
                AlertView.showAlert(view: self, message: "Please input your account name".localized, title: "")
                return
            }
            else if self.accountNumbTF.text!.isEmptyField {
                AlertView.showAlert(view: self, message: "Please insert your account number".localized, title: "")
                return
            }
            else if self.accountNumbTF.text!.count <= 6 {
                AlertView.showAlert(view: self, message: "Account number not valid!".localized, title: "")
                return
            }
            API.request(API.Service.createNewBankAccount(bankId: bank.id, accountName: nameTF.text!, accountNo: accountNumbTF.text!), showLoading: true, success: {
                (response: APIResponse) in
                AlertView.showAlert(view: self, message: "", title: "Success", rightBtnAction: { _ in
                    guard let bankAccount = response.data?.bankAccount else { return }
                    DispatchQueue.main.async {
                        self.delegate?.bankAccountAdded(bankAccount: bankAccount)
                        self.navigationController?.popViewController(animated: true)
                    }
                }, leftBtnAction: { _ in })
            }, failure: {
                (error) in
                let error = error.message ?? "Unknown Error Occurred".localized
                AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
            })

        }
        else {
            AlertView.showAlert(view: self, message: "Please select your Bank".localized, title: "")
        }
    }
    
    @objc func editingChanged(_ sender: UITextField) {
     //   self.enableSaveBtn()
    }
    
    func enableSaveBtn() {
        if !self.accountNumbTF.text!.isEmptyField && !self.nameTF.text!.isEmptyField && self.selectedBank != nil {
            self.saveBtn.isUserInteractionEnabled = true
            self.saveBtn.backgroundColor = UIColor.primary
        }
        else {
            self.saveBtn.isUserInteractionEnabled = false
            self.saveBtn.backgroundColor = UIColor.darkGray
        }
    }
    
    func callGetBanksAPI() {
        API.request(API.Service.getBanks, showLoading: false, success: { (response: APIResponse) in
            guard let banks = response.data?.banks else { return }
            self.banks = banks
        })
    }
    
    @objc func selectBank() {
        let dialogVC = PickerViewController(nibName: "PickerView", bundle: nil)
        bankPopup = PopupDialog(viewController: dialogVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false)
        dialogVC.tableView.delegate = self
        dialogVC.tableView.dataSource = self
        dialogVC.fitHeight()
        let buttonOne = DefaultButton(title: "Close".localized.uppercased()) {
            
        }
        buttonOne.titleColor = UIColor.primary
        buttonOne.titleFont = UIFont(name: "SanFransiscoText-SemiBold", size: 16.interfaceIdiomSize(size: 5))
        bankPopup?.addButtons([buttonOne])
        self.present(bankPopup!, animated: true, completion: nil)
    }
    
    
}

extension AddBankAccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.banks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PickerTableCell
        cell.pickerLbl.text = self.banks[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectBankLbl.text = self.banks[indexPath.row].name
        self.selectBankLbl.textColor = UIColor.black
        self.selectedBank = self.banks[indexPath.row]
        self.bankPopup?.dismiss()
    }
    
}



