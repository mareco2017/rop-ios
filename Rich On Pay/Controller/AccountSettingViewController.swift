//
//  AccountSettingViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/27/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class AccountSettingViewController: UIViewController {
    
    var menu = ["Ganti Kata Sandi", "Ganti Nomor Handphone", "Ganti Email", "Rekening Bank"]
    var icons = ["baseline_lock", "baseline_phone", "baseline_mail", "baseline_account"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pengaturan Akun"
    }
    
    func requestOTP(type: Int) {
        if type == 0 {
            
        }
    }
}

extension AccountSettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AccountSettingTableCell
        cell.iconIV.image = UIImage(named: icons[indexPath.row])
        cell.nameLbl.text = menu[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as!ChangePasswordViewController
            self.show(vc, sender: nil)
            break
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "BankAccountsViewController")
            self.show(vc!, sender: nil)
            break
        default:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangeEmailPhoneViewController") as! ChangeEmailPhoneViewController
            vc.isVerifyEmail = indexPath.row == 2
            self.show(vc, sender: nil)
            break
        }
    }
}

class AccountSettingTableCell: UITableViewCell {
    
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
}



