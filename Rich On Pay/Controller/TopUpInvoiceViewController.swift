//
//  TopUpInvoiceViewController.swift
//  Rich On Pay
//
//  Created by Edison on 10/10/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class TopUpInvoiceViewController: UIViewController {
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var completeTransactionButton: UIButton!
    @IBOutlet weak var expiryDurationLabel: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var transactionCodelbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var bankAccountNameLbl: UILabel!
    @IBOutlet weak var bankNumber: UILabel!
    @IBOutlet weak var bankLogoIV: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var expiryTimeLabel: UILabel!
    
    var upgradeRequest: UpgradeRequest!
    var bankAccount: BankAccount!
    var timer = Timer()
    var isTopup = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pembayaran"
        if isTopup {
            transactionCodelbl.text = "Kode Transaksi: \(upgradeRequest.referenceNumber!)"
            if let orderDetails = upgradeRequest.orderDetails {
                if orderDetails.count > 0 {
                    priceLbl.text = "Isi Saldo Rp \(orderDetails[0].price.formattedWithSeparator)"
                    self.totalAmountLbl.text = "Rp \(orderDetails[0].subtotal.formattedWithSeparator)"
                }
            }
        }
        else {
            self.totalAmountLbl.text = "Rp \(upgradeRequest.price.formattedWithSeparator)"
            let package = upgradeRequest.package == 1 ? "SILVER" : (upgradeRequest.package == 2 ? "GOLD" : "PLATINUM")
            priceLbl.text = "\(package) Rp \(upgradeRequest.price.formattedWithSeparator)"
            transactionCodelbl.text = "Kode Transaksi: \(upgradeRequest.code!)"
        }
        self.bankNumber.text = bankAccount.accountNo
        self.bankAccountNameLbl.text = bankAccount.accountName
        self.bankLogoIV.sd_setImage(with: URL(string: bankAccount.bank.logo))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        dateLbl.text = "\(dateFormatter.string(from: upgradeRequest.createdAt))"
        dateFormatter.dateFormat = "hh:mm a"
        expiryTimeLabel.text = "Transfer sebelum" + " \(dateFormatter.string(from: upgradeRequest.expireAt))"
        startTimer()
        if upgradeRequest.status != 1 {
            completeTransactionButton.isHidden = true
            infoLbl.isHidden = true
            cancelBtn.isHidden = true
        }
        switch upgradeRequest.status {
        case 5,4:
            self.statusLbl.text = "BATAL"
            self.statusLbl.backgroundColor = UIColor.primary
            break
        case 1:
            self.statusLbl.text = "MENUNGGU"
            self.statusLbl.backgroundColor = UIColor.yellowR
            break
        case 6:
            self.statusLbl.text = "REFUND"
            self.statusLbl.backgroundColor = UIColor.purpleR
            break
        case 3:
            self.statusLbl.text = "BERHASIL"
            self.statusLbl.backgroundColor = UIColor.greenR
            break
        case 2:
            self.statusLbl.text = "DIPROSES"
            self.statusLbl.backgroundColor = UIColor.blueR
            break
        default:
            break
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDownDate), userInfo: nil, repeats: true)
    }
    
    @objc func countDownDate() {
        let currentDate = Date()
        let expiryDate = upgradeRequest.expireAt!
        let diffDateComponents = Calendar.current.dateComponents(Set(arrayLiteral: .hour, .minute, .second), from: currentDate, to: expiryDate)
        var hoursLeft = diffDateComponents.hour ?? 0
        hoursLeft = hoursLeft > 0 ? hoursLeft : 0
        var minutesLeft = diffDateComponents.minute ?? 0
        minutesLeft = minutesLeft > 0 ? minutesLeft : 0
        var secondsLeft = diffDateComponents.second ?? 0
        secondsLeft = secondsLeft > 0 ? secondsLeft : 0
        
        let expiryDateText = "\(hoursLeft) Jam dan \(minutesLeft) Menit lagi"
        expiryDurationLabel.text = expiryDateText
        completeTransactionButton.isUserInteractionEnabled = hoursLeft + minutesLeft + secondsLeft > 0
        completeTransactionButton.alpha = hoursLeft + minutesLeft + secondsLeft > 0 ? 1.0 : 0.0
    }
    
    @IBAction func finishPayBtnClicked(_ sender: Any) {
        if isTopup {
            API.request(API.Service.topUpPaid(orderId: self.upgradeRequest.id), showLoading: true, success: {
                (response: APIResponse) in
//                self.reloadTransactionData()
                DispatchQueue.main.async {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }, failure: {
                (error) in
                let error = error.message ?? "Unknown Error Occurred"
                AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
            })
        }
        else {
            API.request(API.Service.payUpgrade(id: self.upgradeRequest.id, packages: self.upgradeRequest.package, selectedBank: self.bankAccount.id), showLoading: true, success: { (response: APIResponse) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshTransaction"), object: nil)
                self.navigationController?.popToRootViewController(animated: true)
            }) { (error) in
                guard let message = error.message else {
                    return
                }
                AlertView.showAlert(view: self, message: message, title: "Maaf")
            }
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
            let messageText = "Apakah anda ingin membatalkan transaksi ini ?"
            let alertController = UIAlertController(title: "Konfirmasi", message: messageText, preferredStyle: .alert)
            let alertRightAction = UIAlertAction(title: "Benar, batalkan", style: .default, handler: {
                (completion) in
                 if self.isTopup {
                     self.callCancelTopUpAPI()
                }
                 else {
                    self.callCancelUpgradeAPI()
                }
            })
            let alertLeftAction = UIAlertAction(title: "Tutup", style: .cancel, handler: nil)
            alertController.addAction(alertRightAction)
            alertController.addAction(alertLeftAction)
            present(alertController, animated: true, completion: nil)
    }
    
    func callCancelUpgradeAPI() {
        API.request(API.Service.cancelUpgrade(id: self.upgradeRequest.id, packages: self.upgradeRequest.package, selectedBank: self.bankAccount.id), showLoading: true, success: { (response: APIResponse) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshTransaction"), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }) { (error) in
            guard let message = error.message else {
                return
            }
            AlertView.showAlert(view: self, message: message, title: "Maaf")
        }
    }
    
    func callCancelTopUpAPI() {
        API.request(API.Service.cancelTopUp(orderId: self.upgradeRequest.id), showLoading: true, success: {
            (response: APIResponse) in
            DispatchQueue.main.async {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }, failure: {
            (error) in
            self.completeTransactionButton.isUserInteractionEnabled = true
            self.completeTransactionButton.alpha = 1.0
            let error = error.message ?? "Unknown Error Occurred"
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
}

