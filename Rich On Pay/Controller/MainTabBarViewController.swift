//
//  MainTabBarViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/17/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class MainTabarViewController: UITabBarController, UITabBarControllerDelegate {
    
    var perviousIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let tabViewController1 = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController")
        let tabViewController2 = self.storyboard!.instantiateViewController(withIdentifier: "TransactionChildViewController")
//        let tabViewController3 = self.storyboard!.instantiateViewController(withIdentifier: "TESTING")
        let tabViewController5 = self.storyboard!.instantiateViewController(withIdentifier: "AccountViewController")
        
        tabViewController1.tabBarItem = UITabBarItem(
            title: "Beranda",
            image:UIImage(named: "ic_tab_home") ,
            tag:2)
        tabViewController2.tabBarItem = UITabBarItem(
            title: "Pesanan",
            image: UIImage(named: "ic_tab_order"),
            tag: 1)
   
        tabViewController5.tabBarItem = UITabBarItem(
            title: "Akun",
            image:UIImage(named: "ic_tab_account") ,
            tag:3)
        
        let navigationController1 = UINavigationController(rootViewController: tabViewController1)
        navigationController1.hidesBottomBarWhenPushed = true
        let navigationController2 = UINavigationController(rootViewController: tabViewController2)
        navigationController2.hidesBottomBarWhenPushed = true
        let navigationController5 = UINavigationController(rootViewController: tabViewController5)
        navigationController5.hidesBottomBarWhenPushed = true
        
        let controllers = [navigationController1, navigationController2, navigationController5]
        
        self.viewControllers = controllers
        
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        self.perviousIndex = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        if tabBarController.selectedIndex == 2{
//            tabBarController.selectedIndex = self.perviousIndex
//            self.showAlertProgress()
//        }
    }
    
    
    
}
