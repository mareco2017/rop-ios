//
//  MutationViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/27/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MutationViewController: MXSegmentedPagerController, MXPagerViewDelegate, MXPagerViewDataSource  {
    
    var viewControllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        segmentedPager.backgroundColor = .white
        segmentedPager.delegate = self
        segmentedPager.dataSource = self
        for i in 0 ..< 2 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "TransactionChildViewController") as! TransactionChildViewController
            vc.id = i
            self.viewControllers.append(vc)
        }
        // Segmented Control customization
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        segmentedPager.segmentedControl.backgroundColor = .white
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.lightGrayR, NSAttributedStringKey.font : UIFont(name: "Roboto-Medium", size: 14.0)!]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.primary, NSAttributedStringKey.font : UIFont(name: "Roboto-Medium", size: 14.0)!]
        segmentedPager.segmentedControlPosition = .top
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.primary
        segmentedPager.segmentedControl.selectionIndicatorHeight = 2
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return ["PENDING", "BERHASIL"][index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
        //        print("progress \(parallaxHeader.progress)")
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        return viewControllers[index]
    }
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return viewControllers.count
    }
    
    func numberOfPages(in pagerView: MXPagerView) -> Int {
        return viewControllers.count
    }
    
    func pagerView(_ pagerView: MXPagerView, viewForPageAt index: Int) -> UIView? {
        return UIView()
    }
    
}
