//
//  TopUpAmountSelectionViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/24/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import SwiftyUserDefaults
import UIKit

class TopUpAmountSelectionViewController: UIViewController {
    
    @IBOutlet weak var infoWrapperView: UIView!
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var amountListCollectionView: UICollectionView!
    @IBOutlet weak var amountListCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var upgradeAccountButton: UIButton!
    @IBOutlet weak var maximumBalanceLabel: UILabel!
    @IBOutlet weak var upgradeAccountButtonTopConstraint: NSLayoutConstraint!
    
    var selectedAmountTag: Int!
    var amountList = [TopUp]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bank Transfer".localized
//        infoWrapperView.layer.cornerRadius = 2
//        infoWrapperView.layer.borderColor = UIColor(hex: "78CAFF").cgColor
//        infoWrapperView.layer.borderWidth = 1
        amountTextField.addTarget(self, action: #selector(textFieldDidEdited(_:)), for: .editingChanged)
        amountListCollectionView.delegate = self
        amountListCollectionView.dataSource = self
        populateCollectionView()
        updateBalanceLabel()
        proceedButton.setTitle("Proceed To Payment".localized, for: .normal)
        toggleProceedButton(enabled: false)
        callGetTopUpListAPI()
        if let user = Defaults[.user] {
            if let wallets = user.wallets {
                if wallets.count > 1 {
                    currentBalanceLabel.text = "Rp \(wallets[0].balance.formattedWithSeparator)"
                }
            }
        }
    }
    
    func populateCollectionView() {
        amountListCollectionView.reloadData()
        let collectionViewNewHeight = round(Double(amountList.count) / 3.0) * 48
        amountListCollectionViewHeight.constant = CGFloat(collectionViewNewHeight)
    }
    
    func showUpgradeAccountScreen() {
//        let nextViewController = UIStoryboard.loadUpgradeAccountMainViewController()
//        let navigationController = UINavigationController(rootViewController: nextViewController)
//        show(navigationController, sender: nil)
    }
    
    func showUpgradeAccountPendingScreen() {
//        let nextViewController = UIStoryboard.loadUpgradeAccountPendingViewController()
//        let navigationController = UINavigationController(rootViewController: nextViewController)
//        show(navigationController, sender: nil)
    }
    
    func callGetTopUpListAPI() {
        API.request(API.Service.getTopUpList, showLoading: true, success: {
            (response: APIResponse) in
            guard let data = response.data else { return }
            if let topUpData = data.topUp {
                self.amountList = topUpData
                self.populateCollectionView()
            }

        }, failure: { (error) in
            if let error = error.message {
                AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
            }
            else {
                AlertView.showAlert(view: self, message: "Unknown Error Occurred".localized, title: "Sorry".localized)
            }
        })
    }
    
    fileprivate func updateBalanceLabel() {
        guard let user = Defaults[.user] else { return }
//        guard let userBalance = user.balances?.primary else { return }
//        let balance = NSMutableAttributedString(string: "Rp \(userBalance.formattedWithSeparator)")
//        let rpRange = NSRange(location: 0,length: 3)
//        let balanceRange = NSRange(location:3,length: balance.length - 3)
//        balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "SanFranciscoText-Semibold", size: 16.interfaceIdiomSize(size: 5))!, range: rpRange)
//        balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "SanFranciscoText-Bold", size: 22.interfaceIdiomSize(size: 5))!, range: balanceRange)
//        currentBalanceLabel.attributedText = balance
    }
    
    @IBAction func textFieldDidEdited(_ sender: UITextField) {
        if selectedAmountTag != nil {
            selectedAmountTag = nil
            amountListCollectionView.reloadData()
        }
        let amountValue = sender.text!.toAmtFormat
        toggleProceedButton(enabled: amountValue >= 10000)
    }
    
    fileprivate func highlightSelectedCellView(cell: TopUpAmountCollectionViewCell) {
        cell.titleLabel.textColor = UIColor.white
        cell.roundedCornerView.backgroundColor = UIColor.primary
        
        updateAmountTextFieldWith(text: (cell.titleLabel.text)!)
    }
    
    fileprivate func unhighlightPreviousCellView(cell: TopUpAmountCollectionViewCell) {
        cell.titleLabel.textColor = UIColor.primary
        cell.roundedCornerView.backgroundColor = UIColor(hex: "E8E9EA")
    }
    
    fileprivate func updateAmountTextFieldWith(text: String) {
        let amountText = text.replacingOccurrences(of: "Rp ", with: "")
        amountTextField.text = amountText
        
        toggleProceedButton(enabled: true)
    }
    
    func toggleProceedButton(enabled: Bool) {
        proceedButton.isUserInteractionEnabled = enabled
        proceedButton.backgroundColor = enabled ? UIColor.primary : UIColor(hex: "8A8A8F")
    }
    
    @IBAction func proceedButtonTapped(_ sender: UIButton) {
        callGetPaymentBanksAPI()
    }
    
    func callGetPaymentBanksAPI() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopUpSelectBankViewController") as! TopUpSelectBankViewController
        vc.amount = self.amountTextField.text!.toAmtFormat
        vc.isTopup = true
        self.show(vc, sender: nil)
//        API.request(API.Service.getPaymentBankAccounts, showLoading: true, success: { (response: APIResponse) in
//            let nextVC = UIStoryboard.loadTopUpBankSelectionViewController()
//            guard let data = response.data else { return }
//            guard let paymentBankAccounts = data.paymentBankAccounts else
//            {
//                AlertView.showAlert(view: self, message: "Payment Bank Accounts Not Found!", title: "Sorry".localized)
//                return
//            }
//            nextVC.paymentBankAccountList = paymentBankAccounts
//            nextVC.selectedAmount = self.amountTextField.text!.toAmtFormat
//            self.show(nextVC, sender: nil)
//        }) { (error) in
//            AlertView.showAlert(view: self, message: error.message ?? "Unknown Error Occurred".localized, title: "Sorry".localized)
//        }
    }
    
}

extension TopUpAmountSelectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.amountList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TopUpAmountCollectionViewCell
        
        cell.titleLabel.text = "Rp \(amountList[indexPath.item].amount.formattedWithSeparator)"
        cell.titleLabel.textColor = UIColor.primary
        cell.roundedCornerView.backgroundColor = UIColor(hex: "E8E9EA")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedAmountTag != nil {
            let previousIndexPath = IndexPath(item: selectedAmountTag, section: 0)
            let cell = collectionView.cellForItem(at: previousIndexPath) as! TopUpAmountCollectionViewCell
            unhighlightPreviousCellView(cell: cell)
        }
        let cell = collectionView.cellForItem(at: indexPath) as! TopUpAmountCollectionViewCell
        highlightSelectedCellView(cell: cell)
        selectedAmountTag = indexPath.item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width / 3 - 8
        return CGSize(width: itemWidth.interfaceIdiomSize(size: 10) , height: 40.interfaceIdiomSize(size: 10))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}

class TopUpAmountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var roundedCornerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
}

