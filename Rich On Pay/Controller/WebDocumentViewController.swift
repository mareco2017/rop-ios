//
//  WebDocumentViewController.swift
//  Mareco
//
//  Created by Edy on 6/1/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class WebDocumentViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var currentWebView: UIWebView!
    var url: String?
    var baseUrl = "/terms-conditions"
    var showBackBtn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "Jaringan"
        self.currentWebView.delegate = self
        self.clearWebviewCached()
        let requestedUrl = URLRequest(url:  URL(string: "https://richonpay.com/webview\(url!)")!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        self.currentWebView.loadRequest(requestedUrl)
        self.currentWebView.scalesPageToFit = false
        self.currentWebView.isMultipleTouchEnabled = false
        self.currentWebView.scrollView.bounces = false
        if showBackBtn {
            self.addBackButton()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        if progressView == nil {
//            progressView = HorizontalProgressView(frame: CGRect(x: 0, y: navigationController?.navigationBar.frame.maxY ?? 0, width: self.view.frame.width, height: 2))
//            progressView.startAnimation()
//            self.view.addSubview(progressView)
//        }
    }
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
//        backButton.setImage(UIImage(named: "BackButton.png"), for: .normal) // Image can be downloaded from here below link
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func clearWebviewCached() {
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
//        progressView.stopAnimation()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        progressView.stopAnimation()
        AlertView.showAlert(view: self, message: error.localizedDescription , title: "Error")
    }
    
}
