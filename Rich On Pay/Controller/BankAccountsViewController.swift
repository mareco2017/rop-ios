//
//  BankManageViewController.swift
//  Mareco
//
//  Created by Edison on 12/8/17.
//  Copyright © 2017 Mareco. All rights reserved.
//

import UIKit

protocol BankSelectDelegate: class {
    func didSelectBank(bank: BankAccount)
}

class BankAccountsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var isCashierWithdraw = false
    var bankAccounts = [BankAccount]()
    var selectedBankAccount: BankAccount?
    var delegate: BankSelectDelegate?
    var isWithdraw = false
    var amount: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = isWithdraw ? "Bank Accounts" : "My Bank Accounts".localized
        if !isCashierWithdraw {
           callGetBankAccountsAPI()
        }
    }
    
    func callGetBankAccountsAPI() {
        API.request(API.Service.getBankAccountList, showLoading: true, success: {
            (response: APIResponse) in
            guard let bankAccounts = response.data?.bankAccounts else { return }
            self.bankAccounts = bankAccounts
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, failure: {
            (error) in
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddBank" {
            if let vc = segue.destination as? AddBankAccountViewController {
                vc.delegate = self
            }
        }
    }
    
    func callRemoveBankAccountAPI(bankAccountId: Int, index: Int) {
        API.request(API.Service.removeBankAccount(bankAccountId: bankAccountId), showLoading: true, success: {
            (response: APIResponse) in
            self.bankAccounts.remove(at: index)
            self.tableView.reloadData()
        }, failure: {
            (error) in
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    func callSetPrimaryBankAccount(bankAccountId: Int, index: Int) {
        API.request(API.Service.setPrimaryBankAccount(bankAccountId: bankAccountId), showLoading: true, success: {
            (response: APIResponse) in
            for bankAccount in self.bankAccounts {
                bankAccount.isPrimary = 0
            }
            self.bankAccounts[index].isPrimary = 1
            self.tableView.reloadData()
        }, failure: {
            (error) in
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    func loadAddBankAccountTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addBankCell", for: indexPath) as! AddBankAccountTableCell
        cell.addBankAccountLbl.text = "Add Bank Account".localized
        return cell
    }
    
    func loadBankAccountTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BankManageTableCell
        let bankAccount = bankAccounts[indexPath.row]
        cell.accountNameLbl.text = bankAccount.accountName
        if let bank = bankAccount.bank {
            cell.bankLogoIV.sd_setImage(with: URL(string: bank.logo))
            cell.accountNumbLbl.text = "\(bankAccount.accountNo!) / \(bankAccount.bank.abbr!)"
        }
        else if let bankDetail = bankAccount.bankDetail {
            cell.bankLogoIV.sd_setImage(with: URL(string: bankDetail.logo))
            cell.accountNumbLbl.text = "\(bankAccount.accountNo!) / \(bankAccount.bankDetail.abbr!)"
        }
        if !isWithdraw {
            if let selectedBankAccount = self.selectedBankAccount {
                if selectedBankAccount.id == bankAccount.id {
                    cell.accessoryType = .checkmark
                }
                else {
                    cell.accessoryType = .none
                }
            }
            else {
                if bankAccount.isPrimary == 1 {
                    cell.accessoryType = .checkmark
                }
                else {
                    cell.accessoryType = .none
                }
            }
        }
        return cell
    }
}

extension BankAccountsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankAccounts.count + (!isCashierWithdraw ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == bankAccounts.count && !isCashierWithdraw {
            return loadAddBankAccountTableViewCell(indexPath: indexPath)
        }
        return loadBankAccountTableViewCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isWithdraw {
            print(self.amount)
            API.request(API.Service.balanceWithdraw(bankId: self.bankAccounts[indexPath.row].id, amount: self.amount), showLoading: true, success: { (response: APIResponse) in
                AlertView.showAlert(view: self, message: "Bonus anda sedang di proses silahakan tunggu maksimal 2 x 24 jam", title: "Sukses", showCancelBtn:false, rightBtnTitle: "Back", leftBtnTitle: "", rightBtnAction: {_ in
                    self.delegate?.didSelectBank(bank: self.bankAccounts[indexPath.row])
                    self.navigationController?.popViewController(animated: true)
                }, leftBtnAction: { _ in
                    
                })
            }, failure: { (error) in
                guard let message = error.message else {
                    return
                }
                AlertView.showAlert(view: self, message: message, title: "Maaf")
            })
            return
        }
        if indexPath.row == bankAccounts.count {
            self.performSegue(withIdentifier: "toAddBank", sender: nil)
            return
        }
//        if selectedBank != nil {
//            self.delegate?.didSelectBank(bank: bankAccounts[indexPath.row])
//            self.navigationController?.popViewController(animated: true)
//            return
//        }
        if bankAccounts[indexPath.row].isPrimary == 1 {
            return
        }
        let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let setDefaultBtn = UIAlertAction(title: "Set as Default Bank Account".localized, style: .default, handler: { (alert) in
            guard let bankAccountId = self.bankAccounts[indexPath.row].id else { return }
            self.callSetPrimaryBankAccount(bankAccountId: bankAccountId, index: indexPath.row)
        })
//        setDefaultBtn.setValue(UIImage(named: "ic_checked")!.withRenderingMode(.alwaysOriginal) , forKey: "image")
        setDefaultBtn.setValue(UIColor.blueR, forKey: "titleTextColor")
        let removeBtn = UIAlertAction(title: "Delete Bank Account".localized, style: .default) { (alert) in
            guard let bankAccountId = self.bankAccounts[indexPath.row].id else { return }
            self.callRemoveBankAccountAPI(bankAccountId: bankAccountId, index: indexPath.row)
        }
//        removeBtn.setValue(UIImage(named: "ic_trash_1")!.withRenderingMode(.alwaysOriginal) , forKey: "image")
        removeBtn.setValue(UIColor.primary, forKey: "titleTextColor")
        let cancel = UIAlertAction(title: "Cancel".localized, style: .cancel) { (alert) in
            
        }
        cancel.setValue(UIColor.primary, forKey: "titleTextColor")
        alertViewController.addAction(setDefaultBtn)
        alertViewController.addAction(removeBtn)
        alertViewController.addAction(cancel)
        self.present(alertViewController, animated: true, completion: nil)
    }
}

extension BankAccountsViewController: AddBankAccountDelegate {
    func bankAccountAdded(bankAccount: BankAccount) {
        for bank in bankAccounts {
            bank.isPrimary = 0
        }
        bankAccounts.append(bankAccount)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

class BankManageTableCell: UITableViewCell {
    
    @IBOutlet weak var bankLogoIV: UIImageView!
    @IBOutlet weak var accountNameLbl: UILabel!
    @IBOutlet weak var accountNumbLbl: UILabel!
    
}

class AddBankAccountTableCell: UITableViewCell {
    @IBOutlet weak var addBankAccountLbl: UILabel!
    
}
