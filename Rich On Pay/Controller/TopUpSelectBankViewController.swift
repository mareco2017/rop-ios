//
//  TopUpSelectBankViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/24/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class TopUpSelectBankViewController: UIViewController {
    
    var selectedPackages: Int!
    var bankAccounts: [BankAccount] = [BankAccount]()
    var selectedBank: Int?
    var upgradeRequest: UpgradeRequest?
    var isTopup = false
    var amount: Double!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getBanks()
        self.title = "Upgrade Paket"
    }
    
    func getBanks() {
        API.request(API.Service.getPaymentBanks, showLoading: false, success: { (response: APIResponse) in
            guard let banks = response.data?.bankAccounts else {
                return
            }
            self.bankAccounts = banks
            DispatchQueue.main.async {
                 self.collectionView.reloadData()
                self.collectionView.fitContentHeight(footerHeight: 10)
            }
        }) { (error) in
            
        }
    }
    
    @IBAction func payClicked(_ sender: Any) {
        if let user = Defaults[.user] {
            if user.verificationStatus.nric == 2 {
                AlertView.showAlert(view: self, message: "Silahkan upload Identitas Anda terlebih dahulu", title: "Maaf", showCancelBtn: true, rightBtnTitle: "Upload Sekarang", leftBtnTitle: "Batal", rightBtnAction: { _ in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                    self.show(vc, sender: nil)
                }, leftBtnAction: { _ in
                    
                })
                return
            }
            if user.verificationStatus.nric == 0 {
                AlertView.showAlert(view: self, message: "Identitas anda sedang proses peninjauan", title: "Maaf")
                return
            }
        }
        if let selectedBank = self.selectedBank {
            if isTopup {
                API.request(API.Service.requestTopUp(destBankAccountId: self.bankAccounts[selectedBank].id, amount: self.amount), showLoading: true, success: { (response: APIResponse) in
                    guard let data = response.data?.orderRequest else { return }
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopUpInvoiceViewController") as!  TopUpInvoiceViewController
                    vc.bankAccount = self.bankAccounts[selectedBank]
                    vc.upgradeRequest = data
                    vc.isTopup = true
                    self.show(vc, sender: nil)
//                    self.showNextScreen(order: data.order)
                }, failure: {                    (error) in
                    if let error = error.message {
                        AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
                    }
                })
            }
            else {
                API.request(API.Service.upgradeBank(id: (self.upgradeRequest?.id!)!, selectedBank: self.bankAccounts[selectedBank].id), showLoading: true, success: { (response: APIResponse) in
                    guard let upgradeRequest = response.data?.upgradeRequest else {
                        return
                    }
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopUpInvoiceViewController") as!  TopUpInvoiceViewController
                    vc.bankAccount = self.bankAccounts[selectedBank]
                    vc.upgradeRequest = upgradeRequest
                    self.show(vc, sender: nil)
                }) { (error) in
                    guard let message = error.message else {
                        return
                    }
                    AlertView.showAlert(view: self, message: message, title: "Maaf")
                }
            }
        }
        else {
            AlertView.showAlert(view: self, message: "Silahkan Pilih Bank terlebih dahulu", title: "Maaf")
        }
    }
    
}

extension TopUpSelectBankViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bankAccounts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width / 3 - 10, height: self.view.frame.width / 3 - 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as!BankCollectionViewCell
        cell.bankIV.sd_setImage(with: URL(string: self.bankAccounts[indexPath.item].bank.logo))
        cell.containerView.layer.borderWidth = 1
        cell.containerView.layer.borderColor = UIColor.clear.cgColor
        if let selectedBank = self.selectedBank {
            cell.containerView.layer.borderColor = indexPath.item == selectedBank ? UIColor.primary.cgColor : UIColor.clear.cgColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedBank = indexPath.item
        self.collectionView.reloadData()
    }
    
}

class BankCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bankIV: UIImageView!
    
}

