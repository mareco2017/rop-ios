//
//  ChangePasswordViewController.swift
//  Rich On Pay
//
//  Created by Edison on 10/14/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var currentPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmNewPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Kata Sandi"
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        if newPasswordTF.text! != confirmNewPassword.text! {
            AlertView.showAlert(view: self, message: "Password baru tidak cocok!", title: "Maaf")
        }
        else {
            API.request(API.Service.changePassword(password: self.currentPasswordTF.text!, newPassword: self.newPasswordTF.text!, confirmPassword: self.confirmNewPassword.text!), showLoading: true, success: { (response: APIResponse) in
                AlertView.showAlert(view: self, message: "Password berhasil diganti!", title: "Sukses", rightBtnAction: {
                    _ in
                    self.navigationController?.popViewController(animated: true)
                    //                    self.showAccountScreen()
                }, leftBtnAction: { _ in })
            }, failure: { (error) in
                if let errors = error.errors {
                    if let newPasswError = errors.newPassword {
                        if newPasswError.count > 0 {
                            AlertView.showAlert(view: self, message: newPasswError[0], title: "Maaf")
                        }
                    }
                }
                else {
                    AlertView.showAlert(view: self, message: error.message, title: "Maaf")
                }
            })
        }
    }
    
    
}
