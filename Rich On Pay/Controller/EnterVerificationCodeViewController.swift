//
//  EnterVerificationCodeViewController.swift
//  Mareco
//
//  Created by Edy on 22/3/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class EnterVerificationCodeViewController: UIViewController {
    
    @IBOutlet weak var headerTopCT: NSLayoutConstraint!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var credentialLbl: UILabel!
    @IBOutlet var codeLabels: Array<UILabel> = []
    @IBOutlet weak var hiddenCodeTextField: UITextField!
    @IBOutlet weak var tappableView: UIView!
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var wrapperViewHeight: NSLayoutConstraint!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    
    var currentPosition = 0
    var timer = Timer()
    var cooldownTime = 60
    var numberOfResend = 0
    var isFromLogin = true
    var credential: String!
    var willInitiateResendCode = false
    var forVerifyEmail = false
    var isFromRegister = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenCodeTextField.becomeFirstResponder()
        hiddenCodeTextField.delegate = self
        hiddenCodeTextField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        indicatorView.hidesWhenStopped = true
        credentialLbl.text = self.credential
        if UIDevice().screenType == .iPhone4_4S {
            for label in codeLabels {
                label.layer.cornerRadius = 8
                label.layer.masksToBounds = true
                label.layer.borderWidth = 1
                label.font = UIFont(name: "SFProText-Semibold", size: 16.0)
                setInactiveBorder(label: label)
            }
            wrapperViewHeight.constant = 40
        }
        else {
            for label in codeLabels {
                label.layer.cornerRadius = 13
                label.layer.masksToBounds = true
                label.layer.borderWidth = 1
                setInactiveBorder(label: label)
            }
        }
        
        setActiveBorder(label: codeLabels[0])
        if willInitiateResendCode {
            resendCode()
        }
        else {
            resendCodeButton.isUserInteractionEnabled = false
            resendCodeCooldown()
        }
        let hintText = forVerifyEmail ? "Silahkan masukkan kode verifikasi yang telah kami kirimkan ke".localized : "Silahkan masukkan kode verifikasi yang telah kami kirimkan ke".localized
        hintLabel.text = hintText
        resendCodeButton.isHidden = !isFromRegister
        self.closeBtn.isHidden = !isFromRegister
        headerTopCT.constant = isFromRegister ? 50 : 20
//        navigationItem.hidesBackButton = forUpdateProfile
//        navigationController?.isNavigationBarHidden = !forUpdateProfile && !isFromLogin && !isFromRegister
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.hiddenCodeTextField.becomeFirstResponder()
//            self.navigationController?.setNavigationBorder(visible: true)
            self.navigationController?.navigationBar.shadowImage = UIImage()
        }
    }
    
    @objc func showKeyboard() {
        hiddenCodeTextField.becomeFirstResponder()
    }

    func deleteCode() {
        if currentPosition == 0 || currentPosition == 3 && hiddenCodeTextField.text?.count == 4 {
            codeLabels[currentPosition].text = ""
            setActiveBorder(label: codeLabels[currentPosition])
            return
        }
        codeLabels[currentPosition - 1].text = ""
        setInactiveBorder(label: codeLabels[currentPosition])
        setActiveBorder(label: codeLabels[currentPosition - 1])
        
        currentPosition -= 1
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setActiveBorder(label: UILabel) {
        label.layer.borderColor = UIColor.primary.cgColor
    }
    
    func setFilledBorder(label: UILabel) {
        label.layer.borderColor = UIColor.black.cgColor
    }
    
    func setInactiveBorder(label: UILabel) {
        label.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func resendButtonTapped(_ sender: UIButton) {
        resendCode()
    }
    
    func resendCode() {
        numberOfResend += 1
        if numberOfResend <= 5 {
            callRequestOTPAPI()
            resendCodeButton.isUserInteractionEnabled = false
            resendCodeCooldown()
        }
        else {
            resendCodeButton.isHidden = true
        }
        
    }
    
    func resendCodeCooldown(){
        cooldownTime = 60
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        self.cooldownTime -= 1
        resendCodeButton.setTitle("\(cooldownTime)" + "s left".localized, for: .normal)
        if cooldownTime == 0 {
            resendCodeButton.isUserInteractionEnabled = true
            resendCodeButton.setTitle("Resend Code".localized, for: .normal)
            timer.invalidate()
        }
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField) {
        if textField.text!.count == 4 {
            activityIndicator(isShow: true)
            if isFromRegister {
                self.callVerifyPhoneAPI()
            }
            else {
                if forVerifyEmail {
                    self.callUpdateEmailAPI()
                }
                else {
                    self.callUpdatePhoneNumberAPI()
                }
            }
        }
    }
    
    func activityIndicator(isShow: Bool) {
        if isShow {
            self.indicatorView.startAnimating()
            self.tappableView.isHidden = true
        }
        else {
            self.indicatorView.stopAnimating()
            self.tappableView.isHidden = false
        }
    }

    func callRequestOTPAPI() {
        API.request(API.Service.requestOTP, showLoading: false, success: { (response: APIResponse) in
            self.resendCodeCooldown()
        }, failure: { (error) in
            AlertView.showAlert(view: self, message: error.message ?? "", title: "Sorry".localized)
        })
    }
    
    func createWallet() {
//        API.request(API.Service.createWallet, showLoading: false, success: { (response: LoginResponse) in
//            DispatchQueue.main.async {
//                Defaults[.user] = response.data.user
//                self.submitCode(isPinVerified: response.data.user.isPinVerified)
//            }
//        }, failure: { (error) in
//            self.activityIndicator(isShow: false)
//            if let message = error.message {
//                AlertView.showAlert(view: self, message: message, title: "asd".localized)
//            }
//        })
    }

    func callVerifyPhoneAPI() {
        API.request(API.Service.verifyPhone(pin: hiddenCodeTextField.text!, phoneNumber: credential), showLoading: false, success: {
            (response: APIResponse) in
            if let user = response.data?.user {
                Defaults[.user] = user
            }
            Defaults[.isLogging] = true
            DispatchQueue.main.async {
                self.activityIndicator(isShow: false)
                self.view.endEditing(true)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabarViewController")
                self.swapRootView(vc: vc!)
            }
        }, failure: {
            (error) in
            self.activityIndicator(isShow: false)
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    func callUpdateEmailAPI() {
        API.request(API.Service.updateEmail(pin: hiddenCodeTextField.text!, email: credential), showLoading: false, success: {
            (response: APIResponse) in
            if let user = response.data?.user {
                Defaults[.user] = user
            }
            DispatchQueue.main.async {
                self.activityIndicator(isShow: false)
                self.view.endEditing(true)
                AlertView.showAlert(view: self, message: "You email has changed successfully", title: "Success", rightBtnAction: {
                    _ in
                    self.navigationController?.popToRootViewController(animated: true)
//                    self.showAccountScreen()
                }, leftBtnAction: { _ in })
            }
        }, failure: {
            (error) in
            self.activityIndicator(isShow: false)
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    func callUpdatePhoneNumberAPI() {
        API.request(API.Service.updatePhone(pin: hiddenCodeTextField.text!, phoneNumber: credential.replacingOccurrences(of: " ", with: "", options: String.CompareOptions.literal, range: nil)), showLoading: false, success: {
            (response: APIResponse) in
            if let user = response.data?.user {
                Defaults[.user] = user
            }
            DispatchQueue.main.async {
                self.activityIndicator(isShow: false)
                self.view.endEditing(true)
                AlertView.showAlert(view: self, message: "You phone number has changed successfully", title: "Success", rightBtnAction: {
                    _ in
                    self.navigationController?.popToRootViewController(animated: true)
                }, leftBtnAction: { _ in })
            }
        }, failure: {
            (error) in
            self.activityIndicator(isShow: false)
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    

}

extension EnterVerificationCodeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if indicatorView.isAnimating {
            return false
        }
        let newLength = text.count + string.count - range.length
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b") == -92
        if (isBackSpace) {
            deleteCode()
            return true
        }
       
        if newLength <= 4 && newLength != 0 && string.isNumberFiltered {
            codeLabels[newLength - 1].text = string.last?.description
            if newLength < 4 {
                setActiveBorder(label: codeLabels[newLength])
                currentPosition += 1
            }
            setFilledBorder(label: codeLabels[newLength - 1])
        }
        
        return newLength <= 4 && string.isNumberFiltered
    }
}
