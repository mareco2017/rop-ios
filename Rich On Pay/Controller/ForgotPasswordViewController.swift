//
//  ForgotPasswordViewController.swift
//  Rich On Pay
//
//  Created by Edison on 10/12/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendBtnClicked(_ sender: Any) {
        API.request(API.Service.forgotPassword(email: self.emailTF.text!), showLoading: true, success: { (response: APIResponse) in
            AlertView.showAlert(view: self, message: "Kami telah mengirimkan link atur ulang kata sandi ke email Anda", title: "Berhasil")
        }) { (error) in
            if let message = error.message {
                AlertView.showAlert(view: self, message: message, title: "Maaf")
            }
        }
    }
    
    
}
