//
//  HomeViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/17/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwiftyUserDefaults

class HomeViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pointBttmView: UIView!
    @IBOutlet weak var saldoBottomView: UIView!
    @IBOutlet weak var saldoBtn: UIButton!
    @IBOutlet weak var pointBtn: UIButton!
    @IBOutlet weak var walletIV: UIImageView!
    @IBOutlet weak var topUpBtn: UIButton!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    fileprivate var refreshControl: UIRefreshControl!
    
    var sdWebImageSource: [SDWebImageSource] = []
    var homeMenu = ["Pulsa", "Listrik", "Game Voucher"]
    var homeIcons = ["ic_pulsa", "ic_listrik", "ic_voucher_game"]
    var banner: [Banner] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slideshow.slideshowInterval = 5.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 5))
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.primary
        pageControl.pageIndicatorTintColor = UIColor.white
        slideshow.pageIndicator = pageControl
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.didTap))
        slideshow.addGestureRecognizer(recognizer)
        self.collectionView.fitContentHeight(footerHeight: 25)
        
        let logo = UIImage(named: "ic_logo_nav")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.getHome()
        self.addRefreshControl()
        NotificationCenter.default.addObserver(self, selector: #selector(setupProfile), name: NSNotification.Name(rawValue: "RefreshHome"), object: nil)
        let csButton = UIBarButtonItem(image: UIImage(named: "ic_cs")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(onClickCS))
        
        self.navigationItem.rightBarButtonItem  = csButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       self.setupProfile()
    }
    
    @objc func onClickCS() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerServiceController") as! CustomerServiceController
        self.show(vc, sender: nil)
    }
    
    @objc func setupProfile() {
        if let user = Defaults[.user] {
            userNameLbl.text = user.fullName
            packageLbl.text = user.packages == 0 ? "Free Member" : (user.packages ==  1 ? "Silver Member" : (user.packages == 2 ? "Gold Member" : "Platinum Member"))
           if let wallets = user.wallets {
                if wallets.count > 0 {
                    amountLbl.text = "Rp \(wallets[0].balance.formattedWithSeparator)"
                }
            }
            
        }
    }
    
    fileprivate func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.primary
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.primary]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh".localized, attributes: attributes)
        refreshControl.addTarget(self, action: #selector(getHome), for: UIControlEvents.valueChanged)
        self.scrollView.addSubview(refreshControl)
    }
    
    @objc func getHome() {
        self.getProfile()
        API.request(API.Service.getHome, showLoading: false, success: { (response: APIResponse) in
            if let banners = response.data?.banners {
                self.banner = banners
                self.sdWebImageSource.removeAll()
                for i in 0 ..< banners.count {
                    self.sdWebImageSource.append(SDWebImageSource(urlString: banners[i].cover.url)!)
                    
                }
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.slideshow.setImageInputs(self.sdWebImageSource)
            }
        }
        }) { (error) in
            self.refreshControl.endRefreshing()
        }
    }
    
    
    @objc func didTap() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebDocumentViewController") as! WebDocumentViewController
        vc.title = ""
        vc.url = "/promo/\(self.banner[slideshow.currentPage].id!)"
        self.show(vc, sender: nil)
//        let fullScreenController = slideshow.presentFullScreenController(from: self)
//        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
//        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    @IBAction func onMenuChange(_ sender: UIButton) {
        pointBttmView.backgroundColor = sender.tag == 1 ? UIColor.primary : UIColor.clear
        saldoBottomView.backgroundColor = sender.tag == 0 ? UIColor.primary : UIColor.clear
        saldoBtn.setTitleColor(sender.tag == 0 ? UIColor.primary : UIColor.lightGrayR, for: .normal)
        pointBtn.setTitleColor(sender.tag == 1 ? UIColor.primary : UIColor.lightGrayR, for: .normal)
        topUpBtn.setTitle(sender.tag == 0 ? "ISI SALDO" : "TUKAR", for: .normal)
        topUpBtn.tag = sender.tag
        walletIV.image = UIImage(named: sender.tag == 0 ? "ic_wallet" : "ic_point")
        if let user = Defaults[.user] {
            if let wallets = user.wallets {
                if wallets.count > 1 {
                    if sender.tag == 0 {
                        amountLbl.text = "Rp \(wallets[0].balance.formattedWithSeparator)"
                    }
                    else {
                        amountLbl.text = "\(wallets[1].balance.formattedWithSeparator)"
                    }
                }
            }
        }
    }
    
    @IBAction func onClickWalletBtn(_ sender: UIButton) {
        if sender.tag == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopUpAmountSelectionViewController") as! TopUpAmountSelectionViewController
            self.show(vc, sender: nil)
        }
        else {
             self.showAlertProgress()
        }
    }
    
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homeMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width / 3 - 10, height: self.view.frame.width / 3 - 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
        cell.menuLbl.text = homeMenu[indexPath.row]
        cell.iconIV.image = UIImage(named: homeIcons[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            let nextViewController = UIStoryboard.loadPrepaidPhoneCreditViewController()
            show(nextViewController, sender: nil)
        }
        else if indexPath.item == 1 {
            let nextViewController = UIStoryboard.loadElectricityPaymentViewController()
            show(nextViewController, sender: nil)
        }
        else if indexPath.item == 2 {
            let nextViewController = UIStoryboard.loadGameVouchersSelectionViewController()
            show(nextViewController, sender: nil)
        }
        else {
             self.showAlertProgress()
        }
    }
    
}

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var menuLbl: UILabel!

    
}

