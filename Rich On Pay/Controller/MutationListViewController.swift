//
//  MutationListViewController.swift
//  Rich On Pay
//
//  Created by Edison on 11/13/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class  MutationListViewController: UIViewController, BankSelectDelegate {
    
    @IBOutlet weak var bottomViewCTHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerViewCTHeight: NSLayoutConstraint!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var type = 0
    var totalAmount:Double = 0
    var orders = [Order]()
    var claimStatus: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if claimStatus == nil {
        }
        else {
            headerTitle.text = "Total Bonus"
            if claimStatus == 0 {
                headerViewCTHeight.constant = 0
            }
        }
        self.getTransaction()
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "ic_transaction_bonus"), style: .done, target: self, action: #selector(onRightButtonClicked))
        rightBarButtonItem.tintColor = UIColor.black
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    @IBAction func onCLickTransfer(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BankAccountsViewController") as! BankAccountsViewController
        vc.isWithdraw = true
        vc.amount = self.totalAmount
        vc.delegate = self
        self.show(vc, sender: nil)
    }
    
    @objc func onRightButtonClicked() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
        self.show(vc, sender: nil)
    }
    
    func didSelectBank(bank: BankAccount) {
        self.orders.removeAll()
        self.totalAmountLbl.text = "Rp 0"
        DispatchQueue.main.async {
            if self.orders.count != 0 {
                self.bottomView.isHidden = false
                self.bottomViewCTHeight.constant = 100
            }
            else {
                self.bottomView.isHidden = true
                self.bottomViewCTHeight.constant = 0
            }
            self.tableView.reloadData()
        }
    }
    
    @objc func getTransaction() {
        if claimStatus == nil {
            getMutation()
        }
        else {
            self.getStatement()
        }
    }
    
    func getStatement() {
        API.request(API.Service.getStatement(claimed: self.claimStatus!), showLoading: false, success: { (response: APIResponse) in
            if let orders = response.data?.statements {
                self.orders = orders
                var totalAmount: Double = 0
                for order in orders {
                    totalAmount += order.amount
                }
                self.totalAmount = totalAmount
                self.totalAmountLbl.text = "Rp \(totalAmount.formattedWithSeparator)"
                DispatchQueue.main.async {
                    
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            //            self.refreshControl.endRefreshing()
        }
    }
    
    func getMutation() {
        API.request(API.Service.getTransaction(status: "3", type: "15,19"), showLoading: false, success: { (response: APIResponse) in
            if let orders = response.data?.orders {
                self.orders = orders.filter{ $0.claimed == nil }
                var totalAmount: Double = 0
                for order in self.orders {
                    totalAmount += order.total
                }
                self.totalAmount = totalAmount
                self.totalAmountLbl.text = "Rp \(totalAmount.formattedWithSeparator)"
                DispatchQueue.main.async {
                    if self.orders.count != 0 {
                        self.bottomView.isHidden = false
                        self.bottomViewCTHeight.constant = 100
                    }
                    else {
                        self.bottomView.isHidden = true
                        self.bottomViewCTHeight.constant = 0
                    }
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            //            self.refreshControl.endRefreshing()
        }
    }
    
}

extension MutationListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orders.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return claimStatus != nil ? 80 : 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MutationListTableCell
        let order = self.orders[indexPath.row]
        if claimStatus != nil {
            cell.transactionIdLbl.text = "Statement ID: \(order.statementNo!)"
            cell.amountLbl.text = "Rp \(order.amount.formattedWithSeparator)"
            cell.bankLbl.text = "\(order.bankAccount.accountNo!) / \(order.bankAccount.accountName!)"
        }
        else {
            cell.transactionIdLbl.text = "\(order.referenceNo!)"
            cell.amountLbl.text = "Rp \(order.total.formattedWithSeparator)"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        cell.dateLbl.text = "\(dateFormatter.string(from: order.createdAt))"
       
        return cell
    }
    
}

class MutationListTableCell: UITableViewCell {
    
    @IBOutlet weak var bankLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var transactionIdLbl: UILabel!
    
}
