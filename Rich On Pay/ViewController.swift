//
//  ViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/17/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ViewController: UIViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickLogin(_ sender: UIButton) {
        if emailTF.text!.isEmptyField {
            AlertView.showAlert(view: self, message: "Email tidak boleh kosong!", title: "Maaf")
            return
        }
        else if passwTF.text!.isEmptyField {
            AlertView.showAlert(view: self, message: "Password tidak boleh kosong!", title: "Maaf")
            return
        }
        API.request(API.Service.login(username: self.emailTF.text!, pasw: self.passwTF.text!), showLoading: true, success: { (response: APIResponse) in
            guard let data = response.data else {
                return
            }
            API.token = data.token
            Defaults[.user] = data.user
            Defaults[.isLogging] = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabarViewController")
            self.swapRootView(vc: vc!)
//            }
        }) { (error) in
            guard let message = error.message else {
                return
            }
            AlertView.showAlert(view: self, message: message, title: "Maaf")
        }
    }
    


}

