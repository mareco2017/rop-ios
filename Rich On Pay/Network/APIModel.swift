//
//  APIModel.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import Foundation
import ObjectMapper

class APIError: NSObject, Mappable {
    
    var message: String!
    var error: String!
    var errors: Errors!
    
    required init?(map: Map) {
    }
    
    override init(){}
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["error"]
        errors <- map["errors"]
    }
    
}

class Errors: NSObject, Mappable {
    
    var name: [String]!
    var phone: [String]!
    var email: [String]!
    var newPhone: [String]!
    var newEmail: [String]!
    var newPassword: [String]!
    var password:[String]!
    var leaderCode: [String]!
    var sponsorCode:[String]!
    
    required init?(map: Map) {
    }
    
    override init(){}
    
    func mapping(map: Map) {
        email <- map["email"]
        name <- map["first_name"]
        phone <- map["phone_number"]
        password <- map["password"]
        leaderCode <- map["leader_code"]
        sponsorCode <- map["sponsor_code"]
        newPhone <- map["new_phone_number"]
        newEmail <- map["new_email"]
        newPassword <- map["new_password"]
    }
    
}


class APISuccess: Mappable {
    
    var message: String!
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
    
}
