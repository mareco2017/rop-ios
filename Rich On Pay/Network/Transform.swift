//
//  Transform.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import ObjectMapper

class IntTransform: TransformType {
    
    typealias Object = Int
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> Int? {
        if let value = value as? Int {
            return value
        }
        
        return Int(value as! String)
    }
    
    func transformToJSON(_ value: Int?) -> String? {
        return String(describing: value)
    }
}

class FloatTransform: TransformType {
    
    typealias Object = Float
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> Float? {
        return Float(value as! String)
    }
    
    func transformToJSON(_ value: Float?) -> String? {
        return String(describing: value)
    }
}

class SafeStringTransform: TransformType {
    
    typealias Object = String
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> String? {
        if let string = value as? String {
            return string
        }
        return ""
    }
    
    func transformToJSON(_ value: String?) -> String? {
        return String(describing: value)
    }
}
//
//class IntergerTransform: TransformType {
//
//    typealias Object = Int
//    typealias JSON = String
//
//    func transformFromJSON(_ value: Any?) -> Int? {
//        if let intVal = value as? String {
//            return Int(intVal)
//        }
//        return Int(value)
//    }
//
//    func transformToJSON(_ value: Float?) -> String? {
//        return String(describing: value)
//    }
//}

open class DateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    var dateFormatter: DateFormatter {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            // formatter.timeZone = NSTimeZone.local
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            return formatter
        }
    }
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let value = value as? String {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            if value == "0000-00-00 00:00:00" {
                return Date()
            }
            dateFormatter2.timeZone = TimeZone(abbreviation: "Local")
            if let date = dateFormatter.date(from: "\(value)+0000") {
                return date
            }
            else {
                dateFormatter2.locale = Locale(identifier: NSLocale.current.identifier)
                return dateFormatter2.date(from: "\(value)+0000")! as Date
            }
        }
        
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return dateFormatter.string(from: date as Date)
        }
        return nil
    }
}


open class TimeTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    var dateFormatter: DateFormatter {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            // formatter.timeZone = NSTimeZone.local
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            return formatter
        }
    }
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let value = value as? String {
            let dateFormatter2 = DateFormatter()
            // dateFormatter2.locale = Locale(identifier: "en_US")
            dateFormatter2.dateFormat = "HH:mm:ss"
            //            if value == "00:00:00" {
            //                return Date()
            //            }
            // dateFormatter2.timeZone = TimeZone(abbreviation: "Local")
            if let date = dateFormatter.date(from: "\(value)") {
                return date
            }
            else {
                dateFormatter2.locale = Locale(identifier: NSLocale.current.identifier)
                return dateFormatter2.date(from: "\(value)")! as Date
            }
        }
        
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return dateFormatter.string(from: date as Date)
        }
        return nil
    }
}

open class DateDOBTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    var dateFormatter: DateFormatter {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            // formatter.timeZone = NSTimeZone.local
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            return formatter
        }
    }
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let value = value as? String {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd"
            if value == "0000-00-00 00:00:00" {
                return Date()
            }
            dateFormatter2.timeZone = TimeZone(abbreviation: "Local")
            return dateFormatter.date(from: "\(value)")! as Date
        }
        
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return dateFormatter.string(from: date as Date)
        }
        return nil
    }
}

extension NSDate {
    var formattedISO8601: String {
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:SSxxxxx"   //+00:00
        return formatter.string(from: self as Date)
    }
}
