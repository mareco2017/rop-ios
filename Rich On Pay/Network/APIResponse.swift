//
//  APIResponse.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import ObjectMapper

class APIResponse: Mappable {
    
    var message: String!
    var data: APIData?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        data <- map["data"]
    }
    
}

class APIData: Mappable {
    
    var token: String!
    var user: User!
    var bankAccounts: [BankAccount]!
    var bankAccount: BankAccount!
    var banks: [Bank]!
    var upgradeRequest: UpgradeRequest!
    var banners: [Banner]!
    var requests: [UpgradeRequest]!
    var paymentProducts: [PaymentProduct]!
    var paymentProductCategories: [PaymentProductCategory]!
    var regionCategories: [RegionCategories]!
    var order: Order!
    var topUp: [TopUp]!
    var orderRequest: UpgradeRequest!
    var receipt: Receipt!
    var orders: [Order]!
    var statements: [Order]!
    var email: String!
    var phone: String!
    var wa: String!
    var cs: String!
    var currentLimitUser: Double!
    var limitUser: Double!
    var adminFee: Double!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        topUp <- map["topup"]
        user <- map["user"]
        token <- map["token"]
        banks <- map["banks"]
        bankAccounts <- map["bank_accounts"]
        bankAccount <- map["bank_account"]
        upgradeRequest <- map["upgrade_request"]
        banners <- map["banners"]
        requests <- map["upgrade_requests"]
        paymentProducts <- map["paymentProducts"]
        paymentProductCategories <- map["categories"]
        regionCategories <- map["categories"]
        order <- map["order"]
        orderRequest <- map["order"]
        receipt <- map["receipt"]
        orders <- map["orders"]
        statements <- map["result"]
        email <- map["email"]
        wa <- map["whatsapp"]
        phone <- map["phone"]
        cs <- map["cs"]
        currentLimitUser <- map["currentLimitUser"]
        limitUser <- map["limitUser"]
        adminFee <- map["adminFee"]
    }
}
