//
//  API.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import KeychainAccess
import Moya
import ObjectMapper
import Moya_ObjectMapper
import Alamofire
import SwiftyJSON
import SystemConfiguration
import SwiftyUserDefaults

class DefaultAlamofireManager: Alamofire.SessionManager {
    static let sharedManager: DefaultAlamofireManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        return DefaultAlamofireManager(configuration: configuration)
    }()
    static let defaultManager: DefaultAlamofireManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        return DefaultAlamofireManager(configuration: configuration)
    }()
}

struct API {
    
    fileprivate static let keychain = Keychain(service: "com.mareco.beta")
    
    fileprivate static var provider = API.initProvider()
    
    static var productionURL = "https://richonpay.com/api"
    
    static var stagingURL = "https://rop.mareco.id/api"
    
    static var production = true
    
    static var showLog = true
    
   static  var isRefreshToken = false
    
    static var token: String? {
        get {
            return keychain["token"]
        }
        set {
            keychain["token"] = newValue
        }
    }
    static var verifyToken: String? {
        get {
            return keychain["verifyToken"]
        }
        set {
            keychain["verifyToken"] = newValue
        }
    }
    static var facebookToken: String? {
        get {
            return keychain["facebookToken"]
        }
        set {
            keychain["facebookToken"] = newValue
        }
    }
    static var deviceSession: String? {
        get {
            return keychain["deviceSession"]
        }
        set {
            keychain["deviceSession"] = newValue
        }
    }
    static var deviceToken: String? {
        get {
            return keychain["deviceToken"]
        }
        set {
            keychain["deviceToken"] = newValue
        }
    }
    static var loggedIn: Bool {
        get {
            return keychain["token"] != nil
        }
    }
    
    static var totalRequests = 0
    
    fileprivate static func initProvider() -> MoyaProvider<Service> {
        var manager = DefaultAlamofireManager()
        let endpointClosure = { (target: Service) -> Endpoint in
            manager = target.manager
            let endpoint: Endpoint = Endpoint(
                url: target.url,
                sampleResponseClosure: {.networkResponse(200, target.sampleData as Data)},
                method: target.method,
                task: Task.requestParameters(parameters: target.parameters!, encoding: target.parameterEncoding), httpHeaderFields: nil
            )
            switch target {
            case .login,
                 .register,
                 .forgotPassword:
                return endpoint.adding(newHTTPHeaderFields: ["Accept": "application/json"])
            default:
                if let token = API.token {
                    return endpoint.adding(
                        newHTTPHeaderFields: ["Authorization": "Bearer " + token, "Accept": "application/json",
                                              "Device-Type": "iOS"]
                    )
                }
                return endpoint.adding(newHTTPHeaderFields: ["Accept": "application/json"])
            }
        }
        return MoyaProvider(
            endpointClosure: endpointClosure,
            manager: manager
        )
    }
    
    static func request<T: Mappable>(_ target: API.Service, showLoading: Bool, success: @escaping ((T) -> Void), view: UIView? = UIView(), failure: ((APIError) -> Void)? = nil) {
        showProgress(isShowing: showLoading)
        provider.request(target) { (result) in
            showProgress(isShowing: false)
            switch result {
            case let .success(response):
                do {
                    if self.showLog {
                        let data = try response.mapString()
                        print(data)
                    }
                    print(response.statusCode)
                    if response.statusCode >= 200 && response.statusCode < 400 {
                        let object: T = try response.mapObject(T.self)
                        success(object)
                    }
                    else if response.statusCode == 401 {
                        if !isRefreshToken {
                            tokenExpired(target, showLoading: showLoading, success: success, failure: failure)
                        }
                        else {
                            API.request(target, showLoading: showLoading, success: success, failure: failure)
                        }
                    }
                    else {
                        let json:APIError = try response.mapObject(APIError.self)
                        if let failure = failure {
                            failure(json)
                        }else{
                            if let messages = json.message {
                                showError(messages)
                            }
                        }
                    }
                } catch {
                    showError("Unknown error occurred !")
                }
            case let .failure(error):
                //                let json = APIError()
                //                json.message = "Time out"
                //                failure!(json)
                if error.code == 4 {
                    //    request(target, showLoading: false, success: success, failure: failure)
                }
                let json = APIError()
                json.message = error.localizedDescription
                if let failure = failure {
                    failure(json)
                }
                print("Failure: \(error)")
            }
        }
    }
    
    static func upload<T: Mappable>(_ target: API.Service, showLoading: Bool, success: @escaping ((T) -> Void), failure: ((APIError) -> Void)? = nil) {
        showProgress(isShowing: showLoading)
        provider.manager.upload(
            multipartFormData: { multipartFormData in
                for (key, val) in target.parameters! {
                    
                    if let val = val as? UIImage {
                        multipartFormData.append(UIImageJPEGRepresentation(val, 80)!, withName: key, fileName: "\(key).jpg", mimeType: "image/jpeg")
                    } else {
                        let val = String(describing: val)
                        multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }
        }, usingThreshold: Manager.multipartFormDataEncodingMemoryThreshold , to: provider.endpoint(target).url, method: provider.endpoint(target).method, headers:  ["Authorization": "Bearer \(API.token!)"],
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseString(
                    completionHandler: { (response) in
                        showProgress(isShowing: false)
                        if let string = response.result.value {
                            if let data = convertToDictionary(text: string){
                                if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! < 400 {
                                    if let object = T(JSON: data) {
                                        showProgress(isShowing: false)
                                        success(object)
                                    }
                                }
                                else if response.response?.statusCode == 401 {
                                    if !isRefreshToken {
                                        tokenExpired(target, showLoading: showLoading, success: success, failure: failure)
                                    }
                                    else {
                                        API.request(target, showLoading: showLoading, success: success, failure: failure)
                                    }
                                }
                                else {
                                    let json:APIError = APIError(JSON: data)!
                                    if let failure = failure {
                                        showProgress(isShowing: false)
                                        failure(json)
                                    }else{
                                        if let messages = json.message {
                                            showProgress(isShowing: false)
                                            showError(messages)
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            let json = APIError()
                            json.message = "Unknown error occurred. Please try again later."
                            if let failure = failure {
                                failure(json)
                            }
                        }
                })
                
                upload.resume()
            case .failure:
                showProgress(isShowing: false)
            }
        })
    }
    
    static func request<T: Mappable>(_ target: API.Service, showLoading: Bool, success: @escaping (([T]) -> Void), failure: ((APIError) -> Void)? = nil) {
        showProgress(isShowing: showLoading)
        provider.request(target) { (result) in
            showProgress(isShowing: false)
            switch result {
            case let .success(response):
                do {
                    if self.showLog {
                        let data = try response.mapString()
                        print(data)
                    }
                    if response.statusCode >= 200 && response.statusCode < 400 {
                        let array: [T] = try response.mapArray(T.self)
                        success(array)
                    }
                    else if response.statusCode == 401 {
                        if !isRefreshToken {
                            tokenExpired(target, showLoading: showLoading, success: success, failure: failure)
                        }
                        else {
                            API.request(target, showLoading: showLoading, success: success, failure: failure)
                        }
                    }
                    else {
                        let json:APIError = try response.mapObject(APIError.self)
                        if let failure = failure {
                            failure(json)
                        }else{
                            if let messages = json.message {
                                showError(messages)
                            }
                        }
                    }
                } catch {
                }
            case let .failure(error):
                if error.code == 4 {
                    //request(target, showLoading: false, success: success, failure: failure)
                }
                let json = APIError()
                json.error = error.localizedDescription
                if let failure = failure {
                    failure(json)
                }
                print("Failure: \(error.code)")
            }
        }
    }
    
    static func requestJSON(_ target: API.Service, showLoading: Bool, completion: @escaping ((JSON) -> Void)) {
        showProgress(isShowing: showLoading)
        provider.request(target) { (result) in
            switch result {
            case let .success(response):
                showProgress(isShowing: false)
                do {
                    let responseString = try response.mapString()
                    let json = JSON.init(parseJSON: responseString)
                    if response.statusCode >= 200 && response.statusCode < 400 {
                        print(json)
                        completion(json)
                    }
                    else if response.statusCode == 401 {
                        
                    }
                    else {
                        if let message = json["message"].string {
                            showError(message)
                        }
                        completion(true)
                    }
                } catch {
                }
            case .failure:
                showProgress(isShowing: false)
                completion(false)
            }
        }
    }
    
    fileprivate static func showProgress(isShowing: Bool) {
        totalRequests = isShowing ? totalRequests+1 : totalRequests-1
        if  isShowing{
            DispatchQueue.main.async(execute: {
                GiFHUD.showWithOverlay()
            })
        }
        else {
            GiFHUD.dismiss()
        }
    }
    
    static func showError(_ error: String) {
        //    dispatch_after(DispatchTime.now(dispatch_time_t(DISPATCH_TIME_NOW), 400000000), DispatchQueue.main, { () -> Void in
        //                    SVProgressHUD.showError(withStatus: error)
        //                })
    }
    
    static func tokenExpired<T: Mappable>(_ target: API.Service, showLoading: Bool, success: @escaping ((T) -> Void), failure: ((APIError) -> Void)? = nil) {
        if UIApplication.topViewController() != nil && API.token != nil {
            isRefreshToken = true
            API.request(API.Service.refreshToken, showLoading: false, success: { (response: APIResponse) in
                API.token = response.data?.token
                isRefreshToken = false
                API.request(target, showLoading: showLoading, success: success, failure: failure)
            }, failure: { (error) in
                if API.token == nil {
                    return
                }
                if let message = error.message {
                    if message == "The token has been blacklisted" || message == "Token Signature could not be verified." || message == "Failed to create session. No user binded." || message == "Token has expired and can no longer be refreshed" || message == "Unauthenticated."{
                        showAlertLogout()
                    }
                }
            })
        }
    }
    
    static func tokenExpired<T: Mappable>(_ target: API.Service, showLoading: Bool, success: @escaping (([T]) -> Void), failure: ((APIError) -> Void)? = nil) {
        if UIApplication.topViewController() != nil && API.token != nil {
            isRefreshToken = true
            API.request(API.Service.refreshToken, showLoading: false, success: { (response: APIResponse) in
                API.token = response.data?.token
                API.request(target, showLoading: showLoading, success: success, failure: failure)
                isRefreshToken = false
            }, failure: { (error) in
                if API.token == nil {
                    return
                }
                if let message = error.message {
                    if message == "The token has been blacklisted" || message == "Token Signature could not be verified." || message == "Failed to create session. No user binded." || message == "Token has expired and can no longer be refreshed" || message == "Unauthenticated." {
                        showAlertLogout()
                    }
                }
            })
        }
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func showAlertLogout() {
        if let vc = UIApplication.topViewController() {
            API.token = nil
            AlertView.showAlert(view: vc, message: "", title: "Session Expired", rightBtnAction: {_ in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let newVc = storyboard.instantiateViewController(withIdentifier: "ViewController")
                vc.swapRootView(vc: newVc)
            }, leftBtnAction: {_ in
            })
            
        }
    }
    
    static func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
}

extension API {
    
    enum Service: TargetType {
        
        var headers: [String: String]? {
            return ["Accept": "application/json"]
        }
        /// The parameters to be encoded in the request
        /// AUTH
       
        case login(username: String, pasw: String)
        case register(pN: String, email: String, name: String, password: String, paswConfirm: String, hasRefferal: Int, sponsorCode: String, leaderCode: String)
        case getBankList
        case getBanks
        case createNewBankAccount(bankId: Int, accountName: String, accountNo: String)
        case removeBankAccount(bankAccountId: Int)
        case setPrimaryBankAccount(bankAccountId: Int)
        case getBankAccountList
        case refreshToken
        case upgrade(packages: Int)
        case upgradeBank(id: Int, selectedBank: Int)
        case getPaymentBanks
        case uploadIdentity(ktp: UIImage, selfieKtp: UIImage)
        case getProfile
        case updatePhone(pin: String, phoneNumber: String)
        case verifyPhone(pin: String, phoneNumber: String)
        case updateEmail(pin: String, email: String)
        case changePhone(phoneNumber: String)
        case changeEmail(email: String)
        case requestOTP
        case getHome
        case logout
        case payUpgrade(id: Int, packages: Int, selectedBank: Int)
        case cancelUpgrade(id: Int, packages: Int, selectedBank: Int)
        case getTransaction(status: String, type: String)
        case forgotPassword(email: String)
        case changePassword(password: String, newPassword: String, confirmPassword: String)
         case getPaymentProductByType(type: Int)
        case getPaymentProductsByCategory(productType: Int, categoryId: Int)
        case inquiryPDAM(type: Int, productId: Int, customerId: String)
        case electricityInquiry(isPrepaid: Bool, customerId: String)
        case getTopUpList
        case requestTopUp(destBankAccountId: Int, amount: Double)
        case topUpPaid(orderId: Int)
        case cancelTopUp(orderId: Int)
        case phonePrepaidPayment(paymentProductId: Int, phoneNumber: String, password: String)
         case electricityPayment(password: String,  orderId: Int, paymentProductId: Int, customerId: String)
        case gameVoucherPayment(paymentProductId: Int, password: String)
        case getStatement(claimed: Int)
        case balanceWithdraw(bankId: Int, amount: Double)
        case contactUs
        
        //Base Url
        var baseURL: URL { return NSURL(string: production ? productionURL : stagingURL)! as URL }
        var url: String {
            return baseURL.appendingPathComponent(path).absoluteString
        }
        
        var manager: DefaultAlamofireManager {
            return DefaultAlamofireManager.defaultManager
//            switch self {
//            case .uploadIdentificationPictures,
//                 .updateProfile:
//                return DefaultAlamofireManager.sharedManager
//            default:
//                return DefaultAlamofireManager.defaultManager
//            }
        }
        
        //API Route
        var path: String {
            switch self {
            case .register:
                return "/v1/register"
            case .login:
                return "/v1/login"
            case .getBankList:
                return "/v1/bank-account"
            case .createNewBankAccount:
                return "/v1/bank-account/create"
            case .removeBankAccount(let bankAccountId):
                return "/v1/bank-account/\(bankAccountId)/remove"
            case .setPrimaryBankAccount(let bankAccountId):
                return "/v1/bank-account/\(bankAccountId)/set-primary"
            case .getBankAccountList:
                return "/v1/bank-account"
            case .refreshToken:
                return "/v1/refresh"
            case .getPaymentBanks:
                return "/v1/bank-account/payment"
            case .upgrade:
                return "/v1/package/upgrade"
            case .upgradeBank(let id, _):
                return "/v1/package/upgrade/\(id)/pay"
            case .uploadIdentity:
                return "/v1/user/upload-identity"
            case .getProfile:
                return "/v1/user/me"
            case .updatePhone:
                return "/v1/user/update-phone-number"
            case .updateEmail:
                return "/v1/user/update-email"
            case .changePhone:
                return "/v1/user/change-phone-number"
            case .changeEmail:
                return "/v1/user/change-email"
            case .requestOTP:
                return "/v1/user/resend-otp"
            case .getHome:
                return "/v1/home"
            case .logout:
                return "/v1/logout"
            case .verifyPhone:
                return "/v1/user/verify-phone-number"
            case .payUpgrade(let id, _, _):
                return "/v1/package/upgrade/\(id)/confirm-payment"
            case .cancelUpgrade(let id, _, _):
                return "/v1/package/upgrade/\(id)/cancel"
            case .getTransaction:
                return "/v1/order/owned"
            case .forgotPassword:
                return "/v1/forgot-password"
            case .getBanks:
                return "/v1/bank"
            case .changePassword:
                return "/v1/user/change-password"
            case .getPaymentProductByType(let type):
                return "/v1/payment/\(type)"
            case .getPaymentProductsByCategory(let productType, let categoryId):
                return "/v1/payment/\(productType)/\(categoryId)"
            case .inquiryPDAM(let type, _, _):
                return "/v1/payment/\(type)/inquiry"
            case .electricityInquiry:
                return "/v1/payment/3/inquiry"
            case .getTopUpList:
                return "/v1/balance/topup-list"
            case .requestTopUp:
                return "/v1/balance/topup"
            case .topUpPaid(let orderId):
                return "/v1/balance/topup/\(orderId)/paid"
            case .cancelTopUp(let orderId):
                return "/v1/balance/topup/\(orderId)/cancel"
            case .phonePrepaidPayment:
                return "/v1/payment/1/payment"
            case .electricityPayment:
                return "/v1/payment/3/payment"
            case .gameVoucherPayment:
                return "/v1/payment/4/payment"
            case .getStatement:
                return "/v1/order/statement"
            case .balanceWithdraw:
                return "/v1/balance/bonus-withdraw"
            case .contactUs:
                return "/v1/contact-us"
            default:
                return ""
            }
            
            
        }
        
        //API Method
        var method: Moya.Method {
            switch self {
            case .login,
                 .balanceWithdraw,
                 .register,
                 .createNewBankAccount,
                 .removeBankAccount,
                 .setPrimaryBankAccount,
                 .refreshToken,
                 .upgrade,
                 .uploadIdentity,
                 .updatePhone,
                 .updateEmail,
                 .changePhone,
                 .changeEmail,
                 .requestOTP,
                 .logout,
                 .verifyPhone,
                 .payUpgrade,
                 .cancelUpgrade,
                 .forgotPassword,
                 .upgradeBank,
                 .changePassword,
                 .inquiryPDAM,
                 .electricityInquiry,
                 .requestTopUp,
                 .topUpPaid,
                 .cancelTopUp,
                 .phonePrepaidPayment,
                 .electricityPayment,
                 .gameVoucherPayment:
                return .post
            default:
                return .get
            }
        }
        
        
        //API Body and Parameter
        var parameters: [String: Any]? {
            switch self {
            case .balanceWithdraw(let bankId, let amount):
                return [
                    "bank_account_id": bankId,
                    "amount": amount
                ]
            case .getStatement(let claimed):
                return [
                    "claimed": claimed
                ]
            case.getTransaction(let status, let type):
                return [
                    "status": status,
                    "type": type
                ]
            case .requestTopUp(let destBankAccountId, let amount):
                return [
                    "dest_bank_account_id" : destBankAccountId,
                    "amount" : amount
                ]
            case .login(let email, let password):
                return [
                    "email": email,
                    "password": password
                ]
            case .requestOTP:
                return [
                    "usage": "verify_phone_number"
                ]
            case .changePassword(let oldPassword, let password, let newPassword):
                return [
                    "password": oldPassword,
                    "new_password": password,
                    "new_password_confirmation": newPassword
                ]
            case .forgotPassword(let email):
                return [
                    "email": email
                ]
            case .register(let pN, let email, let name, let password, let paswConfirm, let hasRefferal, let sponsorCode, let leaderCode):
                return [
                    "phone_number": pN,
                    "email": email,
                    "password": password,
                    "password_confirmation": paswConfirm,
                    "has_referral": hasRefferal,
                    "sponsor_code": sponsorCode,
                    "leader_code": leaderCode,
                    "first_name": name
                ]
            case .createNewBankAccount(let bankId, let accountName, let accountNo):
                return [
                    "bank_id" : bankId,
                    "account_name" : accountName,
                    "account_no" : accountNo,
                    "is_primary": 1
                ]
            case .upgrade(let packages):
                return [
                    "package" : packages
                ]
            case .upgradeBank(_, let selectedBank):
                return [
                    "bank_account_id" : selectedBank
                ]
            case .payUpgrade(_, let packages, let selectedBank):
                return [
                    "package" : packages,
                    "bank_account_id" : selectedBank
                ]
            case .cancelUpgrade(_, let packages, let selectedBank):
                return [
                    "package" : packages,
                    "bank_account_id" : selectedBank
                ]
            case.uploadIdentity(let ktp, let selfieKtp):
                return [
                    "nric_picture" : ktp,
                    "selfie_picture" : selfieKtp
                ]
            case .updatePhone(let pin, let phoneNumber):
                print(pin,"asdasda123123")
                return [
                    "pin" : pin,
                    "phone_number" : phoneNumber
                ]
            case .updateEmail(let pin, let email):
                return [
                    "pin" : pin,
                    "email" : email
                ]
            case .changePhone(let phoneNumber):
                return [
                    "new_phone_number" : phoneNumber
                ]
            case .changeEmail(let email):
                return [
                    "new_email" : email
                ]
            case .verifyPhone(let pin, let phoneNumber):
                return [
                    "pin" : pin,
                    "phone_number" : phoneNumber
                ]
            case .inquiryPDAM(_, let productId, let customerId):
                return [
                    "payment_product_id": productId,
                    "customer_id": customerId
                ]
            case .electricityInquiry(let isPrepaid, let customerId):
                return [
                    "inquiry_type": (!isPrepaid) == true ? 1 : 0,
                    "customer_id": customerId
                ]
            case .phonePrepaidPayment(let paymentProductId, let phoneNumber, let password):
                return [
                    "payment_product_id": paymentProductId,
                    "phone_number": phoneNumber,
                    "password": password,
                    "wallet_type": 0
                ]
            case .electricityPayment(let password, let orderId, let paymentProductId, let customerId):
                var param = [String: Any]()
                param["password"] = password
                param["wallet_type"] = 0
                param["order_id"] = orderId
                param["customer_id"] = customerId
                if paymentProductId != 0 {
                    param["payment_product_id"] = paymentProductId
                }
                return param
            case .gameVoucherPayment(let paymentProductId, let password):
                return [
                    "password": password,
                    "wallet_type": 0,
                    "payment_product_id": paymentProductId
                ]
            default:
                return [:]
                //                return nil
            }
        }
        
        //API Encoding type
        var parameterEncoding: Moya.ParameterEncoding {
            switch self {
            case .login,
                 .register,
                 .inquiryPDAM,
                 .phonePrepaidPayment,
                 .electricityPayment,
                 .gameVoucherPayment:
                return JSONEncoding.default
            default:
                return URLEncoding.default
            }
        }
        var sampleData: Data {
            switch self {
            default:
                return "".data(using: String.Encoding.utf8)!
            }
        }
        var dateFormatter: DateFormatter {
            get {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                return formatter
            }
        }
        
        var task: Task {
            return .requestPlain
        }
        
    }
    
}

struct JsonArrayEncoding: Moya.ParameterEncoding {
    
    public static var `default`: JsonArrayEncoding { return JsonArrayEncoding() }
    /// Creates a URL request by encoding parameters and applying them onto an existing request.
    ///
    /// - parameter urlRequest: The request to have parameters applied.
    /// - parameter parameters: The parameters to apply.
    ///
    /// - throws: An `AFError.parameterEncodingFailed` error if encoding fails.
    ///
    /// - returns: The encoded request.
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var req = try urlRequest.asURLRequest()
        let json = try JSONSerialization.data(withJSONObject: parameters!["members"]!, options: JSONSerialization.WritingOptions.prettyPrinted)
        req.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        req.httpBody = json
        return req
    }
    
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}

