//
//  GameVouchersPaymentViewController.swift
//  Mareco
//
//  Created by Edy on 12/5/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class GameVouchersPaymentViewController: UIViewController {
    
    
    @IBOutlet weak var voucherTypeTextField: PhoneNumberTextField!
    @IBOutlet weak var voucherAmountButton: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var currentBalanceLabel: UILabel!
    
    var paymentProducts = [PaymentProduct]()
    var selectedPaymentProduct: PaymentProduct!
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?
    var voucherType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Game Voucher".localized
        prepareView()
        updateBalanceLabel()
    }
    
    func prepareView() {
        voucherTypeTextField.text = voucherType
        
        let initialPaymentProduct = paymentProducts[0]
        updateViewWith(paymentProduct: initialPaymentProduct)
    }
    
    func updateViewWith(paymentProduct: PaymentProduct) {
        voucherAmountButton.setTitle(paymentProduct.name, for: .normal)
        voucherAmountButton.alignImageOnTrailingEdge(withOffset: -10)
        
        let totalPrice = paymentProduct.price ?? 0
        totalPriceLabel.text = "Rp \(totalPrice.formattedWithSeparator)"
        selectedPaymentProduct = paymentProduct
    }
    
    fileprivate func updateBalanceLabel() {
        guard let user = Defaults[.user] else { return }
        if let wallets = user.wallets {
            if wallets.count > 0 {
                let userBalance = wallets[0].balance!
                let balance = NSMutableAttributedString(string: "Rp \(userBalance.formattedWithSeparator)")
                let rpRange = NSRange(location: 0,length: 3)
                let balanceRange = NSRange(location:3,length: balance.length - 3)
                balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Roboto-Regular", size: 16.interfaceIdiomSize(size: 5))!, range: rpRange)
                balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Roboto-Bold", size: 22.interfaceIdiomSize(size: 5))!, range: balanceRange)
                currentBalanceLabel.attributedText = balance
            }
        }
    }
    
    @IBAction func amountButtonDidTapped(_ sender: UIButton) {
        showPaymentProductAmountSelectionScreen()
    }
    
    func showPaymentProductAmountSelectionScreen() {
        let nextViewController = UIStoryboard.loadPaymentProductAmountSelectionViewController()
        nextViewController.delegate = self
        nextViewController.paymentProducts = paymentProducts
        nextViewController.type = .gameVoucher
        show(nextViewController, sender: nil)
    }
    
    @IBAction func buyButtonDidTapped(_ sender: UIButton) {
        view.endEditing(true)
        if selectedPaymentProduct == nil {
            AlertView.showAlert(view: self, message: "No amount selected yet", title: "Sorry".localized)
            return
        }
//        let primaryBalance = Defaults[.user]?.balances?.primary ?? 0
//        if selectedPaymentProduct.price > primaryBalance {
//            AlertView.showAlert(view: self, message: "Insufficient Balance", title: "Sorry".localized)
//            return
//        }
        
        if let user = Defaults[.user] {
            if let wallets = user.wallets {
                if wallets.count > 0 {
                    let primaryBalance = wallets[0].balance!
                    if selectedPaymentProduct.price > primaryBalance {
                        AlertView.showAlert(view: self, message: "Insufficient Balance", title: "Sorry".localized)
                        return
                    }
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerifiedPasswordViewController") as! VerifiedPasswordViewController
                    nextViewController.paymentProductId = selectedPaymentProduct.id
                    nextViewController.confirmViewType = .gameVoucher
                    nextViewController.selectedPaymentProduct = selectedPaymentProduct
                    
                    self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nextViewController)
                    nextViewController.modalPresentationStyle = .custom
                    nextViewController.transitioningDelegate = self.halfModalTransitioningDelegate
                    self.present(nextViewController, animated: true, completion: nil)
                }
            }
        }
//        let nextViewController = UIStoryboard.loadVerificationPaymentViewController()
//        let wallets = Wallets()
//        wallets.primary = Defaults[.user]?.balances?.primary
//        wallets.point = Defaults[.user]?.balances?.point
//        nextViewController.wallets = wallets
//        nextViewController.selectedPaymentProduct = selectedPaymentProduct
//        nextViewController.productName = selectedPaymentProduct.name
//        nextViewController.pinViewType = .gameVoucher
//
//        let nav = UINavigationController(rootViewController: nextViewController)
//        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nextViewController)
//        nav.modalPresentationStyle = .custom
//        nav.transitioningDelegate = self.halfModalTransitioningDelegate
//        self.present(nav, animated: true, completion: nil)
    }
}

extension GameVouchersPaymentViewController: PaymentProductAmountSelectionDelegate {
    func didSelectPaymentProductAmount(for paymentProduct: PaymentProduct) {
        updateViewWith(paymentProduct: paymentProduct)
    }
}
