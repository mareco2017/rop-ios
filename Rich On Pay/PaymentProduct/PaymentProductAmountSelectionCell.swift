//
//  PaymentProductAmountSelectionCell.swift
//  Mareco
//
//  Created by Edy on 10/5/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit

class PaymentProductAmountTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func setUpViewWith(data: PaymentProduct, type: String) {
        typeLabel.text = type.localized
        amountLabel.text = "\(data.name!)"
        priceLabel.text = "Rp " + "\(data.price.formattedWithSeparator)"
    }
    
    func setUpBankViewWith(data: PaymentProduct, type: String) {
        typeLabel.text = type.localized
        amountLabel.text = "\(data.name!)"
        priceLabel.text = ""
    }
}
