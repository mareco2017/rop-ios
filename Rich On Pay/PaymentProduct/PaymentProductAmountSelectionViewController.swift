//
//  PaymentProductAmountSelectionViewController.swift
//  Mareco
//
//  Created by Edy on 10/5/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit

enum PaymentProductType: String {
    case phoneCredit = "Phone Credit"
    case electricity = "Electricity"
    case gameVoucher = "Game Voucher"
}

protocol PaymentProductAmountSelectionDelegate {
    func didSelectPaymentProductAmount(for paymentProduct: PaymentProduct)
}

class PaymentProductAmountSelectionViewController: UIViewController {
    
    @IBOutlet weak var amountsTableView: UITableView!
    
    var paymentProducts: [PaymentProduct]!
    var type: PaymentProductType = .phoneCredit
    var delegate: PaymentProductAmountSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Amount".localized
        let amountsTableViewCell = UINib(nibName: "PaymentProductAmountTableViewCell", bundle: nil)
        amountsTableView.register(amountsTableViewCell, forCellReuseIdentifier: "cell")
        amountsTableView.estimatedRowHeight = 70
        amountsTableView.rowHeight = UITableViewAutomaticDimension
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension PaymentProductAmountSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentProducts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentProductAmountTableViewCell
        
        cell.setUpViewWith(data: paymentProducts[indexPath.row], type: type.rawValue)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.popViewController(animated: true)
        delegate?.didSelectPaymentProductAmount(for: paymentProducts[indexPath.row])
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
