//
//  GameVouchersSelectionViewController.swift
//  Mareco
//
//  Created by Edy on 11/5/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit

class GameVouchersSelectionViewController: UIViewController {
    
    @IBOutlet weak var gameVouchersTableView: UITableView!
    
    var gameVouchers: [String]!
    var type = 4
    var paymentProducts = [PaymentProduct]()
    var paymentProductCategories = [PaymentProductCategory]()
    var filteredPaymentProducts = [PaymentProduct]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Game Voucher".localized
        let nibFile = UINib(nibName: "GameVouchersMenuTableViewCell", bundle: nil)
        gameVouchersTableView.register(nibFile, forCellReuseIdentifier: "cell")
        gameVouchersTableView.tableFooterView = UIView()
        callGetPaymentProductsAPI()
    }
    
    func callGetPaymentProductsAPI() {
        API.request(API.Service.getPaymentProductByType(type: type), showLoading: true, success: {
            (response: APIResponse) in
            guard let data = response.data else { return }
            self.paymentProductCategories = data.paymentProductCategories
            self.gameVouchersTableView.reloadData()
        }, failure: {
            (error) in
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    func callGetPaymentProductsByCategoryAPI(category: PaymentProductCategory) {
        API.request(API.Service.getPaymentProductsByCategory(productType: type, categoryId: category.id), showLoading: true, success: {
            (response: APIResponse) in
            guard let data = response.data else { return }
            if data.paymentProducts.count > 0 {
                self.showGameVoucherPaymentScreen(with: data.paymentProducts, voucherType: category.category)
            }
            else {
                AlertView.showAlert(view: self, message: "Voucher out of stock".localized, title: "Sorry".localized)
            }
        }, failure: {
            (error) in
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    func showGameVoucherPaymentScreen(with paymentProducts: [PaymentProduct], voucherType: String) {
        let nextViewController = UIStoryboard.loadGameVouchersPaymentViewController()
        nextViewController.paymentProducts = paymentProducts
        nextViewController.voucherType = voucherType
        show(nextViewController, sender: nil)
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension GameVouchersSelectionViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentProductCategories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = tableView.backgroundColor
        view.frame = CGRect.zero
        let label = UILabel()
        label.font = UIFont(name: "SFProText-Regular", size: 13.interfaceIdiomSize(size: 7))
        label.text = "Pilih Voucher"
        label.frame = CGRect(x: 16, y: 14, width: tableView.frame.size.width - 30, height: 21)
        label.textColor = UIColor(hex: "6D6D72")
        
        let seperator = UIView()
        seperator.frame = CGRect(x: 0, y : 37 , width: tableView.frame.width, height: 1)
        seperator.backgroundColor = UIColor.lightGray
        
        view.addSubview(seperator)
        view.addSubview(label)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GameVouchersMenuTableViewCell
        
        let paymentProductCategory = paymentProductCategories[indexPath.row]
        cell.setupCellWith(data: paymentProductCategory)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCategory = paymentProductCategories[indexPath.row]
        callGetPaymentProductsByCategoryAPI(category: selectedCategory)
    }
}
