//
//  TransactionSuccessViewController.swift
//  Mareco
//
//  Created by Edison on 4/11/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

enum PayType: String {
    
    case transfer = "transfer"
    case pay = "pay"
    case merchantTopUp = "merchantTopUp"
    case payPhonePrepaid = "payPhonePrepaid"
    case splitPay = "splitPay"
    case gameVoucher = "gameVoucher"
    case electricity = "electricity"
    case cashierPay = "cashierPay"
    case pdam = "pdam"
}

class TransactionSuccessViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var phoneNumberlbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var transactionIdLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var triangleIV: UIImageView!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var viewTransactionButton: UIButton!
    @IBOutlet weak var backToHomeButton: UIButton!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productNameWrapperView: UIView!
    @IBOutlet weak var productNameWrapperViewHeight: NSLayoutConstraint!
    @IBOutlet weak var splitMemberWrapperView: UIView!
    @IBOutlet weak var splitMemberWrapperViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recipientHintLabel: UILabel!
    @IBOutlet weak var firstMemberIV: CircularImageView!
    @IBOutlet weak var seconMemberdIV: CircularImageView!
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var codeWrapperView: UIView!
    @IBOutlet weak var codeWrapperViewHeight: NSLayoutConstraint!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var codeHintLabel: UILabel!
    
    var order: Order!
    var transaction: Transaction!
    var receipt: Receipt!
    var type: PayType = .pay
    var hidePhoneNumber = false
//    var liveTransactionMainDelegate: LiveTransactionMainDelegate?
//    var animationView: LOTAnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.triangleIV.image = UIImage(named: "ic_triangle")!.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        self.setupData()
//        animationView = LOTAnimationView(name: "lottie_success")
//        animationView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//        animationView.contentMode = .scaleAspectFill
//        animationView.autoReverseAnimation = false
//        animationView.animationSpeed = 0.8
//        animationView.loopAnimation = false
//        animationView.frame = CGRect(x: 0, y: 0, width: self.placeholderView.frame.size.width, height: self.placeholderView.frame.size.height)
//        self.placeholderView.addSubview(animationView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.addDashline(view: self.contentView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        animationView.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.hideNavBar(hide: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.hideNavBar(hide: false)
    }
    
    override func viewWillDisappear(_ animated: Bool)  {
        super.viewWillDisappear(animated)
        self.hideNavBar(hide: false)
    }
    
    func setupData() {
        let referenceNumber = receipt != nil ? receipt.refrenceNumber : order.referenceNo
        var phoneNumber = ""
        var amountText = "-"
        let transactionDate = receipt != nil ? receipt.date : order.createdAt
        
        productNameWrapperView.isHidden = true
        productNameWrapperViewHeight.constant = 0
        codeWrapperView.isHidden = true
        codeWrapperViewHeight.constant = 0
        
        switch type {
        case .merchantTopUp:
            phoneNumber = order.ownable.phoneNumber.sensorsPhoneNumber
            hintLabel.text = "Top Up".localized
            viewTransactionButton.setTitle("View Live Transaction".localized, for: .normal)
            viewTransactionButton.tag = 11
            backToHomeButton.tag = 10
            if order.orderDetails.count > 0 {
                amountText = order.orderDetails[0].total.formattedWithSeparator
            }
            break
        case .payPhonePrepaid, .pdam:
            phoneNumber = receipt.customerId
            productNameWrapperView.isHidden = false
            productNameWrapperViewHeight.constant = 31
            productNameLabel.text = receipt.product
            amountText = receipt.amount.formattedWithSeparator
            if type == .pdam {
                recipientHintLabel.text = "Nomor Pelanggan"
                phoneNumber = receipt.customerId
            }
            break
        case .gameVoucher, .electricity:
            let tap = UITapGestureRecognizer(target: self, action: #selector(copyToClipboard))
            codeLabel.addGestureRecognizer(tap)
            
            codeWrapperView.isHidden = receipt.serialNumber.count == 0
            codeWrapperViewHeight.constant = receipt.serialNumber.count != 0 ? 68 : 0
            codeLabel.text = receipt.serialNumber
            codeHintLabel.text = type == .gameVoucher && receipt.serialNumber.count > 0 ? "Kode Redeem".localized : "Kode Token".localized
            
            productNameWrapperView.isHidden = false
            productNameWrapperViewHeight.constant = 31

            productNameLabel.text = receipt.product
            recipientHintLabel.text = ""
            
            amountText = receipt.amount.formattedWithSeparator
            break
        default:
            if type == .transfer {
                phoneNumber = hidePhoneNumber ? receipt.recipient.sensorsPhoneNumber : receipt.recipient
            }
            if type == .pdam {
                
            }
            else {
                phoneNumber = type == .pay || type == .cashierPay ? receipt.recipientPhone.sensorsPhoneNumber : (hidePhoneNumber ? receipt.receiverPhone.sensorsPhoneNumber : receipt.receiverPhone)
            }
            amountText = receipt.amount.formattedWithSeparator
            if type == .cashierPay {
                self.viewTransactionButton.setTitle("View Live Transaction", for: .normal)
            }
            break
        }
        self.transactionIdLbl.text = referenceNumber
        self.phoneNumberlbl.text = phoneNumber
        self.amountLbl.text = "Rp \(amountText)"
        if receipt != nil {
            if  receipt.walletType != nil && receipt.walletType == 2 {
                self.amountLbl.text = "\(amountText) Point"
            }
        }
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "dd MMM yyyy - h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        if let transactionDate = transactionDate {
            self.dateTimeLbl.text = formatter.string(from: transactionDate)
        }
    }
    
    func setupSplitMembers() {
  
    }
    
    func hideNavBar(hide: Bool) {
        if self.type == .splitPay {
            self.navigationController?.navigationBar.isHidden = hide
        }
    }
    
    @objc func toSplitMemberListScreen() {
//        let nextVC = UIStoryboard.loadSplitMemberListViewController()
//        nextVC.channel = self.channel
//        self.show(nextVC, sender: nil)
    }
    
    @objc func copyToClipboard() {
        UIPasteboard.general.string = codeLabel.text
        self.showToast(message: "Text berhasil di salin!".localized)
    }
    
    func addDashline(view: UIView) {
        view.layer.sublayers?.forEach {
            if $0.name == "dashLayer" {
                $0.removeFromSuperlayer()
            }
        }
        let dashLayer = CAShapeLayer()
        dashLayer.name = "dashLayer"
        dashLayer.strokeColor = UIColor.lightGray.cgColor
        dashLayer.lineDashPattern = [4, 4]
        dashLayer.frame = view.bounds
        dashLayer.fillColor = nil
        dashLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 0).cgPath
        view.layer.addSublayer(dashLayer)
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        if sender.tag == 10 || sender.tag == 11 {
            let willShowLiveTransaction = sender.tag == 11
            dismiss(animated: true, completion: {
//                self.liveTransactionMainDelegate?.refreshCashierAnd(willShowLiveTransaction: willShowLiveTransaction)
            })
            return
        }
        if type == .cashierPay {
//            let initialViewController = UIStoryboard.loadLiveTransactionMainViewController()
//            if sender.tag == 1 {
//                initialViewController.toLiveTransaction = true
//            }
//            self.swapRootView(vc: initialViewController)
        }
        else {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainTabarViewController")
            self.swapRootView(vc: vc)
            self.getProfile()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            print("b")
            self.scrollView.backgroundColor = UIColor.lightGray
        }
        
        if (scrollView.contentOffset.y <= 0){
            print("t")
            self.scrollView.backgroundColor = UIColor.white
            //reach top
        }
        
        if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
            //not top and not bottom
        }
    }
}
