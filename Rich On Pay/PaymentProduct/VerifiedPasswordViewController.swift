//
//  VerifiedPasswordViewController.swift
//  Rich On Pay
//
//  Created by Edison on 11/8/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

enum PasswordViewType {
    case payPhonePrepaid
    case electricity
    case gameVoucher
}

class VerifiedPasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var errorLbl: UILabel!
    @IBOutlet weak var passwordTextfield: UITextField!
    var paymentProductId: Int!
    var credentials: String!
    var confirmViewType = PasswordViewType.payPhonePrepaid
    var orderId: Int!
    var customerId: String!
    var selectedPaymentProduct: PaymentProduct!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 10
        self.view.clipsToBounds = true
        passwordTextfield.becomeFirstResponder()
        passwordTextfield.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
    }
    
    @IBAction func onClosePressed(_ sender: UIButton) {
        view.endEditing(true)
        if let delegate = navigationController?.transitioningDelegate as? HalfModalTransitioningDelegate {
            delegate.interactiveDismiss = false
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func eyeBtnTapped(_ sender: UIButton) {
        sender.tag = sender.tag == 0 ? 1 : 0 
        passwordTextfield.isSecureTextEntry = sender.tag == 0
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField) {
        errorLbl.text = ""
    }
    
    func callFinishPhonePrepaidPaymentAPI() {
        API.request(API.Service.phonePrepaidPayment(paymentProductId: paymentProductId, phoneNumber: credentials, password: self.passwordTextfield.text!), showLoading: true, success: {
            (response: APIResponse) in
            DispatchQueue.main.async {
                let nextViewController = UIStoryboard.loadTransactionSuccessViewController()
                nextViewController.receipt = response.data?.receipt
                nextViewController.type = .payPhonePrepaid
                self.present(nextViewController, animated: true, completion: nil)
            }
        }, failure: {
            (error) in
            guard let message = error.message else {
                return
            }
            self.errorLbl.text = message
        })
    }
    
    func callFinishElectricityPaymentAPI() {
        _ = paymentProductId != nil
        if paymentProductId == nil {
            paymentProductId = 0
        }
        API.request(API.Service.electricityPayment(password: passwordTextfield.text!, orderId: orderId, paymentProductId: paymentProductId, customerId: customerId), showLoading: true, success: {
            (response: APIResponse) in
            DispatchQueue.main.async {
                let nextViewController = UIStoryboard.loadTransactionSuccessViewController()
                nextViewController.receipt = response.data?.receipt
                nextViewController.type = .electricity
                self.present(nextViewController, animated: true, completion: nil)
            }
        }, failure: {
            (error) in
            guard let message = error.message else {
                return
            }
            self.errorLbl.text = message
        })
    }
    
    func callFinishGameVoucherPaymentAPI() {
        API.request(API.Service.gameVoucherPayment(paymentProductId: paymentProductId, password: passwordTextfield.text!), showLoading: true, success: {
            (response: APIResponse) in
            DispatchQueue.main.async {
                let nextViewController = UIStoryboard.loadTransactionSuccessViewController()
                nextViewController.receipt = response.data?.receipt
                nextViewController.type = .gameVoucher
                self.present(nextViewController, animated: true, completion: nil)
            }
        }, failure: {
            (error) in
            guard let message = error.message else {
                return
            }
            self.errorLbl.text = message
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch confirmViewType {
        case .payPhonePrepaid:
            callFinishPhonePrepaidPaymentAPI()
            break
        case .gameVoucher:
            callFinishGameVoucherPaymentAPI()
            break
        default:
            callFinishElectricityPaymentAPI()
            break
        }
        return true
    }
    
}
