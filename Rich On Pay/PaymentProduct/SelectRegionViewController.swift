//
//  SelectRegionViewController.swift
//  Mareco
//
//  Created by Edison on 9/13/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit

protocol RegionDelegate: class {
    func finishSelected(region: RegionPaymentProducts)
}

class SelectRegionViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var categories = [RegionCategories]()
    var delegate: RegionDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pilih Wilayah"
        self.getRegion()
        tableView.tableFooterView?.isHidden = true
    }
    
    private func getRegion() {
        API.request(API.Service.getPaymentProductByType(type: 5), showLoading: true, success: { (response: APIResponse) in
            guard let categories = response.data?.regionCategories else {
                return
            }
            print(categories.count)
            self.categories = categories
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { (error) in
            
        }
    }
    
}

extension SelectRegionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories[section].paymentProducts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! RegionTableCell
        headerCell.productLbl.text = self.categories[section].category
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RegionTableCell
        cell.productLbl.text = self.categories[indexPath.section].paymentProducts[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.finishSelected(region: self.categories[indexPath.section].paymentProducts[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
    
}

class RegionTableCell: UITableViewCell {
    
    @IBOutlet weak var productLbl: UILabel!
    
}

