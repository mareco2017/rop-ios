//
//  PDAMViewController.swift
//  Mareco
//
//  Created by Edison on 9/12/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class PDAMViewController: UIViewController, RegionDelegate {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var paymentDetailView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var paymentDetailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var customerNumberTF: PhoneNumberTextField!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var currentBalanceLabel: UILabel!
    
    @IBOutlet weak var customerNumberLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var resiLbl: UILabel!
    @IBOutlet weak var totalBillLbl: UILabel!
    @IBOutlet weak var pamLbl: UILabel!
    @IBOutlet weak var periodLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var totalDendaLbl: UILabel!
    @IBOutlet weak var billNonAirLbl: UILabel!
    @IBOutlet weak var stampLbl: UILabel!
    @IBOutlet weak var adminPriceLbl: UILabel!
    @IBOutlet weak var buttomTotalBillLbl: UILabel!
    
    var paymentId: Int!
    var timer: Timer!
    var paymentProductId: Int?
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?
    var order: Order?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBalanceLabel()
        prepareView()
    }
    
    fileprivate func prepareView() {
//        toggleWrapperViewState(visible: false)
        customerNumberTF.keyboardType = .phonePad
        customerNumberTF.addTarget(self, action: #selector(numberTextFieldDidChanged(_:)), for: .editingChanged)
        selectBtn.alignImageOnTrailingEdge()
    }
    
    fileprivate func updateBalanceLabel() {
        guard let user = Defaults[.user] else { return }
//        guard let userBalance = user.balances?.primary else { return }
//        let balance = NSMutableAttributedString(string: "Rp \(userBalance.formattedWithSeparator)")
//        let rpRange = NSRange(location: 0,length: 3)
//        let balanceRange = NSRange(location:3,length: balance.length - 3)
//        balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "SFProDisplay-Semibold", size: 16.interfaceIdiomSize(size: 5))!, range: rpRange)
//        balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "SFProDisplay-Bold", size: 22.interfaceIdiomSize(size: 5))!, range: balanceRange)
//        currentBalanceLabel.attributedText = balance
    }
    
    func finishSelected(region: RegionPaymentProducts) {
        self.paymentProductId = region.id
        getPaymentInformation()
        selectBtn.setTitle(region.name, for: .normal)
        selectBtn.alignImageOnTrailingEdge(withOffset: -10)
    }
    
    @IBAction func numberTextFieldDidChanged(_ sender: UITextField) {
        if timer != nil {
            timer.invalidate()
        }
        self.timer = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(getPaymentInformation), userInfo: nil, repeats: false)
    }
    
    @objc func getPaymentInformation() {
        if let paymentProductId = self.paymentProductId {
            if self.customerNumberTF.text!.count > 2 {
                API.request(API.Service.inquiryPDAM(type: 5, productId: paymentProductId, customerId: self.customerNumberTF.text!), showLoading: false, success: { (response: APIResponse) in
                    guard let order = response.data?.order else {
                        return
                    }
                    self.order = order
                    DispatchQueue.main.async {
                        self.setupView(order: order)
                    }
                }, failure: { (error) in
                    
                })
            }
        }
    }
    
    func setupView(order: Order) {
        self.paymentDetailHeightConstraint.constant = 590
        self.paymentDetailView.isHidden = false
        self.bottomView.isHidden = false
        self.customerNumberLbl.text = self.customerNumberTF.text!
        if let orderOption = order.option {
            self.fullNameLbl.text = orderOption.customerName
            self.resiLbl.text = orderOption.ref2
            self.totalBillLbl.text = "\(orderOption.months.count)"
            self.pamLbl.text = orderOption.pdamName
            var monthsString = ""
            for (i, month) in orderOption.months.enumerated() {
                if i == 0 {
                    monthsString = month
                }
                else {
                    monthsString = "\(monthsString), \(month)"
                }
            }
            self.periodLbl.text = monthsString
            self.totalPriceLbl.text = "Rp " + orderOption.totalAmount.formattedWithSeparator
            self.totalDendaLbl.text = "Rp " + orderOption.penalty.formattedWithSeparator
            self.adminPriceLbl.text = "Rp " + orderOption.fee.formattedWithSeparator
            self.buttomTotalBillLbl.text = "Rp " + orderOption.amount.formattedWithSeparator
        }
    }
    
    @IBAction func showRegionSelectionScreen() {
        let nextViewController = UIStoryboard.loadSelectRegionViewController()
        nextViewController.delegate = self
        show(nextViewController, sender: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        guard let order = self.order else {
            return
        }
//        let nextViewController = UIStoryboard.loadVerificationPaymentViewController()
//        let wallets = Wallets()
//        wallets.primary = Defaults[.user]?.balances?.primary
//        wallets.point = Defaults[.user]?.balances?.point
//        nextViewController.wallets = wallets
//        nextViewController.amount = order.option.totalAmount
//        nextViewController.customerId = self.customerNumberTF.text!
//        nextViewController.paymentId = self.paymentProductId
//        nextViewController.orderId = order.id
//        nextViewController.pinViewType = .pdam
//
//        let nav = UINavigationController(rootViewController: nextViewController)
//        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nextViewController)
//        nav.modalPresentationStyle = .custom
//        nav.transitioningDelegate = self.halfModalTransitioningDelegate
//        self.present(nav, animated: true, completion: nil)
    }
    
    
}
