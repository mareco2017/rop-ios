//
//  ElectricityPaymentViewController.swift
//  Mareco
//
//  Created by Edy on 11/5/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ElectricityPaymentViewController: UIViewController {
    
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var infoWrapperView: UIView!
    @IBOutlet weak var customerDetailSectionWrapperView: UIView!
    @IBOutlet weak var customerDetailSectionWrapperHeight: NSLayoutConstraint!
    @IBOutlet weak var billDetailWrapperView: UIView!
    @IBOutlet weak var billDetailWrapperHeight: NSLayoutConstraint!
    @IBOutlet weak var amountSelectionWrapperView: UIView!
    @IBOutlet weak var amountSelectionWrapperHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentSectionWrapperView: UIView!
    @IBOutlet weak var paymentSectionWrapperHeight: NSLayoutConstraint!
    @IBOutlet weak var standMeterTopCT: NSLayoutConstraint!
    @IBOutlet weak var billTypeButton: UIButton!
    @IBOutlet weak var tokenTypeButton: UIButton!
    
    @IBOutlet weak var customerIdTextField: UITextField!
    @IBOutlet weak var customerDetailSectionTitleLabel: UILabel!
    @IBOutlet weak var customerIdLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var powerRateLabel: UILabel!
    @IBOutlet weak var billCountTitleLabel: UILabel!
    @IBOutlet weak var billCountLabel: UILabel!
    @IBOutlet weak var standMeterTitleLbl: UILabel!
    @IBOutlet weak var standMeterCountLbl: UILabel!
    
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var fineTotalLabel: UILabel!
    @IBOutlet weak var adminFeeLabel: UILabel!
    @IBOutlet weak var billTotalLabel: UILabel!
    
    @IBOutlet weak var grandTotalLbl: UILabel!
    @IBOutlet weak var adminFeeLbl: UILabel!
    @IBOutlet weak var amountButton: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    var selectedTag = 0
    var prepaidOrder: Order!
    var postpaidOrder: Order!
    var paymentProducts: [PaymentProduct]!
    var selectedPaymentProduct: PaymentProduct!
    var prepaidCustomerId = ""
    var postpaidCustomerId = ""
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Listrik"
        infoWrapperView.layer.cornerRadius = 2
        infoWrapperView.layer.borderColor = UIColor.primary.cgColor
        infoWrapperView.layer.borderWidth = 1
        
        customerIdTextField.delegate = self
        
        updateBalanceLabel()
        
        toggleBillWrapperViewState(visible: false)
        toggleTokenWrapperViewState(visible: false)
    }
    
    fileprivate func updateBalanceLabel() {
        guard let user = Defaults[.user] else { return }
        if let wallets = user.wallets {
            if wallets.count > 0 {
                let userBalance = wallets[0].balance!
                let balance = NSMutableAttributedString(string: "Rp \(userBalance.formattedWithSeparator)")
                let rpRange = NSRange(location: 0,length: 3)
                let balanceRange = NSRange(location:3,length: balance.length - 3)
                balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Roboto-Regular", size: 16.interfaceIdiomSize(size: 5))!, range: rpRange)
                balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Roboto-Bold", size: 22.interfaceIdiomSize(size: 5))!, range: balanceRange)
                currentBalanceLabel.attributedText = balance
            }
        }
    }
    
    func toggleBillWrapperViewState(visible: Bool) {
        customerDetailSectionWrapperView.isHidden = !visible
        customerDetailSectionWrapperHeight.constant = visible ? 220 : 0
        billDetailWrapperView.isHidden = !visible
        billDetailWrapperHeight.constant = visible ? 280 : 0
        amountSelectionWrapperView.isHidden = true
        amountSelectionWrapperHeight.constant = 0
        paymentSectionWrapperView.isHidden = !visible
        paymentSectionWrapperHeight.constant = visible ? 165 : 0
        billCountTitleLabel.isHidden = false
        billCountLabel.isHidden = false
        standMeterTitleLbl.text = "Stand Meter".localized
    }
    
    func toggleTokenWrapperViewState(visible: Bool) {
        customerDetailSectionWrapperView.isHidden = !visible
        customerDetailSectionWrapperHeight.constant = visible ? 220 : 0
        billDetailWrapperView.isHidden = true
        billDetailWrapperHeight.constant = 0
        amountSelectionWrapperView.isHidden = !visible
        amountSelectionWrapperHeight.constant = visible ? 100 : 0
        paymentSectionWrapperView.isHidden = !visible
        paymentSectionWrapperHeight.constant = visible ? 165 : 0
        billCountTitleLabel.isHidden = true
        billCountLabel.isHidden = true
//        standMeterTopCT.constant = 0
        standMeterTitleLbl.text = "ID Pelanggan".localized
    }
    
    func setDataOnView(forPrepaid: Bool) {
        if forPrepaid {
            customerIdTextField.text = prepaidCustomerId
            if prepaidOrder != nil {
                customerIdLabel.text = prepaidOrder.option.meterNo
                customerNameLabel.text = prepaidOrder.option.customerName
                powerRateLabel.text = prepaidOrder.option.powerRate
                standMeterCountLbl.text = prepaidOrder.option.customerId
                updateViewWith(paymentProduct: selectedPaymentProduct)
                toggleTokenWrapperViewState(visible: true)
            }
            else {
                toggleBillWrapperViewState(visible: false)
                toggleTokenWrapperViewState(visible: false)
            }
        }
        else {
            customerIdTextField.text = postpaidCustomerId
            if postpaidOrder != nil {
                customerIdLabel.text = postpaidOrder.option.customerId
                customerNameLabel.text = postpaidOrder.option.customerName
                powerRateLabel.text = postpaidOrder.option.powerRate
                billCountLabel.text = "\(postpaidOrder.option.billCount!)"
                periodLabel.text = postpaidOrder.option.period
                standMeterCountLbl.text = postpaidOrder.option.standMeter
                if let penalty = postpaidOrder.option.penalty {
                    fineTotalLabel.text = "Rp \(penalty.formattedWithSeparator)"
                }
                else {
                    fineTotalLabel.text = "Rp 0"
                }
                adminFeeLabel.text = "Rp \(postpaidOrder.option.adminCharge.formattedWithSeparator)"
                billTotalLabel.text = "Rp \(postpaidOrder.option.amount.formattedWithSeparator)"
                let totalPrice = postpaidOrder.option.adminCharge + postpaidOrder.option.penalty + postpaidOrder.option.amount!
                totalPriceLabel.text = "Rp \(totalPrice.formattedWithSeparator)"
                adminFeeLbl.text = "Rp \(postpaidOrder.option.plnFee.formattedWithSeparator)"
                let grandTotal = totalPrice + postpaidOrder.option.plnFee
                grandTotalLbl.text = "Rp \(grandTotal.formattedWithSeparator)"
                toggleBillWrapperViewState(visible: true)
            }
            else {
                toggleBillWrapperViewState(visible: false)
                toggleTokenWrapperViewState(visible: false)
            }
        }
    }
    
    @IBAction func productTypeButtonDidTapped(_ sender: UIButton) {
        if selectedTag != sender.tag {
            selectedTag = sender.tag
            let radioOnIcon = UIImage(named: "ic_radio_on")
            let radioOffIcon = UIImage(named: "ic_radio_off")
            billTypeButton.setImage(selectedTag == 0 ? radioOnIcon : radioOffIcon, for: .normal)
            tokenTypeButton.setImage(selectedTag == 1 ? radioOnIcon : radioOffIcon, for: .normal)
            
            if selectedTag == 0 {
                toggleBillWrapperViewState(visible: true)
                toggleTokenWrapperViewState(visible: false)
                setDataOnView(forPrepaid: false)
            }
            else {
                setDataOnView(forPrepaid: true)
            }
        }
    }
    
    @IBAction func amountButtonDidTapped(_ sender: UIButton) {
        showPaymentProductAmountSelectionScreen()
    }
    
    func showPaymentProductAmountSelectionScreen() {
        let nextViewController = UIStoryboard.loadPaymentProductAmountSelectionViewController()
        nextViewController.delegate = self
        nextViewController.paymentProducts = paymentProducts
        nextViewController.type = .electricity
        show(nextViewController, sender: nil)
    }
    
    func updateViewWith(paymentProduct: PaymentProduct) {
        amountButton.setTitle(paymentProduct.name, for: .normal)
        amountButton.alignImageOnTrailingEdge(withOffset: -10)
        
        let totalPrice = paymentProduct.price ?? 0
        totalPriceLabel.text = "Rp \(totalPrice.formattedWithSeparator)"
        adminFeeLbl.text = "Rp \(prepaidOrder.option.plnFee.formattedWithSeparator)"
        let grandTotal = totalPrice + prepaidOrder.option.plnFee
        grandTotalLbl.text = "Rp \(grandTotal.formattedWithSeparator)"
        selectedPaymentProduct = paymentProduct
    }
    
    func callGetElectricityInquiry(forPrepaid: Bool, customerId: String) {
        API.request(API.Service.electricityInquiry(isPrepaid: forPrepaid, customerId: customerId), showLoading: true, success: {
            (response: APIResponse) in
            guard let data = response.data else { return }
            if forPrepaid {
                self.prepaidOrder = data.order
                self.paymentProducts = data.paymentProducts
                if self.paymentProducts.count > 0 {
                    self.selectedPaymentProduct = self.paymentProducts[0]
                }
            }
            else {
                self.postpaidOrder = data.order
            }
            self.setDataOnView(forPrepaid: forPrepaid)
        }, failure: {
            (error) in
            if forPrepaid {
                self.prepaidOrder = nil
            }
            else {
                self.postpaidOrder = nil
            }
            self.setDataOnView(forPrepaid: forPrepaid)
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    @IBAction func buyButtonDidTapped(_ sender: UIButton) {
        if selectedPaymentProduct == nil && selectedTag == 1 {
            AlertView.showAlert(view: self, message: "No amount selected yet", title: "Sorry".localized)
            return
        }
//        let userBalance = Defaults[.user]?.balances?.primary ?? 0
//        if selectedTag == 0 && postpaidOrder != nil {
//            let totalPrice = postpaidOrder.option.adminCharge + postpaidOrder.option.penalty + postpaidOrder.option.amount!
//            if userBalance < totalPrice {
//                AlertView.showAlert(view: self, message: "Insufficient Balance".localized, title: "Sorry".localized)
//                return
//            }
//        }
//        if selectedTag == 1 && userBalance < selectedPaymentProduct.price {
//            AlertView.showAlert(view: self, message: "Insufficient Balance".localized, title: "Sorry".localized)
//            return
//        }
//        let nextViewController = UIStoryboard.loadVerificationPaymentViewController()
//        let wallets = Wallets()
//        wallets.primary = Defaults[.user]?.balances?.primary
//        wallets.point = Defaults[.user]?.balances?.point
//        nextViewController.wallets = wallets
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerifiedPasswordViewController") as! VerifiedPasswordViewController
        
//        nextViewController.paymentProductId = selectedPaymentProduct.id
        nextViewController.confirmViewType = .payPhonePrepaid
        nextViewController.orderId = selectedTag == 0 ? postpaidOrder.id : prepaidOrder.id
        nextViewController.customerId = selectedTag == 0 ? postpaidCustomerId : prepaidCustomerId
        nextViewController.confirmViewType = .electricity
        if selectedTag != 0 {
            nextViewController.selectedPaymentProduct = selectedPaymentProduct
        }
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nextViewController)
        nextViewController.modalPresentationStyle = .custom
        nextViewController.transitioningDelegate = self.halfModalTransitioningDelegate
        self.present(nextViewController, animated: true, completion: nil)
        
//        if selectedTag == 0 {
//            nextViewController.productName = "Tagihan Listrik"
//            let totalPrice = postpaidOrder.option.adminCharge + postpaidOrder.option.penalty + postpaidOrder.option.amount!
//            nextViewController.amount = totalPrice
//        }
//        else {
//            nextViewController.selectedPaymentProduct = selectedPaymentProduct
//            nextViewController.productName = selectedPaymentProduct.name
//            nextViewController.amount = selectedPaymentProduct.price
//        }

//
//        let nav = UINavigationController(rootViewController: nextViewController)
//        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nextViewController)
//        nav.modalPresentationStyle = .custom
//        nav.transitioningDelegate = self.halfModalTransitioningDelegate
//        self.present(nav, animated: true, completion: nil)
    }
    
}

extension ElectricityPaymentViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        return string.isNumberFiltered && newLength <= 12
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        if selectedTag == 1 {
            prepaidCustomerId = text
        }
        else {
            postpaidCustomerId = text
        }
        callGetElectricityInquiry(forPrepaid: selectedTag == 1, customerId: text)
    }
}

extension ElectricityPaymentViewController: PaymentProductAmountSelectionDelegate {
    func didSelectPaymentProductAmount(for paymentProduct: PaymentProduct) {
        updateViewWith(paymentProduct: paymentProduct)
    }
}
