//
//  PrepaidPhoneCreditViewController.swift
//  Mareco
//
//  Created by Edy on 30/4/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import ContactsUI

class PrepaidPhoneCreditViewController: UIViewController {
    
    @IBOutlet weak var amountButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var amountWrapperView: UIView!
    @IBOutlet weak var amountWrapperViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adminFeeLbl: UILabel!
    
    let pickerView = UIPickerView()
    let pickerTextField = UITextField()
    let popoverViewController = UIViewController()
    var paymentProducts = [PaymentProduct]()
    var paymentProductCategories = [PaymentProductCategory]()
    var filteredPaymentProducts = [PaymentProduct]()
    var selectedCategoryId: Int!
    var type = 1
    var selectedPaymentProduct: PaymentProduct!
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?
    var limitUser: Double = 0
    var currentLimitUser: Double = 0
    var adminFee: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Pulsa"
        prepareView()
        callGetPaymentProductsAPI()
        updateBalanceLabel()
    }
    
    func callGetPaymentProductsAPI() {
        API.request(API.Service.getPaymentProductByType(type: type), showLoading: true, success: {
            (response: APIResponse) in
            guard let data = response.data else { return }
            self.limitUser = data.limitUser
            self.adminFee = data.adminFee
            self.currentLimitUser = data.currentLimitUser
            self.paymentProducts = data.paymentProducts
            self.paymentProductCategories = data.paymentProductCategories
            DispatchQueue.main.async {
                self.prepareView()
            }
        }, failure: {
            (error) in
            let error = error.message ?? "Unknown Error Occurred".localized
            AlertView.showAlert(view: self, message: error, title: "Sorry".localized)
        })
    }
    
    fileprivate func prepareView() {
        toggleWrapperViewState(visible: false)
        phoneNumberTextField.text = Defaults[.user]?.phoneNumber
        phoneNumberTextField.keyboardType = .phonePad
        validatePhoneNumberHeader(with: Defaults[.user]?.phoneNumber ?? "")
        phoneNumberTextField.addTarget(self, action: #selector(phoneNumberTextFieldDidChanged(_:)), for: .editingChanged)
        amountButton.alignImageOnTrailingEdge()
    }
    
    fileprivate func updateBalanceLabel() {
        guard let user = Defaults[.user] else { return }
        if let wallets = user.wallets {
            if wallets.count > 0 {
                    let userBalance = wallets[0].balance!
                let balance = NSMutableAttributedString(string: "Rp \(userBalance.formattedWithSeparator)")
                    let rpRange = NSRange(location: 0,length: 3)
                    let balanceRange = NSRange(location:3,length: balance.length - 3)
                    balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Roboto-Regular", size: 16.interfaceIdiomSize(size: 5))!, range: rpRange)
                    balance.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Roboto-Bold", size: 22.interfaceIdiomSize(size: 5))!, range: balanceRange)
                    currentBalanceLabel.attributedText = balance
            }
        }
    }
    
    @IBAction func contactButtonDidTapped(_ sender: UIButton) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
    }
    
    func toggleWrapperViewState(visible: Bool) {
        if !(self.amountWrapperViewHeight.constant == 160.0 && visible) {
            self.amountWrapperViewHeight.constant = visible ? 160.0 : 0
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
                self.amountWrapperView.alpha = visible ? 1.0 : 0.0
            })
        
            iconImageView.isHidden = !visible
        }
    }

    @IBAction func amountButtonDidTapped(_ sender: UIButton) {
        showPaymentProductAmountSelectionScreen()
    }

    func updateViewWith(paymentProduct: PaymentProduct) {
        amountButton.setTitle(paymentProduct.name, for: .normal)
        amountButton.alignImageOnTrailingEdge(withOffset: -10)
        var totalPrice = paymentProduct.price ?? 0
        print(totalPrice)
        if (totalPrice + currentLimitUser > limitUser) {
            totalPrice += adminFee
            adminFeeLbl.text = "Rp \(adminFee.formattedWithSeparator)"
        }
        print(totalPrice)
        priceLabel.text = "Rp \(totalPrice.formattedWithSeparator)"
        selectedPaymentProduct = paymentProduct
    }
    
    @IBAction func phoneNumberTextFieldDidChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        validatePhoneNumberHeader(with: text)
    }
    
    func getPhoneNumberHeader(from text: String) -> String {
        if text.first == "0" && text.count >= 4 {
            let newText = (text as NSString).substring(to: 4)
            return newText
        }
        else if text.first == "+" && text.count >= 6 {
            let newText = (text as NSString).substring(to: 6)
            return newText
        }
        return ""
    }
    
    func validatePhoneNumberHeader(with textToValidate: String) {
        var text = getPhoneNumberHeader(from: textToValidate)
        if text.count >= 4 {
            if text.first == "0" {
                text.removeFirst()
                text = "+62" + text
                getPhoneNumberType(with: text)
            }
            else if text.first == "+" {
                getPhoneNumberType(with: text)
            }
        }
        else if text.count < 4 {
            toggleWrapperViewState(visible: false)
        }
    }
    
    func getPhoneNumberType(with text: String)  {
        for category in paymentProductCategories {
            guard let data = category.data else { return toggleWrapperViewState(visible: false) }
            if data.index(of: text) != nil {
                selectedCategoryId = category.id
                iconImageView.setImage(url: category.pictureUrl, placeholderImage: nil)
                iconImageView.isHidden = false
                updateAmountPicker()
                return
            }
        }
        toggleWrapperViewState(visible: false)
    }
    
    func updateAmountPicker() {
        filteredPaymentProducts = paymentProducts.filter { $0.categoryId == selectedCategoryId }
        if filteredPaymentProducts.count > 0 {
            let selectedAmountText = "\(Int(filteredPaymentProducts[0].amount!).formattedWithSeparator)"
            amountButton.setTitle(selectedAmountText, for: .normal)
            updateViewWith(paymentProduct: filteredPaymentProducts[0])
            toggleWrapperViewState(visible: true)
        }
    }
    
    func showPaymentProductAmountSelectionScreen() {
        let nextViewController = UIStoryboard.loadPaymentProductAmountSelectionViewController()
        nextViewController.delegate = self
        nextViewController.paymentProducts = filteredPaymentProducts
        show(nextViewController, sender: nil)
    }
    
    @IBAction func buyButtonDidTapped(_ sender: UIButton) {
        view.endEditing(true)
        if selectedPaymentProduct == nil {
            AlertView.showAlert(view: self, message: "No amount selected yet", title: "Sorry".localized)
            return
        }
        if let user = Defaults[.user] {
            if let wallets = user.wallets {
                if wallets.count > 0 {
                    let primaryBalance = wallets[0].balance!
                    if selectedPaymentProduct.price > primaryBalance {
                        AlertView.showAlert(view: self, message: "Insufficient Balance", title: "Sorry".localized)
                        return
                    }
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerifiedPasswordViewController") as! VerifiedPasswordViewController
                    nextViewController.credentials = self.phoneNumberTextField.text!
                    nextViewController.paymentProductId = selectedPaymentProduct.id
                    nextViewController.confirmViewType = .payPhonePrepaid
                    self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nextViewController)
                    nextViewController.modalPresentationStyle = .custom
                    nextViewController.transitioningDelegate = self.halfModalTransitioningDelegate
                    self.present(nextViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
}

extension PrepaidPhoneCreditViewController: PaymentProductAmountSelectionDelegate {
    
    func didSelectPaymentProductAmount(for paymentProduct: PaymentProduct) {
        updateViewWith(paymentProduct: paymentProduct)
    }
}

// MARK: - CNContactPickerDelegate
extension PrepaidPhoneCreditViewController: CNContactPickerDelegate {
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        picker.dismiss(animated: true, completion: nil)

        // Check if the contact has multiple phone numbers
        if contact.phoneNumbers.count > 1 {
            //If so we need the user to select which phone number we want them to use
            let multiplePhoneNumbersAlert = UIAlertController(title: "Which one?", message: "This contact has multiple phone numbers, which one did you want use?", preferredStyle: UIAlertControllerStyle.alert)
            
            for number in contact.phoneNumbers {
                let actualNumber = number.value as CNPhoneNumber
                let phoneNumberLabel = number.label
                let actionTitle = phoneNumberLabel! + " - " + actualNumber.stringValue
                let numberAction = UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default, handler: { (theAction) -> Void in
                    self.phoneNumberTextField.text = actualNumber.stringValue.replacingOccurrences(of: "[^0-9+]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                })
                multiplePhoneNumbersAlert.addAction(numberAction)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (theAction) -> Void in
                //Cancel action completion
            })
            multiplePhoneNumbersAlert.addAction(cancelAction)
            present(multiplePhoneNumbersAlert, animated: true, completion: nil)
        } else {
            //Make sure we have at least one phone number
            if contact.phoneNumbers.count > 0 {
                let actualNumber = (contact.phoneNumbers.first?.value)! as CNPhoneNumber
                _ = contact.phoneNumbers.first!.label
                self.phoneNumberTextField.text = actualNumber.stringValue.replacingOccurrences(of: "[^0-9+]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
            } else {
                let alert = UIAlertController(title: "Missing info", message: "You have no phone numbers associated with this contact", preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                present(alert, animated: true, completion: nil)
            }
        }
    }
}
