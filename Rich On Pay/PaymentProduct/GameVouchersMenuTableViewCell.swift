//
//  GameVouchersMenuTableViewCell.swift
//  Mareco
//
//  Created by Edy on 11/5/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit

class GameVouchersMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setupCellWith(data: PaymentProductCategory) {
        iconImageView.setImage(url: data.pictureUrl, placeholderImage: nil)
        titleLabel.text = data.category
        selectionStyle = .none
    }
    
}
