//
//  Region.swift
//  Mareco
//
//  Created by Edison on 9/13/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import ObjectMapper

class RegionCategories: Mappable {
    
    var id: Int!
    var category: String!
    var paymentProducts: [RegionPaymentProducts]!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        category <- map["category"]
        paymentProducts <- map["payment_products"]
    }
    
}

class RegionPaymentProducts: Mappable {
    
    var id: Int!
    var name: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
    
}

