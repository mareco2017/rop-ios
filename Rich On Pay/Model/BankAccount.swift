//
//  PaymentBankAccount.swift
//  Mareco
//
//  Created by Edy on 12/4/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import ObjectMapper

class Bank: Mappable {
    
    var id: Int!
    var name: String!
    var logo: String!
    var code: Int!
    var status: Int!
    var abbr: String!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        logo <- map["cover_url"]
        code <- map["code"]
        abbr <- map["abbr"]
    }
    
}

class UserBank: Mappable {
    
    var id: Int!
    var accountName: String!
    var accountNumb: String!
    var stat: Int!
    var isDefault: Int!
    var bank: Bank!
//    var invoice: WalletInvoice!
    var type: Int!
    var status: Int!
    
    required init?(map: Map) {
    }
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        accountName <- map["account_name"]
        accountNumb <- map["account_no"]
        stat <- map["stat"]
        bank <- map["bank"]
        isDefault <- map["is_default"]
//        invoice <- map["inv"]
        type <- map["type"]
        status <- map["status"]
    }
    
}

class InstructionBank: Mappable {
    
    var id: Int!
    var content: String!
    var bank: Bank!
    var isDropdown = false
    
    required init?(map: Map) {
    }
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        content <- map["content"]
        bank <- map["bank"]
    }
    
}

class BankVirtual: Mappable {
    
    var id: Int!
    var bankAccount: BankAccount!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["bank_account_id"]
        bankAccount <- map["bank_account"]
    }
    
}


class BankAccount: Mappable {
    
    var id: Int!
    var accountName: String!
    var accountNo: String!
    var ownableType: String!
    var ownableId: Int!
    var branch: String!
    var isPrimary: Int!
    var bank: Bank!
    var bankDetail: Bank!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        accountName <- map["account_name"]
        accountNo <- map["account_no"]
        ownableType <- map["ownable_type"]
        ownableId <- map["ownable_id"]
        branch <- map["branch"]
        isPrimary <- map["is_primary"]
        bank <- map["bank"]
        bankDetail <- map["bank_detail"]
    }
}
