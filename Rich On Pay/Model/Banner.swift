//
//  Banner.swift
//  Rich On Pay
//
//  Created by Edison on 10/11/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import ObjectMapper

class Banner: Mappable {
    
    var id: Int!
    var title: String!
    var descriptionString: String!
    var cover: Cover!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        descriptionString <- map["description"]
        cover <- map["cover"]
    }
}

class Cover: Mappable {
    
    var url: String!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        url <- map["url"]
    }
}
