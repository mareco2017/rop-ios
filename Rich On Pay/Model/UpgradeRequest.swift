//
//  UpgradeRequest.swift
//  Rich On Pay
//
//  Created by Edison on 10/10/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import ObjectMapper

class UpgradeRequest: Mappable {
    
    var id: Int!
    var package: Int!
    var price: Double!
    var requestBy: User!
    var expireAt: Date!
    var createdAt: Date!
    var status: Int!
    var code: String!
    var referenceNumber: String!
    var virtual: BankVirtual?
    var orderDetails: [RequestOrderDetail]!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        package <- map["package"]
        price <- map["price"]
        requestBy <- map["requested_by"]
        status <- map["status"]
        code <- map["code"]
        expireAt <- (map["expired_at"], DateTransform())
        createdAt <- (map["created_at"], DateTransform())
        requestBy <- map["requested_by"]
        virtual <- map["virtual"]
        referenceNumber <- map["reference_number"]
        orderDetails <- map["order_details"]
    }
    
}

class RequestOrderDetail: Mappable {
    
    var price: Double!
    var subtotal: Double!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        price <- map["price"]
        subtotal <- map["sub_total"]
    }
}


