//
//  User.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject,Mappable,NSCoding {
    
    var id:Int!
    var sponsorId: Int?
    var leaderId: Int?
    var phoneNumber: String = ""
    var email: String!
    var firstName: String!
    var fullName: String!
    var profilePicUrl: String = ""
    var packages: Int!
    var gender: Int!
    var referralCode: String = ""
    var verificationStatus: VerificationStatus!
    var wallets: [Wallet]!
    
    required init?(map: Map) {
    }
    
    override init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        phoneNumber <- map["phone_number"]
        email <- map["email"]
        firstName <- map["first_name"]
        fullName <- map["fullname"]
        profilePicUrl <- map["profile_picture_url"]
        referralCode <- map["referral_code"]
        sponsorId <- map["sponsor_id"]
        leaderId <- map["leader_id"]
        packages <- map["packages"]
        gender <- map["gender"]
        verificationStatus <- map["verifications_status"]
        wallets <- map["wallets"]
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        phoneNumber = aDecoder.decodeObject(forKey: "phone_number") as! String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        fullName = aDecoder.decodeObject(forKey: "fullname") as? String
        profilePicUrl = aDecoder.decodeObject(forKey: "profile_picture_url") as! String
        referralCode = aDecoder.decodeObject(forKey: "referral_code") as! String
        sponsorId = aDecoder.decodeObject(forKey: "sponsor_id") as? Int
        leaderId = aDecoder.decodeObject(forKey: "leader_id") as? Int
        packages = aDecoder.decodeObject(forKey: "packages") as! Int
        gender = aDecoder.decodeObject(forKey: "gender") as! Int
        verificationStatus = aDecoder.decodeObject(forKey: "verifications_status") as? VerificationStatus
        wallets = aDecoder.decodeObject(forKey: "wallets") as? [Wallet]
    }
    
    func encode(with aCoder: NSCoder)  {
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(phoneNumber, forKey: "phone_number")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(firstName, forKey: "first_name")
        aCoder.encode(fullName, forKey: "fullname")
        aCoder.encode(profilePicUrl, forKey: "profile_picture_url")
        aCoder.encode(referralCode, forKey: "referral_code")
        aCoder.encode(sponsorId, forKey: "sponsor_id")
        aCoder.encode(leaderId, forKey: "leader_id")
        aCoder.encode(packages, forKey: "packages")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(verificationStatus, forKey: "verifications_status")
        aCoder.encode(wallets, forKey: "wallets")
    }
}

class Wallet: NSObject, Mappable, NSCoding{
    
    var id: Int!
    var balance: Double!
    var type: Int!
    
    required init?(map: Map) {
    }
    
    override init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        balance <- map["balance"]
        type <- map["type"]
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        balance = aDecoder.decodeObject(forKey: "balance") as? Double
        type = aDecoder.decodeObject(forKey: "nric") as? Int
    }
    
    func encode(with aCoder: NSCoder)  {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(balance, forKey: "balance")
        aCoder.encode(type, forKey: "type")
    }
}

class VerificationStatus: NSObject, Mappable, NSCoding{
    
    var phoneNumber: Bool!
    var email: Bool!
    var nric: Int!
    var selfie: Int!
    
    required init?(map: Map) {
    }
    
    override init(){}
    
    func mapping(map: Map) {
        phoneNumber <- map["phone_number"]
        email <- map["email"]
        nric <- map["nric"]
        selfie <- map["selfie"]
    }
    
    required init(coder aDecoder: NSCoder) {
        phoneNumber = aDecoder.decodeObject(forKey: "phone_number") as? Bool
        email = aDecoder.decodeObject(forKey: "email") as? Bool
        nric = aDecoder.decodeObject(forKey: "nric") as? Int
        selfie = aDecoder.decodeObject(forKey: "selfie") as? Int
    }
    
    func encode(with aCoder: NSCoder)  {
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phoneNumber, forKey: "phone_number")
        aCoder.encode(nric, forKey: "nric")
        aCoder.encode(selfie, forKey: "selfie")
    }
}
