//
//  TopUp.swift
//  Mareco
//
//  Created by Edy on 21/3/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import ObjectMapper

class TopUp: Mappable {
    var amount: Int! = 0
    var price: Int! = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        amount <- map["amount"]
        price <- map["price"]
    }
}

