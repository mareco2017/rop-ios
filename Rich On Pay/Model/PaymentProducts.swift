//
//  PaymentProducts.swift
//  Mareco
//
//  Created by Edison on 4/27/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import ObjectMapper

class PaymentProduct: Mappable {
    
    var id: Int!
    var product: String!
    var coverUrl: String!
    var name: String!
    var amount: Double!
    var price: Double!
    var typeId: Int!
    var categoryId: Int!
    var categoryName: String!
    var pictureUrl: String!
    var productCode: Int!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        product <- map["product"]
        coverUrl <- map["cover_url"]
        name <- map["name"]
        amount <- map["amount"]
        price <- map["price"]
        typeId <- map["type_id"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        pictureUrl <- map["picture_url"]
        productCode <- map["product_code"]
    }
    
}

class PaymentProductCategory: Mappable {
    
    var id: Int!
    var data: [String]!
    var category: String!
    var pictureUrl: String!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        data <- map["data"]
        category <- map["category"]
        pictureUrl <- map["picture_url"]
    }
}
