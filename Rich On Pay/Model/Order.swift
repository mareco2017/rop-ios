//
//  Order.swift
//  Mareco
//
//  Created by Edy on 12/4/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import ObjectMapper

class Order: Mappable {
    var id: Int!
    var expiredAt: Date!
    var createdAt: Date!
    var type: Int!
    var status: Int!
    var referenceNo: String!
    var ownable: User!
    var orderDetails: [OrderDetail]!
    var virtual: Virtual!
    var referableUser: User!
    var option: OrderOption!
//    var promo: Voucher!
    var total: Double!
    var recipientPhone: String!
    var referenceType = ""
    var typeString = ""
    var bankAccount: BankAccount!
    var statementNo: String!
    var amount: Double!
    var claimed: Date!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        expiredAt <- (map["expired_at"], DateTransform())
        createdAt <- (map["created_at"], DateTransform())
        claimed <- (map["claimed_at"], DateTransform())
        type <- map["type"]
        referenceNo <- map["reference_number"]
        ownable <- map["ownable"]
        orderDetails <- map["order_details"]
        virtual <- map["virtual"]
        referableUser <- map["user"]
        option <- map["options"]
        recipientPhone <- map["recipient_phone"]
        status <- map["status"]
        referenceType <- map["referable_type"]
        typeString <- map["type_string"]
        total <- map["total"]
        bankAccount <- map["bank_account"]
        statementNo <- map["statement_no"]
        amount <- map["amount"]
    }
}

class OrderDetail: Mappable {
    var id: Int!
    var orderId: Int!
    var qty: Int!
    var price: Double!
    var total: Double!
    var discount: Double!
    var product: PaymentProduct!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        id <- map["id"]
        orderId <- map["order_id"]
        qty <- map["qty"]
        price <- map["price"]
        total <- map["total"]
        discount <- map["discount"]
        product <- map["product"]
    }
}

class OrderOption: Mappable {
    
    var note: String!
    var customerId: String!
    var meterNo: String!
    var refNo: String!
    var customerName: String!
    var powerRate: String!
    var billCount: Int!
    var referenceNo: String!
    var period: String!
    var penalty: Double!
    var adminCharge: Double!
    var amount: Double!
    var serialNumber: String!
    var phoneNumber: String!
    var totalAmount: Double! = 0.0
    var hideName: Bool = false
    var standMeter: String!
    var stampDuty: Double!
    var ppj: Double!
    var ppn: Double!
    var pp: Double!
    var installment: Double!
    var kwh: Double!
    var ref2: String!
    var months: [String]!
    var pdamName: String!
    var fee: Double!
    var plnFee: Double!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        note <- map["note"]
        customerId <- map["customer_id"]
        meterNo <- map["meter_no"]
        refNo <- map["ref_no"]
        customerName <- map["customer_name"]
        powerRate <- map["power_rate"]
        billCount <- map["bill_count"]
        referenceNo <- map["ref_no"]
        period <- map["period"]
        penalty <- map["penalty"]
        adminCharge <- map["admin_charge"]
        amount <- map["amount"]
        serialNumber <- map["sn"]
        phoneNumber <- map["phone_number"]
        totalAmount <- map["total_amount"]
        hideName <- map["hide_name"]
        standMeter <- map["stand_meter"]
        pp <- map["pp"]
        ppj <- map["ppj"]
        ppn <- map["ppn"]
        installment <- map["installment"]
        stampDuty <- map["stamp_duty"]
        kwh <- map["kwh"]
        ref2 <- map["ref2"]
        months <- map["months"]
        pdamName <- map["pdam_name"]
        fee <- map["fee"]
        plnFee <- map["pln_prepaid_admin"]
    }
    
}

class Virtual: Mappable {
    var destBankAccountId: Int!
    var destBankAccount: BankAccount!
    var bankAccount: BankAccount!
    var note: String!
    
    required init?(map: Map) {}
    
    init(){}
    
    func mapping(map: Map) {
        destBankAccountId <- map["dest_bank_account_id"]
        destBankAccount <- map["dest_bank_account"]
        bankAccount <- map["bank_account"]
        note <- map["note"]
    }
}

//class VendorCategory: Mappable {
//
//    var id: Int!
//    var coverUrl: String!
//    var name: BankAccount!
//
//    required init?(map: Map) {}
//
//    init(){}
//
//    func mapping(map: Map) {
//        destBankAccountId <- map["dest_bank_account_id"]
//        destBankAccount <- map["dest_bank_account"]
//        bankAccount <- map["bank_account"]
//    }
//}


