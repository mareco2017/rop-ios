//
//  DataTypes.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit


extension String {
    
    var localized: String {
        return self.localizedWithComment(comment: "")
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType:NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    var sensorsPhoneNumber: String {
        var val = self
        if self.count > 7 {
            for index in 4...self.count - 4 {
                val = val.replacing(range: index ... index, with: "*")
            }
        }
        return val
    }
    
    var hideFullName: String {
        var val = self
        if self.count > 2 {
            for index in 2...self.count - 1{
                val = val.replacing(range: index ... index, with: "*")
            }
        }
        return val
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    var toAmtFormat: Double {
        if let amount = Double(self.replacingOccurrences(of: ".", with: "", options: .literal, range: nil)) {
            return amount
        }
        return 0
    }
    
    func localizedWithComment(comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
    
    var isEmptyField: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    // for convenience we should include String return
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = self.index(self.startIndex, offsetBy: r.lowerBound)
        let end = self.index(self.startIndex, offsetBy: r.upperBound)
        
        return String(self[start...end])
    }
    
    func replacing(range: CountableClosedRange<Int>, with replacementString: String) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end   = self.index(start, offsetBy: range.count)
        return self.replacingCharacters(in: start ..< end, with: replacementString)
    }
    
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
    
    func indicesOf(string: String) -> [Int] {
        var indices = [Int]()
        var searchStartIndex = self.startIndex
        
        while searchStartIndex < self.endIndex,
            let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
            !range.isEmpty
        {
            let index = distance(from: self.startIndex, to: range.lowerBound)
            indices.append(index)
            searchStartIndex = range.upperBound
        }
        
        return indices
    }
    
    var stringToDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        if self == "0000-00-00 00:00:00" {
            return Date()
        }
        dateFormatter.timeZone = TimeZone(abbreviation: "Local")
        return dateFormatter.date(from: "\(self)+0700")! as Date
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailText = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailText.evaluate(with: self)
    }
    
    var isNumberFiltered: Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = self.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        return self == numberFiltered
    }
    
    var isPhoneNumberFiltered: Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789+").inverted
        let compSepByCharInSet = self.components(separatedBy: aSet)
        let phoneNumberFiltered = compSepByCharInSet.joined(separator: "")
        let containsValidChars = self == phoneNumberFiltered
        
        return containsValidChars
    }
    
    func characterAtIndex(index: Int) -> Character? {
        var cur = 0
        for char in self {
            if cur == index {
                return char
            }
            cur += 1
        }
        return nil
    }
    
}

extension Formatter {
    @nonobjc static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        return formatter
    }()
    
}

extension Int {
    
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
    
    var formattedPrice: NSMutableAttributedString {
        let fmt = Formatter.withSeparator.string(for: self) ?? "0"
        let range = NSRange(location:0,length:2)
        let attPrice = NSMutableAttributedString(string:  "Rp \(fmt)", attributes: [NSAttributedStringKey.font:UIFont(name: "Comfortaa-Bold", size: 12.interfaceIdiomSize(size: 7))!])
        attPrice.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: range)
        attPrice.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location: attPrice.length-2,length: 2))
        return attPrice
    }
    
    func interfaceIdiomSize(size: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGFloat(self + size)
        }
        else {
            return CGFloat(self)
        }
    }
    
    func getDayString(inShortStyle: Bool = false) -> String {
        switch self {
        case 1:
            return inShortStyle ? "Mon" : "Monday"
        case 2:
            return inShortStyle ? "Tue" : "Tuesday"
        case 3:
            return inShortStyle ? "Wed" : "Wednesday"
        case 4:
            return inShortStyle ? "Thu" : "Thursday"
        case 5:
            return inShortStyle ? "Fri" : "Friday"
        case 6:
            return inShortStyle ? "Sat" : "Saturday"
        default:
            return inShortStyle ? "Sun" : "Sunday"
        }
    }
    
}

extension CGFloat {
    
    func interfaceIdiomSize(size: CGFloat) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGFloat(self + size)
        }
        else {
            return CGFloat(self)
        }
    }
}


extension Double {
    
    var forTailingZero: String{
        let tempVar = String(format: "%g", self)
        return tempVar
    }
    
    var withoutDecimal: Double {
        let numberFormatter: NumberFormatter = {
            let nf = NumberFormatter()
            nf.numberStyle = .decimal
            nf.minimumFractionDigits = 0
            nf.maximumFractionDigits = 0
            return nf
        }()
        return Double(numberFormatter.string(for: self )!)!
    }
    
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
    
    var formattedPrice: NSMutableAttributedString {
        let fmt = Formatter.withSeparator.string(for: self) ?? "0"
        let range = NSRange(location:0,length:2)
        let attPrice = NSMutableAttributedString(string:  "Rp \(fmt)", attributes: [NSAttributedStringKey.font:UIFont(name: "SegoeUI-SemiBold", size: 14.interfaceIdiomSize(size: 10))!])
        attPrice.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: range)
        return attPrice
    }
    
}

