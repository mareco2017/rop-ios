//
//  UIImage+UIImageView.swift
//  Mareco
//
//  Created by Edison on 11/30/17.
//  Copyright © 2017 Mareco. All rights reserved.
//

import UIKit

class CircularImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentMode = UIViewContentMode.scaleAspectFill
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
}

enum PlaceHolderImage: String {
    
    case store = "placeholder_store"
    case userProfile = "placeholder_user"
    case voucher = "placeholder_voucher"
    case photo = "placeholder_photo"
    case promotion = "placeholder_banner"
    case storeBanner = "placeholder_store_banner"
}

extension UIImageView {
    
    func setImage(url: String, placeholderImage: PlaceHolderImage?) {
        if placeholderImage == nil {
            self.sd_setImage(with: URL(string: url))
        }
        else {
            self.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: placeholderImage!.rawValue))
        }
    }
}

extension UIImage {
    
    class func image(with color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y:0.0, width:1.0, height:1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func placeholderImage(image: PlaceHolderImage) -> UIImage? {
        return UIImage(named: image.rawValue)
    }
    
    func resizeWithPercent(quality: JPEGQuality) -> UIImage? {
        let percentage: CGFloat = quality.rawValue
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
}
