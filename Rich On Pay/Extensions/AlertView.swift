//
//  AlertView.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class AlertView: NSObject {
    
    class func showAlert(view: UIViewController , message: String, title: String, showCancelBtn: Bool? = false, rightBtnTitle: String = "Ok", leftBtnTitle:String? = "Cancel", rightBtnAction: @escaping (UIAlertAction) -> (), leftBtnAction: @escaping (UIAlertAction) -> ()) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: rightBtnTitle, style: .default, handler: rightBtnAction)
        alertController.addAction(defaultAction)
        if showCancelBtn! {
            let cancelBtn = UIAlertAction(title: leftBtnTitle, style: .cancel, handler: leftBtnAction)
            alertController.addAction(cancelBtn)
        }
        view.present(alertController, animated: true, completion: nil)
        
    }
    
    class func showAlert(view: UIViewController , message: String, title: String, showCancelBtn: Bool? = false, rightBtnTitle: String = "Ok", leftBtnTitle:String? = "Cancel") {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: rightBtnTitle, style: .default, handler: nil)
        alertController.addAction(defaultAction)
        if showCancelBtn! {
            let cancelBtn = UIAlertAction(title: leftBtnTitle, style: .cancel, handler: nil)
            alertController.addAction(cancelBtn)
        }
        view.present(alertController, animated: true, completion: nil)
    }
}
