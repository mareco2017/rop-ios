//
//  TextField.swift
//  Rich On Pay
//
//  Created by Edison on 10/24/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

class AmountTextField: UITextField, UITextFieldDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.keyboardType = .numberPad
        self.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
    }
    
    @IBAction func editingChanged(_ sender: UITextField) {
        sender.text = Int(sender.text!.replacingOccurrences(of: ".", with: "", options: .literal, range: nil))?.formattedWithSeparator
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return string.isNumberFiltered
    }
    
}

class PhoneNumberTextField: UITextField, UITextFieldDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let val = string.replacingOccurrences(of: "-", with: "", options: .literal, range: nil).replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        let invalidPlusCharPosition = text.count > 0 && string == "+"
        
        return val.isPhoneNumberFiltered && newLength <= 15 && !invalidPlusCharPosition
    }
    
}
