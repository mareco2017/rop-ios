//
//  UICollectionVIew+UITableView.swift
//  Rich On Pay
//
//  Created by Edison on 9/17/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func fitContentHeight(footerHeight: CGFloat = 0) {
        for constraint in self.constraints {
            if constraint.firstItem as? UICollectionView == self {
                if constraint.firstAttribute == .height {
                    DispatchQueue.main.async {
                        constraint.constant = self.contentSize.height + footerHeight
                    }
                }
            }
        }
    }
    
    func reloadAndFitHeight() {
        self.reloadData()
        self.fitContentHeight()
    }
}

extension UITableView {
    
    func fitContentHeight(footerHeight: CGFloat = 0) {
        for constraint in self.constraints {
            if constraint.firstItem as? UITableView == self {
                if constraint.firstAttribute == .height {
                    DispatchQueue.main.async {
                        constraint.constant = self.contentSize.height + footerHeight
                    }
                }
            }
        }
    }
    
    func reloadAndFitHeight() {
        self.reloadData()
        self.fitContentHeight(footerHeight: 40)
    }
}

