//
//  UIStoryboard+Loader.swift
//  Mareco
//
//  Created by Edy on 21/3/18.
//  Copyright © 2018 Mareco. All rights reserved.
//

import UIKit

fileprivate enum Storyboard : String {
    case paymentProduct = "PaymentProduct"
}

fileprivate extension UIStoryboard {

    static func loadFromPaymentProduct(_ identifier: String) -> UIViewController {
        return load(from: .paymentProduct, identifier: identifier)
    }
    
    static func load(from storyboard: Storyboard, identifier: String) -> UIViewController {
        let uiStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier)
    }
}

// MARK: App View Controllers

extension UIStoryboard {
    
    // MARK: - Auth
    
//    static func loadEnterVerificationCodeViewController() -> EnterVerificationCodeViewController {
//        return loadFromAuth("EnterVerificationCodeViewController") as! EnterVerificationCodeViewController
//    }
    
    // MARK: - PaymentProduct
    static func loadPrepaidPhoneCreditViewController() -> PrepaidPhoneCreditViewController {
        return loadFromPaymentProduct("PrepaidPhoneCreditViewController") as! PrepaidPhoneCreditViewController
    }
    
    static func loadPaymentProductAmountSelectionViewController() -> PaymentProductAmountSelectionViewController {
        return loadFromPaymentProduct("PaymentProductAmountSelectionViewController") as! PaymentProductAmountSelectionViewController
    }
    
    static func loadElectricityPaymentViewController() -> ElectricityPaymentViewController {
        return loadFromPaymentProduct("ElectricityPaymentViewController") as! ElectricityPaymentViewController
    }
    
    static func loadGameVouchersSelectionViewController() -> GameVouchersSelectionViewController {
        return loadFromPaymentProduct("GameVouchersSelectionViewController") as! GameVouchersSelectionViewController
    }
    
    static func loadGameVouchersPaymentViewController() -> GameVouchersPaymentViewController {
        return loadFromPaymentProduct("GameVouchersPaymentViewController") as! GameVouchersPaymentViewController
    }
    
    static func loadTransactionSuccessViewController() -> TransactionSuccessViewController {
        return loadFromPaymentProduct("TransactionSuccessViewController") as! TransactionSuccessViewController
    }
    
    static func loadPDAMViewController() -> PDAMViewController {
        return loadFromPaymentProduct("PDAMViewController") as! PDAMViewController
    }
    
    static func loadSelectRegionViewController() -> SelectRegionViewController {
        return loadFromPaymentProduct("SelectRegionViewController") as! SelectRegionViewController
    }
    
}
