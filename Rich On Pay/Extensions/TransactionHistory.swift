//
//  TransactionHistory.swift
//  Mareco
//
//  Created by Edison on 11/17/17.
//  Copyright © 2017 Mareco. All rights reserved.
//

import Foundation
import ObjectMapper

class Transaction: Mappable {
    
    var id:Int!
    var referenceNumber: String!
    var referencePhoneNumber: String = ""
    var totalAmount: Double!
    var createdAt: Date!
    var type: Int!
    var transactionType: Int!
    var order: Order!
    var relatedReferenceId: String!
    var walletType: Int!
    var combinedSerialNumber: String!
    var fee: Double!
    var productAmount: Double!
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        referenceNumber <- map["reference_number"]
        referencePhoneNumber <- map["reference_phone_number"]
        totalAmount <- map["total_amount"]
        type <- map["type"]
        transactionType <- map["transaction_type"]
        createdAt <- (map["created_at"], DateTransform())
        order <- map["order"]
        relatedReferenceId <- map["related_reference_number"]
        walletType <- map["wallet_type"]
        combinedSerialNumber <- map["combined_serial_number"]
        fee <- map["payment_product_fee"]
        productAmount <- map["payment_product_amount"]
    }
}

class HistoryResponse: Mappable {
    
    var mutations:[TransactionHistory]!
    var totalPages: Int!
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        mutations <- map["mutasi"]
        totalPages <- map["total_pages"]
    }
}

class TransactionHistory: Mappable {
    
    var id: Int!
    var owner: Int!
    var to: User!
    var from: User!
    var createdAt: Date!
    var createdDate: Date!
    var expDate: Date!
    var uniqueId: String!
    var transactionId: String?
    var amt: Double!
    var stat: Int!
    var type: Int!
    var picture = ""
    var walletLog: Int!
    var currentAmtFrom: Double!
    var currentAmtTo: Double!
    var receivedBy: String!
    var paymentMethod: String!
    var totalAmt: AnyObject!
    var confirmation: UserBank?
    var rekTo = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        owner <- map["owner"]
        to <- map["kepada"]
        type <- map["type"]
        from <- map["dari"]
        totalAmt <- map["total_amt"]
        createdAt <- (map["gendt"], DateTransform())
        createdDate <- (map["createdt"], DateTransform())
        expDate <- (map["expdt"], DateTransform())
        uniqueId <- map["uniqueid"]
        amt <- map["amt"]
        paymentMethod <- map["payment_method"]
        stat <- map["stat"]
        picture <- map["picture"]
        receivedBy <- map["received_by"]
        currentAmtTo <- map["current_amt_kepada"]
        currentAmtFrom <- map["current_amt_dari"]
        walletLog <- map["walletlog"]
        confirmation <- map["confirmation"]
        transactionId <- map["transaction_id"]
        rekTo <- map["rek_kepada"]
    }
    
}

//{
//    "owner": 1,
//    "createdt": "2017-11-30 12:19:05",
//    "total_amt": 15000,
//    "received_by": "mareco",
//    "payment_method": "Mareco Wallet",
//    "walletlog": 1,
//    "paidstat": 1,
//    "uniqueid": "027569",
//    "picture": "",
//    "note": "makan di bcs mall jco",
//    "id": 5
//}
