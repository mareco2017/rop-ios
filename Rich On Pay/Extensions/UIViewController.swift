//
//  UIViewController.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

extension UIApplication {
    
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
}

extension UIViewController {
    
    func showToast(message : String) {
        //        let toastLabel = UILabel()
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: 100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.primary.withAlphaComponent(0.9)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "SFProText-Regular", size: 14.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func swapRootView(vc: UIViewController, toNotifScreen: Bool = false) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        UIView.transition(with: window, duration: 0.3, options: .transitionFlipFromLeft, animations: {
            appDelegate.window?.rootViewController = vc
        }, completion: { completed in        })
    }
    
    func showAlertProgress() {
        AlertView.showAlert(view: self, message: "Fitur ini sedang dalam tahap pengembangan", title: "Mohon Maaf")
    }
    
    func getProfile() {
        API.request(API.Service.getProfile, showLoading: false, success: { (response: APIResponse) in
            guard let user = response.data?.user else {
                return
            }
            Defaults[.user] = user
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshHome"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshAccount"), object: nil)
        }) { (error) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshAccount"), object: nil)
        }
    }
    
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    var iPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}
