//
//  invoice.swift
//  Mareco
//
//  Created by Edison on 12/12/17.
//  Copyright © 2017 Mareco. All rights reserved.
//

import Foundation
import ObjectMapper

class Receipt: Mappable {
    
    var refrenceNumber: String!
    var date: Date!
    var recipientPhone: String!
    var receiverPhone: String!
    var recipient: String!
    var amount: Double!
    var serialNumber: String!
    var product: String!
    var customerId: String!
    var walletType: Int!
    
    required init?(map: Map) {
    }
    
    init() {
    }
    
    func mapping(map: Map) {
        refrenceNumber <- map["reference_number"]
        recipientPhone <- map["recipient_phone"]
        receiverPhone <- map["receiver_phone"]
        recipient <- map["recipient"]
        amount <- map["amount"]
        date <- (map["time"], DateTransform())
        serialNumber <- map["sn"]
        product <- map["product"]
        customerId <- map["customer_id"]
        walletType <- map["wallet_type"]
    }
    
}

class WalletInvoice: Mappable {
    
    var id: Int!
    var totalAmt: Double!
    var createdt: Date!
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        totalAmt <- map["total_amt"]
        createdt <- (map["createdt"], DateTransform())
    }
    
}
