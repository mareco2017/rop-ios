//
//  DefaultUser.swift
//  Rich On Pay
//
//  Created by Edison on 9/19/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension UserDefaults {
    subscript(key: DefaultsKey<User?>) -> User? {
        get { return unarchive(key) }
        set { archive(key, newValue) }
    }
}

extension DefaultsKeys {
    static let isLogging = DefaultsKey<Bool>("isLogging_v3")
    static let user = DefaultsKey<User?>("user_v2")
}

