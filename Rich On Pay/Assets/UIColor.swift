//
//  UIColor.swift
//  Rich On Pay
//
//  Created by Edison on 9/17/18.
//  Copyright © 2018 ROP. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc static let primary = UIColor(red: 252, green: 52, blue: 45)
    
    @nonobjc static let lightGrayR = UIColor(red: 107, green: 107, blue: 107)
    
    @nonobjc static let lightGrayS = UIColor(red: 235, green: 235, blue: 235)
    
    @nonobjc static let backgroundSilver = UIColor(red: 196, green: 196, blue: 196)
    
    @nonobjc static let backgroundGold = UIColor(red: 255, green: 201, blue: 109)
    
    @nonobjc static let backgroundPlatinum = UIColor(red: 65, green: 73, blue: 88)
    
    @nonobjc static let badgeSilver = UIColor(red: 135, green: 135, blue: 135)
    
    @nonobjc static let badgeGold = UIColor(red: 248, green: 157, blue: 0)
    
    @nonobjc static let badgePlatinum = UIColor(red: 1, green: 22, blue: 64)
    
    @nonobjc static let yellowR = UIColor(red: 254, green: 178, blue: 40)
    
    @nonobjc static let blueR = UIColor(red: 40, green: 161, blue: 254)
    
    @nonobjc static let greenR = UIColor(red: 6, green: 188, blue: 0)
    
    @nonobjc static let purpleR = UIColor(red: 67, green: 40, blue: 254)
    
    convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
        self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    static func returnRGBColor(r:CGFloat, g:CGFloat, b:CGFloat, alpha:CGFloat) -> UIColor{
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
    
}

