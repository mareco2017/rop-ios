//
//  PickerViewController.swift
//  Mareco
//
//  Created by Edison on 12/5/17.
//  Copyright © 2017 Mareco. All rights reserved.
//

import UIKit

class PickerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "PickerCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "BankCell", bundle: nil), forCellReuseIdentifier: "bankCell")
    }
    
    func fitHeight() {
        for constraint in self.tableView.constraints {
            if constraint.firstItem as? UITableView == self {
                if constraint.firstAttribute == .height {
                    DispatchQueue.main.async {
                        constraint.constant = min(self.tableView.contentSize.height, UIScreen.main.bounds.height - 300)
                    }
                }
            }
        }
    }
    
}

extension PickerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
   
}

class PickerTableCell: UITableViewCell {
    
    @IBOutlet weak var pickerLbl: UILabel!
    
}
